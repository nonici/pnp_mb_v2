/* ************************************************************************** */
/*
  @File Name
    enums.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef ENUMS_H    /* Guard against multiple inclusion */
#define ENUMS_H

// *****************************************************************************
//Turn on various development features and logs

#define MAIN_AXIS_LETTERS   "XYZUVWABCD"

// *****************************************************************************

#include "configuration.h"

// *****************************************************************************

typedef enum {
    axisX,
    axisY,

    axisZ, //axisZ1,
    axisU, //axisZ2,
    axisV, //axisZ3,
    axisW, //axisZ4,

    axisA, //axisA1,
    axisB, //axisA2,
    axisC, //axisA3,
    axisD, //axisA4,

    axisF, //axisFf,
    axisR, //axisFr
} axle_t;

typedef enum {
    AllNozzles = 0,
    Nozzle1 = 1,
    Nozzle2 = 2,
    Nozzle3 = 3,
    Nozzle4 = 4,
} nozzle_t;

typedef enum {
    HeadCam,
    BaseCam,
} camera_t;

typedef enum {
    AXLE_IDLE = 0,
    AXLE_MOVING,
    AXLE_HOMING,
    AXLE_HOMED,
} axle_states_t;

typedef enum {
    MOVE_LINE,
    MOVE_HOMING,
} motion_type_t;

typedef enum {
    SensorN1 = 1,
    SensorN2 = 2,
    SensorN3 = 3,
    SensorN4 = 4,
    SensorA = 5,
    SensorV = 0,
} sensors_t;

typedef enum {
    FEEDER_READY,
    FEEDER_ACTIVE,
    FEEDER_DONE,
    FEEDER_ERROR,
} feeder_status_t;

// *****************************************************************************

//Fet driver pin map:

// FET_CRTL_1_PIN   Nozzle 1 vacuum solenoid        pneumatics/solenoids.c
// FET_CRTL_2_PIN   Nozzle 1 air solenoid           pneumatics/solenoids.c
// FET_CRTL_3_PIN   

// FET_CRTL_4_PIN   Nozzle 2 vacuum solenoid        pneumatics/solenoids.c
// FET_CRTL_5_PIN   Nozzle 2 air solenoid           pneumatics/solenoids.c
// FET_CRTL_6_PIN   

// FET_CRTL_7_PIN   Nozzle 3 vacuum solenoid        pneumatics/solenoids.c
// FET_CRTL_8_PIN   Nozzle 3 air solenoid           pneumatics/solenoids.c
// FET_CRTL_9_PIN   

//FET_CRTL_10_PIN   Nozzle 4 vacuum solenoid        pneumatics/solenoids.c
//FET_CRTL_11_PIN   Nozzle 4 air solenoid           pneumatics/solenoids.c
//FET_CRTL_12_PIN   

//FET_CRTL_13_PIN   Air pump PWM                    pneumatics/pump.c
//FET_CRTL_14_PIN   
//FET_CRTL_15_PIN   

//FET_CRTL_16_PIN   
//FET_CRTL_17_PIN   
//FET_CRTL_18_PIN   

//FET_CRTL_19_PIN   Head camera light               pnp/cam_led.c
//FET_CRTL_20_PIN   Bottom camera light             pnp/cam_led.c
//FET_CRTL_21_PIN   Motor power supply enable       pnp/psu.c

// *****************************************************************************

#define array_length(arr) (sizeof (arr) / sizeof *(arr))

// *****************************************************************************

#endif /* ENUMS_H */

/* *****************************************************************************
 End of File
 */