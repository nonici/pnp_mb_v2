/* ************************************************************************** */
/*
  @File Name
    slots.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef SLOTS_H    /* Guard against multiple inclusion */
#define SLOTS_H

// *****************************************************************************

#include "stdbool.h"
#include "stdlib.h"
#include "math.h"
#include "stdint.h"

#include "../configuration.h"

#include "../comm/comm.h"
#include "feeders.h"
#include "slots.h"
#include "motion/profiler.h"
#include "../pnp/signalers.h"

#include "ms_timer.h"
#include "feeder_dictionary.h"

// *****************************************************************************

//#defines

// *****************************************************************************

//Variables

// *****************************************************************************

void SlotInitialize(void);

void FeederSlotHandler(void);

void DecomposeOpenPNPFeederCommand(char codeLetter, double codeNumber);
void DecomposeKeyboardFeederCommand(uint8_t nodeId, uint8_t keyMap);
void SlotParseReportWord(uint8_t slot, uint8_t report);

feeder_status_t SlotGetCurrentFeederStatus(void);
bool SlotIsCurrentFeederCommandUnanswered(void);

//Functions

// *****************************************************************************

#endif /* SLOTS_H */

/* *****************************************************************************
 End of File
 */
