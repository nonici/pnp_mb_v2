// *****************************************************************************
/*
  @File Name
    slots.c
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "slots.h"

// *****************************************************************************

//defines
//structs
//varibles
//constants

//Feeder/slot structure

typedef struct {
    bool slotFeederPresent;
    bool slotLane1Pressent;
    bool slotLane2Pressent;
    bool slotLane3Pressent;
    bool slotLane4Pressent;
    //    bool comandActive;
    //    bool commandFromKeyboard;
    //    bool commandError;
    //    bool commandTimeout;
    //    bool commandCompleted;
    //    bool commandInvalidLane;
    //    bool commandInvalidPitch;
} slot_map_t;

//predefined feed commands, later replace by feeder structure 
//slot_map_t slotID[NUMBER_OF_SLOTS][NUMBER_OF_LANES];

slot_map_t slotID[NUMBER_OF_SLOTS];

ms_timer_t timerFeederHeartbeat = {0};
ms_timer_t timerFeederTest = {0};
ms_timer_t timerFeederTimeout = {0};

feeder_command_t lastOpenPNPCommand = {0};
feeder_command_t lastKeyboardCommand = {0};
feeder_command_t lastTestCommand = {0};

feeder_status_t currentFeederStatus = FEEDER_READY;
bool isSlotResponseExpected = 0;
bool isOpenPnpCommandUnaswered = 0;
bool isSlotTestActive = 0;

uint8_t slotTestLaneCounter = 0;


// *****************************************************************************

//Local function definitions
void SlotCleanFeederCommand(feeder_command_t *cmd);
void SlotGetLaneBitFieldFromLaneNumber(feeder_command_t *cmd);
void SlotGetPitchBitFieldFromPitchNumber(feeder_command_t *cmd);
void SlotActivatePeelerForCommand(feeder_command_t *cmd);

void BeginGlobalTestOnAllSlots(void);
void EndGlobalTestOnAllSlots(void);


// *****************************************************************************

void SlotInitialize(void) {

    SlotCleanFeederCommand(&lastOpenPNPCommand);
    SlotCleanFeederCommand(&lastKeyboardCommand);
    SlotCleanFeederCommand(&lastTestCommand);

    ////Init struct
    uint32_t i = 0;
    for (i = 0; i < NUMBER_OF_SLOTS; i++) {
        slotID[i].slotFeederPresent = 0;
    }
    //        for (uint32_t j = 0; j == NUMBER_OF_LANES; j++) {
    //            slotID[i][j].slotFeederPresent = 0;
    //            slotID[i][j].slotLanePressent = 0;
    //            slotID[i][j].comandActive = 0;
    //            slotID[i][j].commandFromKeyboard = 0;
    //            slotID[i][j].commandError = 0;
    //            slotID[i][j].commandTimeout = 0;
    //            slotID[i][j].commandCompleted = 0;
    //            //invalid lane
    //            //invalid pitch
    //
    //        }
    //    }

    //Send out first sync message
    //Detect all available slots and mounted feeder through hearth beat message

    //for now fixed values: 0, 10-18
    slotID[0].slotFeederPresent = 1;
    for (i = 10; i < NUMBER_OF_SLOTS; i++) {
        slotID[i].slotFeederPresent = 1;
    }
}

void FeederSlotHandler(void) {

    //PNP sync frame
    if (!MsTimerIsTimerActive(&timerFeederHeartbeat)) {
        MsTimerDelayMs(&timerFeederHeartbeat, 5000);
        //SignalLedToggle(ledBlue);
        //Send out hearth beat signal
    }

    //Messages to send

    //Messages to receive

    //Report results: error / success



    if (isOpenPnpCommandUnaswered) {
        //Wait for peeler
        if (DriverIsActive()) {
            return;
        }
        //
        if (!MsTimerIsTimerActive(&timerFeederTimeout)) {
            currentFeederStatus = FEEDER_ERROR;
        }

        if (currentFeederStatus == FEEDER_DONE) {
            GcodeRegexCommandConfirm();
            currentFeederStatus = FEEDER_READY;
            isOpenPnpCommandUnaswered = 0;
        }
        if (currentFeederStatus == FEEDER_ERROR) {
            GcodeRegexErrorFeedFailed();
            currentFeederStatus = FEEDER_READY;
            isOpenPnpCommandUnaswered = 0;
        }
    }

    if (isSlotTestActive) {
        //Allocate all slot
        if (MsTimerIsTimerActive(&timerFeederTest)) {
            return;
        }
        MsTimerDelayMs(&timerFeederTest, 1250);

        //Prepare common parameters
        lastTestCommand.letter = 'F';
        lastTestCommand.cmdBf = BF_FDR_CMD_ADVANCE;
        lastTestCommand.laneNumber = slotTestLaneCounter;
        lastTestCommand.pitchNumber = 0;
        SlotGetLaneBitFieldFromLaneNumber(&lastTestCommand);
        SlotGetPitchBitFieldFromPitchNumber(&lastTestCommand);

        lastTestCommand.compiledCommand = FeederComposeCommand(lastTestCommand.cmdBf, lastTestCommand.laneNumber, lastTestCommand.pitchBf);

        //Get ready for next run
        slotTestLaneCounter++;
        slotTestLaneCounter %= 4;

        //For all active slots, feed lane

        for (uint32_t i = 0; i < NUMBER_OF_SLOTS; i++) {
            if (slotID[i].slotFeederPresent) {

                lastTestCommand.slotNumber = i;
                CanbusSendCommandToSlot(&lastTestCommand);
            }
        }
        SlotCleanFeederCommand(&lastTestCommand);
    }
}

void DecomposeOpenPNPFeederCommand(char codeLetter, double codeNumber) {
    // Letter, slot ID, '.', lane id, pitch
    // F1.4     //Feed on slot 1, lane 4
    // R10.104  //Retract on slot 10, lane 1, pitch 4mm
    // F11.503  //Invalid lane, invalid pitch

    //Detect command validity
    //Detect if command already active, throw error
    //    if (lastCommand.status != COMMAND_NO_COMMAND) {
    //        //Throw error here
    //        return;
    //    }
    //    lastCommand.status = COMMAND_RECEIVED;

    if (codeLetter == 'B') {
        BeginGlobalTestOnAllSlots();
        GcodeRegexCommandConfirm();
        //Report available slots and detected feeders
        return;
    }
    if (codeLetter == 'E') {
        EndGlobalTestOnAllSlots();
        GcodeRegexCommandConfirm();
        //Generate test report
        return;
    }

    //command letters, check if matched, else error
    if (!(codeLetter == 'F' || codeLetter == 'R' /*|| codeLetter == 'P'*/)) {
        //Throw error here
        //lastCommand.status = COMMAND_INVALID;
        return;
    }

    MsTimerDelayMs(&timerFeederTimeout, (uint32_t) FEEDER_TIMEOUT_MS);
    isOpenPnpCommandUnaswered = 1;
    currentFeederStatus = FEEDER_ACTIVE;
    isSlotResponseExpected = 1;




    //Extract feed data from code number
    //Lane numbers 1 to 4 are enumerated 0 to 3
    lastOpenPNPCommand.letter = codeLetter;
    uint32_t a = (uint32_t) floor(codeNumber * 1000);
    lastOpenPNPCommand.slotNumber = (uint8_t) ((a / 1000));
    lastOpenPNPCommand.laneNumber = (uint8_t) ((a / 100 % 10) - 1);
    lastOpenPNPCommand.pitchNumber = (uint8_t) ((a % 100));

    //Compile new command

    //Feed for pitch
    if (codeLetter == 'F') {
        lastOpenPNPCommand.cmdBf = BF_FDR_CMD_ADVANCE;
        //GcodeRegexCommandConfirm(); //Move later
    }
    //Retract for pitch
    if (codeLetter == 'R') {

        lastOpenPNPCommand.cmdBf = BF_FDR_CMD_RETRACT;
        //GcodeRegexCommandConfirm(); //Move later
    }

    //    //Peel for pitch, not yet implemented, not used with master peeler
    //    if (codeLetter == 'P') {
    //        lastOpenPNPCommand.cmdBf = BF_FDR_CMD_PEEL;
    //        //GcodeRegexCommandConfirm(); //Move later
    //    }

    SlotGetLaneBitFieldFromLaneNumber(&lastOpenPNPCommand);
    SlotGetPitchBitFieldFromPitchNumber(&lastOpenPNPCommand);

    //Create command bit field 
    lastOpenPNPCommand.compiledCommand = FeederComposeCommand(lastOpenPNPCommand.cmdBf, lastOpenPNPCommand.laneNumber, lastOpenPNPCommand.pitchBf);

    //lastCommand.status = COMMAND_COMPILED;
    SlotActivatePeelerForCommand(&lastOpenPNPCommand);

    CanbusSendCommandToSlot(&lastOpenPNPCommand);
    //GcodeRegexCommandConfirm(); //Move later

    SlotCleanFeederCommand(&lastOpenPNPCommand);
}

void DecomposeKeyboardFeederCommand(uint8_t nodeId, uint8_t keyMap) {

    lastKeyboardCommand = FeederComposeCommandFromKeymap(keyMap);
    lastKeyboardCommand.slotNumber = CanGetSlotAddressFromNodeId(nodeId);
    CanbusSendCommandToSlot(&lastKeyboardCommand);
    SlotCleanFeederCommand(&lastKeyboardCommand);
}

void SlotParseReportWord(uint8_t slot, uint8_t report) {

    //Extract feed result information
    fdr_status_report_opcode_t reportStatus = FeederGetStatusFlagsFromStatusWord(report);
    //fdr_lane_flags_bf_t reportLane = FeederGetLaneFlagsFromStatusWord(report);

    //Generate response for OpenPNP based commands
    if (isSlotResponseExpected) {
        switch (reportStatus) {
            case BF_FSR_NO_REPORT:
                //SerialMsgAppendStringToTxBuilder("SlotNoRep\r\n");
                break;
            case BF_FSR_HELLO_LOW_PWR:
                //SerialMsgAppendStringToTxBuilder("SlotLowPwr\r\n");
                break;
            case BF_FSR_HELLO_HIGH_PWR:
                //SerialMsgAppendStringToTxBuilder("SlotHighPwr\r\n");
                break;
            case BF_FSR_READY:
                //SerialMsgAppendStringToTxBuilder("FSR_Ready\r\n");
                break;
            case BF_FSR_FEEDING:
                //SerialMsgAppendStringToTxBuilder("SlotFSRFeeding\r\n");
                break;
            case BF_FSR_ERROR:
                currentFeederStatus = FEEDER_ERROR;
                isSlotResponseExpected = 0;
                break;
            case BF_FSR_TIMEOUT:
                //SerialMsgAppendStringToTxBuilder("SlotTimeout\r\n");
                break;
            case BF_FSR_COMPLETED:
                currentFeederStatus = FEEDER_DONE;
                isSlotResponseExpected = 0;
                break;
            case BF_FSR_RESERVED:
                //SerialMsgAppendStringToTxBuilder("SlotFSRReserved\r\n");
                break;
        }
    }

    //    //If test mode is active
    //    if (isSlotTestActive) {
    //        //Write results to slot objects
    //    }


    //Keyboard commands are skipped 
}

void SlotActivatePeelerForCommand(feeder_command_t * cmd) {
    //Slot zero does not peel
    if (!cmd->slotNumber) {
        return;
    }
    //Do not peel when test is active
    if (isSlotTestActive) {
        return;
    }
    double rotation = ((double) PEELER_ROTATION_FIXED + (double) (PEELER_ROTATION_PER_PITCH * (double) cmd->pitchNumber));
    if (cmd->slotNumber < 10) {
        ProfilerPeelFrontForDegrees(rotation);
    } else {

        ProfilerPeelRearForDegrees(rotation);
    }
}

void SlotCleanFeederCommand(feeder_command_t * cmd) {

    cmd->letter = 0;
    cmd->slotNumber = 0;
    cmd->laneNumber = 0;
    cmd->pitchNumber = 0;
    cmd->cmdBf = 0;
    cmd->laneBf = 0;
    cmd->pitchBf = 0;
    cmd->compiledCommand = 0;
    cmd->compiledReport = 0;
}

void SlotGetLaneBitFieldFromLaneNumber(feeder_command_t * cmd) {
    //Add lane info to compiled command
    switch (cmd->laneNumber) {
        case 0:
            cmd->laneBf = BF_LANE_NO_1;
            break;
        case 1:
            cmd->laneBf = BF_LANE_NO_2;
            break;
        case 2:
            cmd->laneBf = BF_LANE_NO_3;
            break;
        case 3:
            cmd->laneBf = BF_LANE_NO_4;

            break;
        default:
            //Throw error here
            //lastCommand.status = COMMAND_INVALID;
            break;
    }
}

void SlotGetPitchBitFieldFromPitchNumber(feeder_command_t * cmd) {

    //Add pitch info to compiled command
    switch (cmd->pitchNumber) {
        case 0:
            //BUG in feeder firmware here
            //cmd->pitchBf = BF_PITCH_DEFAULT;
            cmd->pitchBf = BF_PITCH_MM_04;
            break;
        case 2:
            cmd->pitchBf = BF_PITCH_MM_02;
            break;
        case 4:
            cmd->pitchBf = BF_PITCH_MM_04;
            break;
        case 8:
            cmd->pitchBf = BF_PITCH_MM_08;
            break;
        case 12:
            cmd->pitchBf = BF_PITCH_MM_12;
            break;
        case 16:
            cmd->pitchBf = BF_PITCH_MM_16;
            break;
        case 20:
            cmd->pitchBf = BF_PITCH_MM_20;
            break;
        case 24:
            cmd->pitchBf = BF_PITCH_MM_24;
            break;
        case 28:
            cmd->pitchBf = BF_PITCH_MM_28;
            break;
        case 32:
            cmd->pitchBf = BF_PITCH_MM_32;
            break;
        case 36:
            cmd->pitchBf = BF_PITCH_MM_36;
            break;
        case 40:
            cmd->pitchBf = BF_PITCH_MM_40;
            break;
        case 44:
            cmd->pitchBf = BF_PITCH_MM_44;
            break;
        case 48:
            cmd->pitchBf = BF_PITCH_MM_48;
            break;
        case 52:
            cmd->pitchBf = BF_PITCH_MM_52;
            break;
        case 56:
            cmd->pitchBf = BF_PITCH_MM_56;
            break;
        default:
            //Throw error here
            //lastCommand.status = COMMAND_INVALID;
            cmd->pitchBf = BF_PITCH_DEFAULT;

            break;
    }
}

void BeginGlobalTestOnAllSlots(void) {

    isSlotTestActive = 1;
}

void EndGlobalTestOnAllSlots(void) {

    isSlotTestActive = 0;
}

feeder_status_t SlotGetCurrentFeederStatus(void) {

    return (currentFeederStatus);
}

bool SlotIsCurrentFeederCommandUnanswered(void) {
    return (isSlotResponseExpected);
}
//Core function

//Global functions

//Local functions


/* *****************************************************************************
 End of File
 */
