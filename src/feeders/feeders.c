// *****************************************************************************
/*
  @File Name
    feeders.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "feeders.h"
#include "slots.h"
#include "../comm/serial.h"

// *****************************************************************************
#define FEEDERS_COMM_SIZE               256

static uint8_t feedersFifoRxMem [FEEDERS_COMM_SIZE];
static uint8_t feedersFifoTxMem [FEEDERS_COMM_SIZE];

ringu8_t feedersFifoRx;
ringu8_t feedersFifoTx;

bool feederPowerOn;
uint32_t feederPowerOnStartCount;
uint32_t feederPowerOnStopCount;

// *****************************************************************************

void FeederPowerOn(void);
void FeederPowerOff(void);

// *****************************************************************************

void FeedersInitialize(void) {
    RingU8Initialize(&feedersFifoRx, feedersFifoRxMem, FEEDERS_COMM_SIZE);
    RingU8Initialize(&feedersFifoTx, feedersFifoTxMem, FEEDERS_COMM_SIZE);
}

void FeedersHandler(void) {

    //    if (serialCommEnabled == false) {
    //            return;
    //        }

    //transmit new messages
    FeedersTransmitToFeeders();

    //parse received
    FeedersForwardFromRxBufferToSerial();

    //Feeder power off on timeout    
    if (feederPowerOn) {
        //check if timer has elapsed
        if ((CORETIMER_CounterGet() - feederPowerOnStartCount) < feederPowerOnStopCount) {
            return;
        } else {
            feederPowerOn = 0;
            feederPowerOnStartCount = 0;
            feederPowerOnStopCount = 0;
            FeederPowerOff();
        }
    }
}

void FeedersReceiveFromFeeders(void) {
    while (UART5_ReceiverIsReady()) {
        UART5_ErrorGet();
        RingU8Write(&feedersFifoRx, (uint8_t) (UART5_ReadByte()));
    }
}

void FeedersTransmitToFeeders(void) {
    while ((UART5_TransmitterIsReady()) && RingU8DataInside(&feedersFifoTx)) {
        UART5_WriteByte(RingU8Read(&feedersFifoTx));
        FeederPowerOn();
    }
}

void FeedersMsgAppendCharToFeeders(char ch) {
    RingU8Write(&feedersFifoTx, ch);
}

void FeedersMsgAppendStringToTxFifo(char * strArray) {
    RingU8AppendString(&feedersFifoTx, strArray);
}

uint32_t FeedersSpaceInTxFifo(void) {
    return (RingU8FreeSpace(&feedersFifoTx));
}

void FeedersForwardFromRxBufferToSerial(void) {
    while (RingU8DataInside(&feedersFifoRx)) {
        SerialMsgAppendCharToTxFifo(RingU8Read(&feedersFifoRx));
    }
}

void FeederPowerOn(void) {
    feederPowerOnStopCount = 2000 * (CORETIMER_FrequencyGet() / 1000);
    feederPowerOnStartCount = CORETIMER_CounterGet();
    feederPowerOn = 1;
    FDR_PWR_FIX_Set();
}

void FeederPowerOff(void) {
    FDR_PWR_FIX_Clear();
}

//Global functions

//Local functions


/* *****************************************************************************
 End of File
 */
