/* ************************************************************************** */
/*
  @File Name
    feeders.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef FEEDERS_H    /* Guard against multiple inclusion */
#define FEEDERS_H

// *****************************************************************************

#include "definitions.h"
#include "ring.h"
#include "../pnp/tests.h"

// *****************************************************************************

//#defines

// *****************************************************************************

//Variables

// *****************************************************************************

void FeedersInitialize(void);
void FeedersHandler(void);
void FeedersReceiveFromFeeders(void);
void FeedersTransmitToFeeders(void);
void FeedersMsgAppendCharToFeeders(char ch);
void FeedersMsgAppendStringToTxFifo(char * strArray);
uint32_t FeedersSpaceInTxFifo(void);
void FeedersForwardFromRxBufferToSerial(void);
// *****************************************************************************

#endif /* FEEDERS_H */

/* *****************************************************************************
 End of File
 */
