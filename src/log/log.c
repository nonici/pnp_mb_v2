// *****************************************************************************
/*
  @File Name
    log.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "log.h"
#include "stringbuilder.h"
#include "pneumatics/sensors.h"
#include "comm/serial.h"
#include "system/system.h"
#include "assert.h"
#include "../configuration.h"

// *****************************************************************************
//bool __atomic_compare_exchange_n (type *ptr, type *expected, type desired, bool weak, int success_memmodel, int failure_memmodel)

// *****************************************************************************

typedef struct {
    uint32_t logTime;
    log_priority_t priority;
    log_message_t message;
    log_caller_obj_t caller;
    double paramDouble;
    uint32_t paramU32;
} log_entry_t;

static log_entry_t logs[LOG_BUFFER_SIZE];

char logTextualBufferMessage[LOG_TEXTUAL_BUFFER_SIZE];
stringbuilder_t logTextualBuffer;

volatile uint32_t lastLogId;
log_priority_t logWritePriority = logDebug;

bool logReportingOn;
bool dumpLogs;
uint32_t dumpLogId;
log_priority_t dumpLogsPriorityLevel;

// *****************************************************************************

const char * const priorityText[] = {
    [logError] = "ERROR ",
    [logWarning] = "WARN  ",
    [logInfo] = "INFO  ",
    [logDebug] = "DEBUG ",
};

const char * const logCallerText[] = {
    //Objects
    [LogCallerObjectAxisX] = "AxisX",
    [LogCallerObjectAxisY] = "AxisY",
    [LogCallerObjectAxisZ] = "AxisZ",
    [LogCallerObjectAxisU] = "AxisU",
    [LogCallerObjectAxisV] = "AxisV",
    [LogCallerObjectAxisW] = "AxisW",
    [LogCallerObjectAxisA] = "AxisA",
    [LogCallerObjectAxisB] = "AxisB",
    [LogCallerObjectAxisC] = "AxisC",
    [LogCallerObjectAxisD] = "AxisD",
    [LogCallerObjectAxisF] = "AxisF",
    [LogCallerObjectAxisR] = "AxisR",
    [LogCallerObjectParser] = "Parser",
    [LogCallerObjectSystem] = "System",
    [LogCallerObjectSlot] = "Slot",
};
const char * const logMsgText[] = {
    //General
    [LogNoMessage] = "NoMsg",
    [LogEventDuration] = "Event duration (us): ",
    [LogEStop] = "EStop",
    [LogInvalidCommand] = "Invalid parser command/",

    //Pneumatics
    [LogSensorsRaw] = "SensorRaw:",
    [LogSensorsReal] = "SensorReal(ch,val):",
    [LogPWMPump] = "PumpPWM:",

    //Motion
    [LogProfilerNewMotionDistance] = "Profiler Movement Distance (steps, mm): ",
    [LogProfilerNewMotionSpeed] = "Profiler Movement Speed: ",
    [LogProfilerNewMotionAcceleration] = "Profiler Movement Acceleration: ",
    [LogProfilerNewMotionDiscarded] = "Profiler Movement Discarded ",
    [LogProfilerNewMotionShort] = "Profiler Movement Short Constant speed: ",
    [LogProfilerNewMotionTrapezoidalShort] = "Profiler Movement Trapezoidal Short ",
    [LogProfilerNewMotionTrapezoidalFull] = "Profiler Movement Trapezoidal Full ",
    [LogProfilerExpectedToMove] = "Profiler expected to move (steps,distance): ",
    [LogProfilerMoved] = "Profiler moved (steps,distance): ",
    [LogProfilerMoveSuccessDistance] = "Move executed (steps,distance): ",
    [LogProfilerMoveSuccessTime] = "Move executed (ticks, seconds): ",
    [LogProfilerMoveFailedTimeOut] = "Move failed, engine timeout (ticks, seconds): ",
    [LogProfilerCurrentPosition] = "Current position is (axis, position): ",
    [LogProfilerDestinationPosition] = "Target destination position is (steps, mm): ",
    [LogEngineAddedStep] = "Engine added step to axis: ",
    [LogProfilerStartingPositionChanged] = "Profiler starting position changed to (steps, mm): ",
    [LogEngineOverrun] = "Engine Overrun",
    [LogEngineNotIdle] = "Engine not IDLE",
    [LogSystemInterruptOverrun] = "System interrupt overrun",
    [LogSystemFatalError] = "FATAL ERROR",
    [LogParserBufferOverflow] = "Log parser buffer overflow",
    [LogMovementEnabled] = "Movement Enabled",
    [LogMovementUnhomingZ] = "Movement UnhomingZ",
    [LogMovementHomingZ] = "Movement Homing Z",
    [LogMovementUnhomingXY] = "Movement UnhomingXY",
    [LogMovementHomingXY] = "Movement HomingXY",
    [LogMovementHomingCompleted] = "Movement Homing Completed",
    [LogMovementStepsToMakeThisBlock] = "StepsToMakeThisBlock is negative ",

    //Slots
    [LogSlotFeedOnSlotInititated] = "Feed Operation inititated F:",
    [LogSlotFeedOnSlotComplete] = "Feed Operation complete F:",
    [LogSlotFeedOnSlotFailed] = "Feed operation faile F:",
};

// *****************************************************************************

void LogDumpingFinished(void);
uint32_t LogGetNewId(void);
bool LogsFindLogsToDump(log_priority_t priority);
void LogConvertLogToText(uint32_t dumpLogId);
void LogDumpTextToUart(void);
void LogClearEntry(uint32_t id);

// *****************************************************************************

void LogInitialize(void) {
    lastLogId = 0;
    dumpLogs = 0;
    //logReportingOn = 1;
    dumpLogsPriorityLevel = logInfo;
    StringbuilderIntialize(&logTextualBuffer, logTextualBufferMessage, LOG_TEXTUAL_BUFFER_SIZE);

    for (uint32_t i = 0; i != LOG_BUFFER_SIZE; i++) {
        LogClearEntry(i);
    }
}

void LogHandler(void) {
#ifdef PNP_DISABLE_LOGS
    return;
#endif
    if (!dumpLogs && !logReportingOn) {
        return;
    }

    if (SerialSpaceInTxFifo() < LOG_TEXTUAL_BUFFER_SIZE) {
        return;
    }

    if (LogsFindLogsToDump(dumpLogsPriorityLevel)) {
        LogConvertLogToText(dumpLogId);
        LogDumpTextToUart();
    } else {
        LogDumpingFinished();
    }
}

void LogNewEntry(log_message_t msgId, log_caller_obj_t caller, log_priority_t prior, uint32_t par1, double par2) {
#ifdef PNP_DISABLE_LOGS
    return; // logs temporarily disabled
#endif
    if (prior > logWritePriority) {
        return;
    }
    uint32_t id = LogGetNewId();
    logs[id].logTime = SystemTimeGet();
    __conditional_software_breakpoint(prior < array_length(priorityText));
    logs[id].priority = prior;
    __conditional_software_breakpoint(msgId < array_length(logMsgText));
    logs[id].message = msgId;
    __conditional_software_breakpoint(caller < array_length(logCallerText));
    logs[id].caller = caller;
    logs[id].paramU32 = par1;
    logs[id].paramDouble = par2;
}

void LogForcedDump(void) {
#ifdef PNP_DISABLE_LOGS
    return; // logs temporarily disabled
#endif
    //force dumping of all logs, called when general exception occurs just before reset
    while (1) {
        if (LogsFindLogsToDump(dumpLogsPriorityLevel)) {
            LogConvertLogToText(dumpLogId);
            LogDumpTextToUart();
            do {
                SerialTransmitFromTxBufferToUart();
            } while (SerialDataInTxFifo());
        } else {
            break;
        }
    }
}

void LogDumpRequest(log_priority_t priority) {
    dumpLogId = lastLogId;
    dumpLogsPriorityLevel = priority;
    dumpLogs = 1;
}

void LogDumpingFinished(void) {
    if (logReportingOn) {
        return;
    }
    dumpLogs = 0;
    dumpLogId = 0;
    dumpLogsPriorityLevel = logWritePriority;
}

void LogReporting(void) {
    logReportingOn = !logReportingOn;
}

uint32_t LogGetNewId(void) {
    while (true) {
        uint32_t oldId = lastLogId;
        uint32_t newId = (oldId + 1) % LOG_BUFFER_SIZE;

        //atomic swap, only if oldId == lasLogId
        if (__atomic_compare_exchange_n(&lastLogId, &oldId, newId, 0, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST)) {
            return oldId;
        } else {
        }
        // retry until succeed
    }
}

bool LogsFindLogsToDump(log_priority_t priority) {

    bool logsToDumpExists = 0;
    uint32_t i = (dumpLogId + 1) % LOG_BUFFER_SIZE;
    while (true) {
        if ((logs[i].message != LogNoMessage)&&(priority >= logs[i].priority)) {
            dumpLogId = i;
            logsToDumpExists = 1;
            break;
        }
        if (i == dumpLogId) {
            LogDumpingFinished();
            break;
        }
        i = (i + 1) % LOG_BUFFER_SIZE;
    }
    return (logsToDumpExists);
}

void LogDumpTextToUart(void) {
    SerialMsgAppendStringToTxFifo(logTextualBufferMessage);
    StringbuilderResetBuilder(&logTextualBuffer);
}

void LogConvertLogToText(uint32_t dumpLogId) {
    //add flags for used and unused types of parameters
    StringbuilderResetBuilder(&logTextualBuffer);
    StringbuilderAppendString(&logTextualBuffer, priorityText[logs[dumpLogId].priority]);
    StringbuilderAppendU32(&logTextualBuffer, dumpLogId);
    StringbuilderAppendChar(&logTextualBuffer, ' ');
    SystemTimeStringGet(&logTextualBuffer, logs[dumpLogId].logTime);
    StringbuilderAppendChar(&logTextualBuffer, ' ');
    StringbuilderAppendString(&logTextualBuffer, logMsgText[logs[dumpLogId].message]);
    StringbuilderAppendChar(&logTextualBuffer, ' ');
    StringbuilderAppendString(&logTextualBuffer, logCallerText[logs[dumpLogId].caller]);
    StringbuilderAppendChar(&logTextualBuffer, ' ');
    StringbuilderAppendU32(&logTextualBuffer, logs[dumpLogId].paramU32);
    StringbuilderAppendChar(&logTextualBuffer, ',');
    StringbuilderAppendDouble(&logTextualBuffer, logs[dumpLogId].paramDouble);
    StringbuilderAppendCrLf(&logTextualBuffer);
    LogClearEntry(dumpLogId);
}

void LogClearEntry(uint32_t id) {

    logs[id].logTime = 0;
    logs[id].priority = logInfo;
    logs[id].caller = LogCallerObjectSystem;
    logs[id].message = LogNoMessage;
    logs[id].paramU32 = 0;
    logs[id].paramDouble = 0.0;
}
/* *****************************************************************************
 End of File
 */
