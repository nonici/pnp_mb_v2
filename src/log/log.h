/* ************************************************************************** */
/*
  @File Name
    log.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef LOG_H    /* Guard against multiple inclusion */
#define LOG_H

// *****************************************************************************

#include <stdint.h>
#include <stdbool.h>

#include "enums.h"

// *****************************************************************************
/*
 * each log entry consists of: 
 * priority             error, info, warning, debug 
 * log id               ID of log, entry in array
 * time unix            not yet implemented
 * time ms              ms time extracted from sys timer
 * message              log message enum,  carries info and text of message
 * U32 parameter        
 * double parameter     
 * 
 *
 * To add new log:
 * 1. add log msg name to log_message_t (log.h)
 * 2. add log message text to logMsgText[]
 * 3. call log with "LogNewEntry(msg, priortiy, u32, double);"
 * 
 */
// *****************************************************************************

typedef enum {
    logError,
    logWarning,
    logInfo,
    logDebug,
} log_priority_t;

typedef enum {
    //Objects
    LogCallerObjectAxisX = 0,
    LogCallerObjectAxisY = 1,
    LogCallerObjectAxisZ = 2,
    LogCallerObjectAxisA = 3,
    LogCallerObjectAxisU = 4,
    LogCallerObjectAxisB = 5,
    LogCallerObjectAxisV = 6,
    LogCallerObjectAxisC = 7,
    LogCallerObjectAxisW = 8,
    LogCallerObjectAxisD = 9,
    LogCallerObjectAxisF = 10,
    LogCallerObjectAxisR = 11,
    LogCallerObjectParser = 12,
    LogCallerObjectSystem = 13,
    LogCallerObjectSlot = 14,
} log_caller_obj_t;

typedef enum {
    //General
    LogNoMessage,
    LogEventDuration,
    LogEStop,
    LogInvalidCommand,



    //Pneumatics
    LogSensorsRaw,
    LogSensorsReal,
    LogPWMPump,

    //Motion
    LogProfilerNewMotionDistance,
    LogProfilerNewMotionSpeed,
    LogProfilerNewMotionAcceleration,
    LogProfilerNewMotionDiscarded,
    LogProfilerNewMotionShort,
    LogProfilerNewMotionTrapezoidalShort,
    LogProfilerNewMotionTrapezoidalFull,
    LogProfilerExpectedToMove,
    LogProfilerMoved,
    LogProfilerMoveSuccessDistance,
    LogProfilerMoveSuccessTime,
    LogProfilerMoveFailedTimeOut,
    LogProfilerCurrentPosition,
    LogProfilerDestinationPosition,
    LogProfilerStartingPositionChanged,
    LogEngineAddedStep,
    LogEngineOverrun,
    LogEngineNotIdle,
    LogSystemInterruptOverrun,
    LogSystemFatalError,
    LogParserBufferOverflow,
    LogMovementEnabled,
    LogMovementUnhomingZ,
    LogMovementHomingZ,
    LogMovementUnhomingXY,
    LogMovementHomingXY,
    LogMovementHomingCompleted,
    LogMovementStepsToMakeThisBlock,
    LogSlotFeedOnSlotInititated,
    LogSlotFeedOnSlotComplete,
    LogSlotFeedOnSlotFailed,
} log_message_t;

// *****************************************************************************

//Variables

// *****************************************************************************

void LogInitialize(void);
void LogHandler(void);
void LogNewEntry(log_message_t msgId, log_caller_obj_t caller, log_priority_t prior, uint32_t par1, double par2);
void LogDumpRequest(log_priority_t priority);
void LogReporting(void);
void LogForcedDump(void);
// *****************************************************************************

#endif /* LOG_H */

/* *****************************************************************************
 End of File
 */
