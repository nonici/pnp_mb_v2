/* ************************************************************************** */
/*
  @File Name
    blocks.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef BLOCKS_H    /* Guard against multiple inclusion */
#define BLOCKS_H

// *****************************************************************************

#include "axes_struct.h"

// *****************************************************************************

extern motion_axis_t axis[NUMBER_OF_AXES];

// *****************************************************************************

void MovementBlockInitialize(axle_t a);
void MovementBlockAdd(axle_t a, double startingSpeed, double startingAccel, double startingJerk, int32_t steps, axle_states_t type) ;
void MovementBlockLoad(axle_t a);
void MovementBlockFinished(axle_t a);

// *****************************************************************************

#endif /* BLOCKS_H */

/* *****************************************************************************
 End of File
 */
