/* ************************************************************************** */
/*
  @File Name
    driver.h
 
  @Author
    Flitch

  @Summary
    Motion related hardware abstraction layer.
  
  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef DRIVER_H    /* Guard against multiple inclusion */
#define DRIVER_H

// *****************************************************************************

#include "enums.h"
#include "axes_struct.h"

// *****************************************************************************

extern motion_axis_t axis[NUMBER_OF_AXES];

// *****************************************************************************


// *****************************************************************************

//Functions

void DriverInitialize(void);

bool DriverIsActive(void);
//Returns true if no movement is queued or being performed
//Why tho?

void DriverAxisDirection(uint32_t a, bool inverseDirection);
//determine direction of movement

void DriverEnableAll(void);
void DriverDisableAll(void);
void DriverDisableXY(void);

void DriverEnableAxis(uint32_t a);
void DriverDisableAxis(uint32_t a);

void DriverSetActiveNozzle(uint32_t toolId);
nozzle_t DriverGetActiveNozzle(void);

double DriverGetHomeCoordinates(axle_t a);
double DriverGetHomingSpeed(axle_t a);

axle_t DriverGetAxleFromLetter(char letter);
void DriverSetCurrentCoordinatesReal(axle_t a, double val);
double DriverGetCurrentCoordinatesReal(axle_t a);
int32_t DriverGetMachineUnitsFromPositionFine(axle_t a, double position);
int32_t DriverGetMachineUnitsFromPositionRough(axle_t a, double position);
double DriverGetPositionFromMachineUnits(axle_t a, int32_t mUnits);

// *****************************************************************************

#endif /* DRIVER_H */

/* *****************************************************************************
 End of File
 */
