// *****************************************************************************
/*
  @File Name
    blocks.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "blocks.h"
#include "motion/driver.h"
#include "../enums.h"
#include "motion/profiler.h"
#include "assert.h"
#include "driver.h"

// *****************************************************************************

// *****************************************************************************

typedef struct {
    double blockSpeed;
    double blockAccHalf;
    double blockJerkSixth;
    int32_t blockStepsToMake;
    axle_states_t blockType;
} movement_block_t;

movement_block_t movementBlock[NUMBER_OF_AXES][NUMBER_OF_BLOCKS];
uint32_t blockHead[NUMBER_OF_AXES];
uint32_t blockTail[NUMBER_OF_AXES];

void MovementBlockInitialize(axle_t a) {
    //for next to clear whole array
    for (uint32_t i = 0; i == NUMBER_OF_BLOCKS; i++) {
        movementBlock[a][i].blockSpeed = 0;
        movementBlock[a][i].blockAccHalf = 0;
        movementBlock[a][i].blockJerkSixth = 0;
        movementBlock[a][i].blockStepsToMake = 0;
    }
    blockHead[a] = 0;
    blockTail[a] = 0;
}

void MovementBlockAdd(axle_t a, double startingSpeed, double startingAccel, double startingJerk, int32_t steps, axle_states_t type) {
    __conditional_software_breakpoint(startingSpeed >= 0);
    movementBlock[a][blockHead[a]].blockSpeed = startingSpeed;
    movementBlock[a][blockHead[a]].blockAccHalf = startingAccel / 2.0d;
    movementBlock[a][blockHead[a]].blockJerkSixth = startingJerk / 6.0d;
    __conditional_software_breakpoint(steps >= 0);
    movementBlock[a][blockHead[a]].blockStepsToMake = steps;
    movementBlock[a][blockHead[a]].blockType = type;
    blockHead[a]++;
    blockHead[a] %= NUMBER_OF_BLOCKS;
}

void MovementBlockLoad(axle_t a) {

    if (blockHead[a] == blockTail[a]) {
        axis[a].axisState = AXLE_IDLE;
        if (a == axisF || a == axisR) {
            DriverDisableAxis(axisF);
        } else {
            ProfilerAnalizeResults(a);
            axis[a].engRuningTimer = 0.0L;
        }
    } else {
        axis[a].engSpeedStartng = movementBlock[a][blockTail[a]].blockSpeed;
        axis[a].engAccHalf = movementBlock[a][blockTail[a]].blockAccHalf;
        axis[a].engJerkSixth = movementBlock[a][blockTail[a]].blockJerkSixth;
        axis[a].engTime = 0;
        axis[a].engPositionMM = axis[a].mmPerStep;
        axis[a].blockAxisState = movementBlock[a][blockTail[a]].blockType;
        axis[a].axisState = axis[a].blockAxisState;
        axis[a].stepsToMakeThisBlock = movementBlock[a][blockTail[a]].blockStepsToMake;
        axis[a].stepsMadeThisBlock = 0;
        blockTail[a]++;
        blockTail[a] %= NUMBER_OF_BLOCKS;
    }
}

void MovementBlockFinished(axle_t a) {
    axis[a].axisState = AXLE_IDLE;
}


/* *****************************************************************************
 End of File
 */
