/* ************************************************************************** */
/*
  @File Name
    engine.h
 
  @Author
    Flitch

  @Summary
    Engine core file.

  @Description
    Main engine interrupt and calculations
 */
/* ************************************************************************** */

#ifndef ENGINE_H    /* Guard against multiple inclusion */
#define ENGINE_H

// *****************************************************************************

#include "axes_struct.h"
#include "enums.h"

// *****************************************************************************

extern motion_axis_t axis[NUMBER_OF_AXES];

// *****************************************************************************

void EngineInitialize(void);
//Setting default parameters for axles

void Engine(void);
//started by periodic interrupt
//checks if axis is enabled and has pending movement
//executes movement map

#endif /* ENGINE_H */

/* *****************************************************************************
End of File
 */
