/* ************************************************************************** */
/*
  @File Name
    movement.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef MOVEMENT_H    /* Guard against multiple inclusion */
#define MOVEMENT_H

// *****************************************************************************

#include "enums.h"
#include "axes_struct.h"
#include "driver.h"
#include "engine.h"
#include "profiler.h"
#include "comm/comm.h"

// *****************************************************************************

extern motion_axis_t axis[NUMBER_OF_AXES];

// *****************************************************************************

typedef enum {
    MOTION_DISABLED,
    MOTION_UNHOMING_Z,
    MOTION_HOMING_Z,
    MOTION_HOMING_XY,
    MOTION_UNHOMING_XY,
    MOTION_IDLE,
    MOTION_ACTIVE,
    MOTION_ESTOP,
    MOTION_FAULT,
} motion_states_t;

// *****************************************************************************

void MotionHandler(void);
void MotionEnable(void);
void MotionDisable(void);
void MachineDisable(void);
void RetractZ(void);
bool MotionIsDisabled(void);
void MotionDisable(void);

// *****************************************************************************

#endif /* MOVEMENT_H */

/* *****************************************************************************
 End of File
 */
