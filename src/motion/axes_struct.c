// *****************************************************************************
/*
  @File Name
    axes_struct.h
 
  @Author
    Flitch

  @Summary
    Declares axis array of structs.

  @Description
    Declares axis array of structs.
 */
// *****************************************************************************

#include "axes_struct.h"

// *****************************************************************************

motion_axis_t axis[NUMBER_OF_AXES];

// *****************************************************************************

//Local function definitions

// *****************************************************************************

//Core function

//Global functions

//Local functions


/* *****************************************************************************
 End of File
 */
