// *****************************************************************************
/*
  @File Name
    profiler.h
 
  @Author
    Flitch

  @Summary
    Motion related profile calculation and planing.
  
  @Description
    Calculation of motion parameters (speeds, distances, timings).
 */
// *****************************************************************************

#include "profiler.h"
#include "../comm/gcode.h"
#include "blocks.h"
#include "driver.h"
#include <fdlmath.h>
#include <math.h>
#include <assert.h>
#include "../log/log.h"

// *****************************************************************************

double speedRatingFactor = 1.0;

// *****************************************************************************

void ProfilerPeelFrontForDegrees(double angle) {
    axis[axisF].currentPositionMachineUnits = 0.0;
    ProfilerGenerateMovementMap(axisF, angle, MOVE_LINE);
    DriverEnableAxis(axisF);
}

void ProfilerPeelRearForDegrees(double angle) {
    axis[axisR].currentPositionMachineUnits = 0.0;
    ProfilerGenerateMovementMap(axisR, angle, MOVE_LINE);
    DriverEnableAxis(axisF);
}

void ProfilerLoadDestination(char letter, double val) {
    //ProfilerCalculatePath(a, val);
    for (uint32_t a = 0; a < NUMBER_OF_AXES; a++) //for all axes
    {
        if (letter == axis[a].axisLetter) {
            axis[a].destinationPosition = val; //For later use
            axis[a].isMovementPending = 1; //For later use
        }
    }
}

void ProfilerRun(void) {
    for (uint32_t a = 0; a < NUMBER_OF_AXES; a++) //for all axes
    {
        if (axis[a].isMovementPending) {
            axis[a].isMovementPending = 0; //For later use
            ProfilerGenerateMovementMap(a, axis[a].destinationPosition, MOVE_LINE);
        }
    }
}

void ProfilerSetSpeedPercent(double speed) {
    __conditional_software_breakpoint(speedRatingFactor > 0);
    speedRatingFactor = speed / 100.0L;
    if (speedRatingFactor < 0.01L) {
        speedRatingFactor = 0.01L;
    }
    if (speedRatingFactor > 1.0L) {
        speedRatingFactor = 1.0L;
    }
}

///Calculates distance and direction of linear and rotational moves

void ProfilerCalculatePath(axle_t a, double val) {
    /*
//
//                        ^  
//                        |
//                      speed 
//                        |            
//                        |       +---------------+ <- max_speed 
//                        |     / |               | \   
//                        |    /  |               |  \
//                        |   /   |               |   \  
//                        |  /    |               |    \
//                        | /     |               |     \
//                        |/      |               |      \  
//           min_speed -> +       |               |       + <- exit_speed 
//                        |       |               |       |
//                        +-------+---------------+-------+-----time -->  
//                        |  T1   |               |   T3  |
//                                ^               ^
//    key positions:accelerate_until  |               | decelerate_after(in mm)
//    isCruising:             | false |      true     | false |
//    isAccel:                | true  |       X       | false |
//    entrySpeed=             +Vmin   +Vmax         +Vmax  
//
     */

    //    ProfilerDetermineAxis
    if (a < 4) {
        //this is axis

        //if not X or Y
        if (a > 1) {
            //WTF DID I JUST WROTE????

            a = (a - 2) + (2 * DriverGetActiveNozzle());
        }
        //remove later
        //        while (axis[a].axisState != AXLE_IDLE) {
        //        }
        ProfilerGenerateMovementMap(a, val, MOVE_LINE);
    }
    //Check for Stop button
    //    if (GetStop()) {
    //        return;
    //    }

    //  To add:
    //  -Movement types: default/automatic, homing, cruising (constant speed), trapezoidal (acceleration), S curve(jerk)
    //  -Argument to fn to be struct of all movements to perform in parallel (X, Y, Z, A, F) or (Z1, Z2, Z3, Z4, F)?
    //  -Lock of 25 ms between previous and next movement
    //
    //
    //
    //
    //
    //

}

void ProfilerSetAxisSpeed(axle_t a, double speed) {
}

void ProfilerGenerateMovementMap(axle_t a, double targetPos, motion_type_t type) {

    __conditional_software_breakpoint(targetPos > -1000.0d && targetPos < 1000.0d);
    __conditional_software_breakpoint(!isnan(targetPos));
    LogNewEntry(LogProfilerDestinationPosition, a, logInfo, 0, targetPos);
    if ((axis[a].axisState != AXLE_IDLE)) {
        LogNewEntry(LogEngineNotIdle, a, logError, 0, 0);
        GcodeRegexErrorFatalError();
        return;
    }
    //Plan a line movement

    //Clear step counter
    axis[a].totalStepsMoved = 0;

    //
    //Finds target position, distance to travel, and direction in both MM and MU
    //

    //Local parameters
    //curentPosMU               //position of axis at start of algorithm
    //currentPosMM              //          ...         in mm
    //targetPos                 //ideal target position, argument of function
    //targetPosMM               //closest target position to ideal, multiple of steps
    //targetPosMU               //closest target position to ideal in steps
    //totalMoveDistanceMU       //difference of currentPosMU and targetPosMU
    //totalMoveDistanceMM       //totalMoveDistanceMU converted to mm, multiple of steps
    //direction                 //1 or 0, determinate direction of movement

    int32_t curentPosMU = axis[a].currentPositionMachineUnits;
    int32_t targetPosMU = DriverGetMachineUnitsFromPositionFine(a, targetPos);

#ifdef PNP_DEV_MOTION
    //Used only in debug
    double curentPosMM = DriverGetPositionFromMachineUnits(a, curentPosMU);
    double targetPosMM = DriverGetPositionFromMachineUnits(a, targetPosMU);
#endif /*PNP_DEV_MOTION*/

    int32_t totalMoveDistanceMU;
    double totalMoveDistanceMM;

    //Find distance and set driver direction
    //assume positive direction (X: Left->Right,Y: Front->Rear,Z: Down->Up, A: CCW, F: Inside->Out)
    bool direction = 1;

    //Find exact number of steps to move from current position to destination position
    if (curentPosMU <= targetPosMU) {
        totalMoveDistanceMU = targetPosMU - curentPosMU;
    } else {
        direction = 0;
        totalMoveDistanceMU = curentPosMU - targetPosMU;
    }
    //Set driver direction to previously calculated value
    DriverAxisDirection(a, direction);

    totalMoveDistanceMM = DriverGetPositionFromMachineUnits(a, totalMoveDistanceMU);
    axis[a].totalStepsToMove = totalMoveDistanceMU;

    if (type == MOVE_LINE) {

#ifdef PNP_DEV_MOTION
        //Debug section    
        //Log movement details
        LogNewEntry(LogProfilerCurrentPosition, a, logInfo, curentPosMU, curentPosMM);
        LogNewEntry(LogProfilerDestinationPosition, a, logInfo, targetPosMU, targetPosMM);
        LogNewEntry(LogProfilerNewMotionDistance, a, logInfo, totalMoveDistanceMU, totalMoveDistanceMM);
#endif /*PNP_DEV_MOTION*/

        //Discard movements shorter than one step
        if (totalMoveDistanceMU == 0) {
            axis[a].axisState = AXLE_IDLE;

#ifdef PNP_DEV_MOTION
            //Log discarded move
            LogNewEntry(LogProfilerNewMotionDiscarded, a, logDebug, totalMoveDistanceMU, totalMoveDistanceMM);
#endif /*PNP_DEV_MOTION*/
            return;
        }

        double speedFactor = speedRatingFactor;

        //Peeler always runs on maximum speed
        if (a == axisF || a == axisR) {
            speedFactor = 1.0;
        }

        //Default values for calculations, maknut ovaj global
        double timeToAccel = 0;
        double distanceToChange = 0;
        double speedMinScaled = axis[a].speedMinDefault * speedFactor;
        double speedMaxScaled = axis[a].speedMaxDefault *speedFactor;
        double accelerationScaled = axis[a].accelerationDefault * speedFactor * speedFactor;

        //Avoid devision by zero
        if (accelerationScaled < 1.0d) {
#ifdef PNP_DEV_MOTION
            LogNewEntry(LogProfilerNewMotionAcceleration, a, logDebug, a, accelerationScaled);
#endif /*PNP_DEV_MOTION*/
            accelerationScaled = 1.0d;
        }
        //Determine movement profile

        //For moves shorter than 100 steps, use slow constant speed
        if (totalMoveDistanceMU < 21) {
            //Push move to engine queue
            MovementBlockAdd(a, (speedMaxScaled * 0.02), 0, 0, totalMoveDistanceMU, AXLE_MOVING);
#ifdef PNP_DEV_MOTION
            //Log move as short motion
            LogNewEntry(LogProfilerNewMotionShort, a, logInfo, totalMoveDistanceMU, totalMoveDistanceMM);
#endif /*PNP_DEV_MOTION*/
        } else {
            //Trapezoidal move (normal or short)

            //Local parameters

            //Check if full trapezoid values are usable
            timeToAccel = (speedMaxScaled - speedMinScaled) / accelerationScaled;
            distanceToChange = speedMinScaled * timeToAccel + (accelerationScaled * timeToAccel * timeToAccel / 2.0d);

#ifdef PNP_DEV_MOTION
            //Prepare message to log
            log_message_t logTrapezoidal = LogProfilerNewMotionTrapezoidalFull;
#endif /*PNP_DEV_MOTION*/

            if (totalMoveDistanceMM < (3 * distanceToChange)) {
                //Full trapezoid not available, calculate short version

                //Calculate new maximum achievable speed

                speedMaxScaled = sqrt(speedMinScaled * speedMinScaled + accelerationScaled * totalMoveDistanceMM * 2.0d / 3.0d);
                __conditional_software_breakpoint(speedMaxScaled > 0.0d);
                //Recalculate time to acceleration with new speeds
                timeToAccel = (speedMaxScaled - speedMinScaled) / accelerationScaled;

                //Recalculate distance to change with new speeds and acceleration times
                distanceToChange = speedMaxScaled * timeToAccel - (accelerationScaled * timeToAccel * timeToAccel / 2);

#ifdef PNP_DEV_MOTION
                logTrapezoidal = LogProfilerNewMotionTrapezoidalShort;
#endif /*PNP_DEV_MOTION*/

            }

#ifdef PNP_DEV_MOTION
            //Log trapezoid parameters
            LogNewEntry(logTrapezoidal, a, logDebug, totalMoveDistanceMU, totalMoveDistanceMM);
            LogNewEntry(LogProfilerNewMotionSpeed, a, logDebug, a, speedMaxScaled);
            LogNewEntry(LogProfilerNewMotionAcceleration, a, logDebug, a, accelerationScaled);
#endif /*PNP_DEV_MOTION*/

            //Number of steps for movement blocks

            int32_t noOfStepsPerRamp = DriverGetMachineUnitsFromPositionRough(a, distanceToChange);
            int32_t noOfStepsPerCruise = totalMoveDistanceMU - noOfStepsPerRamp - noOfStepsPerRamp;

            //Push movement blocks to queue
            //Acceleration
            MovementBlockAdd(a, speedMinScaled, accelerationScaled, 0, noOfStepsPerRamp, AXLE_MOVING);
            //Cruising
            MovementBlockAdd(a, speedMaxScaled, 0, 0, noOfStepsPerCruise, AXLE_MOVING);
            //Deceleration
            MovementBlockAdd(a, speedMaxScaled, -accelerationScaled, 0, noOfStepsPerRamp, AXLE_MOVING);

        }

        //Start movement by loading first block
        int32_t newPosMU = axis[a].currentPositionMachineUnits;
        if (curentPosMU == newPosMU) {
            MovementBlockLoad(a);
        } else {
            __conditional_software_breakpoint(curentPosMU == newPosMU);
            double newPosMM = DriverGetPositionFromMachineUnits(a, newPosMU);
            LogNewEntry(LogProfilerStartingPositionChanged, a, logError, newPosMU, newPosMM);
            GcodeRegexErrorFatalError();
        }

    } else if (type == MOVE_HOMING) {
        MovementBlockAdd(a, DriverGetHomingSpeed(a), 0, 0, 0, AXLE_HOMING);
        MovementBlockLoad(a);
    }
}

void ProfilerAnalizeResults(axle_t a) {
    if (axis[a].totalStepsMoved != axis[a].totalStepsToMove) {
        LogNewEntry(LogProfilerExpectedToMove, a, logWarning, axis[a].totalStepsToMove, (axis[a].totalStepsToMove * axis[a].mmPerStep));
    }
    if (axis[a].engRuningTimer > ENGINE_TIMEOUT_PERIOD_TICKS) {
        LogNewEntry(LogProfilerExpectedToMove, a, logWarning, axis[a].totalStepsToMove, (axis[a].totalStepsToMove * axis[a].mmPerStep));
    }
    LogNewEntry(LogProfilerMoveSuccessDistance, a, logInfo, axis[a].totalStepsMoved, (axis[a].totalStepsMoved * axis[a].mmPerStep));
    LogNewEntry(LogProfilerCurrentPosition, a, logInfo, axis[a].currentPositionMachineUnits, (axis[a].currentPositionMachineUnits * axis[a].mmPerStep));
    LogNewEntry(LogProfilerMoveSuccessDistance, a, logInfo, axis[a].engRuningTimer, (((double) axis[a].engRuningTimer) * TICK_COUNTER_PERIOD_SECONDS));
}

/* *****************************************************************************
 End of File
 */
