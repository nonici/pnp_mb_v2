// *****************************************************************************
/*
  @File Name
    engine.c
 
  @Author
    Flitch

  @Summary
    Engine core file.

  @Description
    Main engine interrupt
 */
// *****************************************************************************

#include <assert.h>

#include "engine.h"
#include "blocks.h"
#include "driver.h"
#include "movement.h"

#include "../log/log.h"
#include "../pnp/signalers.h"

// *****************************************************************************

uint32_t stepBitMap = 0;

// *****************************************************************************

// *****************************************************************************

void EngineInitialize(void) {

    DriverInitialize();

    //Initialize Engine Timer
    TMR2_Start();

}

void Engine(void) {
    //This interrupt has been measured to last up to 1.5 us in debug mode, and up to 1.2 us in normal mode,
    //when using maximum of 4 axes with 3rd order position calculation (jerk)
    //This interrupt must not be shorter than 2 us, because motor optocouplers and motor drivers 
    //require minimum of 2 us of high, and 2 us of low signal,
    //It is recommended however not to reduce it under 3 us, 
    //giving it maximum possible speed rating of 166 666.6 pulses per second 

    //Current interrupt settings is 3 us, and is visible in driver.h "TICK_COUNTER_PERIOD_SECONDS" definition

    //minimum STEP signal timing diagram for: DRV8825, TB6600, and JSS57P2N 
    //
    //                 _______         _______
    //           _____/       \_______/       \_______/
    //                |       |       |       |       |
    //                | 1.9uS | 1.9uS |
    //                |               |
    //                |     4 uS      |
    // 
    // Step frequency:  250 kHz for DRV8825, JSS57P2N; 200 kHz for TB6600 
    // Step period:     4 uS MIN
    // Step High:       1.9 uS MIN
    // Step Low:        1.9 uS MIN
    // Setup time       650 ns MIN      //delay between DIR/MODE and STEP
    // wakeup time      1.7 mS MAX      //delay between ENABLE and STEP
    //
    //Since every pulse that is calculated in *current* interrupt, its 
    //rising edge will be executed in *N+1* interrupt, and it's falling 
    //edge executed in *N+2* interrupt, only timing restriction is minimum 
    //interrupt duration.

#ifdef PNP_DEV_MOTION
    //Debug section    
    //SIG_LED_G_Set(); //scope measurement signal
#endif /*PNP_DEV_MOTION*/

    if (MotionIsDisabled()) {
        return;
    }

    //Clear last steps
    //Clear step pins before stepping, ideally this should be called 10 us after stepping and called by unstep timer, but its not
    GPIO_PortClear(GPIO_PORT_J, STEP_MASK);

    //Execute pending steps
    GPIO_PortSet(GPIO_PORT_J, stepBitMap);

    //Clear stepBitMap before new calculations
    stepBitMap = 0;

    //Pass once for each axis
    for (uint32_t a = 0; a < NUMBER_OF_AXES; a++) //for all axes
    {
        //Skip inactive axes
        if (axis[a].axisState == AXLE_IDLE) {
            continue;
        }

        //increment timers
        axis[a].engTime += TICK_COUNTER_PERIOD_SECONDS;
        axis[a].engRuningTimer++;

        //Check if current axis is taking to long to finish
        if (axis[a].engRuningTimer > ENGINE_TIMEOUT_PERIOD_TICKS) {

        }

        //Calculates current position with given parameters:
        //      Position derivates
        //      1st order - velocity
        //      2nd order - acceleration
        //      3rd order - jerk
        //      Steal rest from marlin fw when coordinated move is developed
        //      4th order - snap
        //      5th order - crackle
        //      6th order - pop
        //
        //      X = Position
        //      X_i = Initial position, not used by algorithm, could be used to shift whole movement back for half step, to reduce rounding errors
        //      t = current time
        //      V_i = Initial velocity
        //      a = acceleration
        //      j = jerk
        //      
        //      Constant speed (Line)
        //      X = X_0 + V_i*t
        //
        //      Accelerating move (Trapezoidal)
        //      X = X_0 + V_i*t + (a*t^2)/2
        //
        //      Jerk accelerated move (S curve)
        //      X = X_0 + V_i*t + (a*t^2)/2 - (j*t^3)/6
        //
        //      For simpler calculation, we dont use X_0 and real coordinates,
        //      but virtualy maped coordinates, always starting from zero position,
        //      always moving in positive direction,
        //      always starting from zero time.
        //

        //Engine parameters
        double par_t = axis[a].engTime;
        double par_t2 = par_t * axis[a].engTime;
        double par_t3 = par_t2 * axis[a].engTime;
        double par_vi = axis[a].engSpeedStartng;
        double par_a_half = axis[a].engAccHalf; //should prepare it outside of interrupt to be A/2
        double par_j_sixth = axis[a].engJerkSixth; //should prepare it outside of interrupt to be J/6

        //Parameters par_a_half (A/2) and par_j_sixth (J/6) are prepared (divided)
        //when being saved to block.

        //To avoid overlapping steps on block transitions and keep time delays correct,
        //every block starts at 1 step distance. This value is prepared when loading next block.

        //Position formula
        double par_X = (par_vi * par_t) + (par_a_half * par_t2) - (par_j_sixth * par_t3);

        //This section adds one step when rounding errors happen at end of deceleration phase.
        //When (par_a_half * par_t2)<(par_vi * par_t), current position should be 0, but is rounded to negative number
        //Additional check of (axis[a].stepsToMakeThisBlock != 0) is performed.
        if (par_X < 0) {
            if ((axis[a].stepsToMakeThisBlock - axis[a].stepsMadeThisBlock) > 0) {
                par_X = axis[a].engPositionMM + axis[a].mmPerStep;
                LogNewEntry(LogEngineAddedStep, a, logInfo, a, 0.0);
            }
        }

        //Checks newly calculated position is greater than current position, and requests a move
        //This can be very small displacement, introducing movement errors
        //Should add half step to engine position, to round errors
        if (par_X < axis[a].engPositionMM) {
            continue;
        }

        //Flags current axis to step at start of next engine interrupt and updates it's position (both engine, and global)
        stepBitMap |= axis[a].stepMask;
        axis[a].engPositionMM = axis[a].engPositionMM + axis[a].mmPerStep;
        axis[a].currentPositionMachineUnits = axis[a].currentPositionMachineUnits + axis[a].directionSigned;
        //Used only for analyzing, should be removed and use global machine position
        axis[a].totalStepsMoved++;
        axis[a].stepsMadeThisBlock++;
        //Checks if this movement block is finished, and loads next one

        //For normal movement, grab next block, if this one is finished and more blocks are queued
        if (axis[a].blockAxisState == AXLE_MOVING) {
            //get next block if current one is finished
            if (axis[a].stepsToMakeThisBlock <= axis[a].stepsMadeThisBlock) {
#ifdef PNP_DEV_MOTION
                if (axis[a].stepsToMakeThisBlock < axis[a].stepsMadeThisBlock) {
                    LogNewEntry(LogMovementStepsToMakeThisBlock, a, logError, (axis[a].stepsToMakeThisBlock - axis[a].stepsMadeThisBlock), 0.0L);
                }
#endif /*PNP_DEV_MOTION*/
                MovementBlockLoad(a);
            }
            continue;
        }

        //For homing movement, check homing pin and set parameter when finished
        if (axis[a].blockAxisState == AXLE_HOMING) {

            if ((GPIO_PinRead(axis[a].homePin)) ^ (axis[a].invertHomePin)) {
                DriverSetCurrentCoordinatesReal(a, DriverGetHomeCoordinates(a));
                axis[a].axisState = AXLE_HOMED;
                axis[a].isHomed = 1;
                //Load next block, if any available
                MovementBlockLoad(a);
            }
        }

    }

#ifdef PNP_DEV_MOTION
    //Debug section
    //SIG_LED_G_Clear(); //scope measurement signal
#endif /*PNP_DEV_MOTION*/
}

/* *****************************************************************************
 End of File
 */
