// *****************************************************************************
/*
  @File Name
    movement.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "movement.h"
#include "blocks.h"

#include "../comm/gcode.h"
#include "../log/log.h"

#include "../pnp/signalers.h"
#include "../pnp/psu.h"

// *****************************************************************************

#define UNHOMING_DISPLACEMENT               4.0d

// *****************************************************************************

motion_states_t motionStateMachine = MOTION_DISABLED;

// *****************************************************************************

void HomeAxis(axle_t a, double destination);
void UnhomeZ(void);
void HomeZ(void);
void UnhomeXY(void);
void HomeXY(void);

// *****************************************************************************

void MotionHandler(void) {
    //Do not enter if Estop is pressed
    if (EmergencyStopUpdateAndGet()) {
        motionStateMachine = MOTION_DISABLED;
        return;
    }

    switch (motionStateMachine) {
        case MOTION_DISABLED:
            //Auto enable and home machine
            MotionEnable();
            break;
        case MOTION_UNHOMING_Z:
            //Move Z axes out of sensor field of view
            if (!DriverIsActive()) {
                HomeZ();
                motionStateMachine = MOTION_HOMING_Z;
            }
            break;
        case MOTION_HOMING_Z:
            //Wait until Z homing is dome
            if ((!axis[axisZ].isEnabled || axis[axisZ].isHomed) && (!axis[axisU].isEnabled || axis[axisU].isHomed) && (!axis[axisV].isEnabled || axis[axisV].isHomed) && (!axis[axisW].isEnabled || axis[axisW].isHomed)) {
                UnhomeXY();
                motionStateMachine = MOTION_UNHOMING_XY;
            }
            break;
        case MOTION_UNHOMING_XY:
            //Move axes out of sensor field of view
            if (!DriverIsActive()) {
                HomeXY();
                motionStateMachine = MOTION_HOMING_XY;
            }
            break;
        case MOTION_HOMING_XY:
            if ((!axis[axisX].isEnabled || axis[axisX].isHomed) && (!axis[axisY].isEnabled || axis[axisY].isHomed)) {
                motionStateMachine = MOTION_IDLE;
                LogNewEntry(LogMovementHomingCompleted, LogCallerObjectSystem, logInfo, 0, 0.0d);
                GcodeRegexHomeComplete();
            }
            break;
        case MOTION_IDLE:
            //if sequential movement is pending, execute it
            break;

        case MOTION_ACTIVE:
            //if nonsequential movement is pending, execute it
            //if sequential movement is pending, return and ignore for now
            break;

        case MOTION_ESTOP:
            //ignore all movementes when ESTOP is pressed
            break;
        case MOTION_FAULT:
            //ignore all movementes when ESTOP is pressed
            break;
    }
}

void MotionEnable(void) {
    //if (motionStateMachine == MOTION_DISABLED) {
    //Set motion parameters and Home all enabled Z axes
    //Clear movement blocks
    uint32_t a = NUMBER_OF_AXES;
    DriverEnableAll();
    while (a--) {
        MovementBlockInitialize(a);
    }
    UnhomeZ();
    motionStateMachine = MOTION_UNHOMING_Z;
}

void MotionDisable(void) {

}

bool MotionIsDisabled(void) {
    return (motionStateMachine == MOTION_DISABLED);
}

void UnhomeZ(void) {
    LogNewEntry(LogMovementUnhomingZ, LogCallerObjectSystem, logInfo, 0, 0.0L);
    axis[axisZ].isHomed = 0;
    axis[axisU].isHomed = 0;
    axis[axisV].isHomed = 0;
    axis[axisW].isHomed = 0;
    DriverSetCurrentCoordinatesReal(axisZ, 0.0L);
    DriverSetCurrentCoordinatesReal(axisU, 0.0L);
    DriverSetCurrentCoordinatesReal(axisV, 0.0L);
    DriverSetCurrentCoordinatesReal(axisW, 0.0L);
    if (axis[axisZ].isEnabled) ProfilerGenerateMovementMap(axisZ, (DriverGetCurrentCoordinatesReal(axisZ) - UNHOMING_DISPLACEMENT), MOVE_LINE);
    if (axis[axisU].isEnabled) ProfilerGenerateMovementMap(axisU, (DriverGetCurrentCoordinatesReal(axisU) - UNHOMING_DISPLACEMENT), MOVE_LINE);
    if (axis[axisV].isEnabled) ProfilerGenerateMovementMap(axisV, (DriverGetCurrentCoordinatesReal(axisV) - UNHOMING_DISPLACEMENT), MOVE_LINE);
    if (axis[axisW].isEnabled) ProfilerGenerateMovementMap(axisW, (DriverGetCurrentCoordinatesReal(axisW) - UNHOMING_DISPLACEMENT), MOVE_LINE);
}

void HomeZ(void) {
    LogNewEntry(LogMovementHomingZ, LogCallerObjectSystem, logInfo, 0, 0.0L);
    //Set asix, home position, target position, and speed
    HomeAxis(axisZ, 100.0L);
    HomeAxis(axisU, 100.0L);
    HomeAxis(axisV, 100.0L);
    HomeAxis(axisW, 100.0L);
}

void UnhomeXY(void) {
    LogNewEntry(LogMovementUnhomingXY, LogCallerObjectSystem, logInfo, 0, 0.0L);
    axis[axisX].isHomed = 0;
    axis[axisY].isHomed = 0;
    if (axis[axisX].isEnabled) ProfilerGenerateMovementMap(axisX, (DriverGetCurrentCoordinatesReal(axisX) + UNHOMING_DISPLACEMENT), MOVE_LINE);
    if (axis[axisY].isEnabled) ProfilerGenerateMovementMap(axisY, (DriverGetCurrentCoordinatesReal(axisY) + UNHOMING_DISPLACEMENT), MOVE_LINE);
}

void RetractZ(void) {
    UnhomeZ();
    while (DriverIsActive());
    HomeZ();
}

void HomeXY(void) {
    LogNewEntry(LogMovementHomingXY, LogCallerObjectSystem, logInfo, 0, 0.0d);
    //Set axis and home target position
    HomeAxis(axisX, -700.0L);
    HomeAxis(axisY, -700.0L);
}

void HomeAxis(axle_t a, double destination) {
    //Set speed, direction, target position, and start moving Z axes
    if ((axis[a].isEnabled) && (axis[a].homePin != GPIO_PIN_NONE)) {
        ProfilerGenerateMovementMap(a, destination, MOVE_HOMING);
    }
}

void MachineDisable(void) {
    //Lower all Z and turn off PSU
    ProfilerSetSpeedPercent(50);
    if (axis[axisZ].isEnabled) ProfilerGenerateMovementMap(axisZ, (0), MOVE_LINE);
    if (axis[axisU].isEnabled) ProfilerGenerateMovementMap(axisU, (0), MOVE_LINE);
    if (axis[axisV].isEnabled) ProfilerGenerateMovementMap(axisV, (0), MOVE_LINE);
    if (axis[axisW].isEnabled) ProfilerGenerateMovementMap(axisW, (0), MOVE_LINE);
    CORETIMER_DelayMs(3000);
    if (axis[axisZ].isEnabled) ProfilerGenerateMovementMap(axisZ, (-4), MOVE_LINE);
    if (axis[axisU].isEnabled) ProfilerGenerateMovementMap(axisU, (-4), MOVE_LINE);
    if (axis[axisV].isEnabled) ProfilerGenerateMovementMap(axisV, (-4), MOVE_LINE);
    if (axis[axisW].isEnabled) ProfilerGenerateMovementMap(axisW, (-4), MOVE_LINE);
    CORETIMER_DelayMs(1000);
    PowerSupplyOff();
}

/* *****************************************************************************
 End of File
 */
