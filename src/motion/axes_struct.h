/* ************************************************************************** */
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Declares axis array of structs.

  @Description
    Declares axis array of structs and default values for axes.
 */
/* ************************************************************************** */

#ifndef AXES_STRUCT_H    /* Guard against multiple inclusion */
#define AXES_STRUCT_H

// *****************************************************************************

#include "stdbool.h"
#include "stdint.h"
#include "definitions.h"
#include "enums.h"

// *****************************************************************************

//#define SLOW_MOTION_PROFILE
#define FAST_MOTION_PROFILE
//#define MAX_SPEED_MOTION_PROFILE

//Steping Axis     Axis port mask       Step pins
#define STEP_X          0x0004          //RJ2
#define STEP_Y          0x0003          //RJ0 + RJ1
#define STEP_YL         0x0002          //RJ1
#define STEP_YR         0x0001          //RJ0
#define STEP_A1         0x0040          //RJ6
#define STEP_A2         0x0080          //RJ7
#define STEP_B1         0x0100          //RJ8
#define STEP_B2         0x0200          //RJ9
#define STEP_C1         0x1000          //RJ12
#define STEP_C2         0x0400          //RJ10
#define STEP_D1         0x0800          //RJ11
#define STEP_D2         0x2000          //RJ13
#define STEP_E1         0x4000          //RJ14
#define STEP_E2         0x8000          //RJ15

#define STEP_MASK       0xFFC7

#define ENGINE_TIMEOUT_PERIOD_TICKS   5000000            //15 seconds at 3 us per tick

//#define TICK_COUNTER_PERIOD_SECONDS     0.000008L      //8us
//#define TICK_COUNTER_PERIOD_SECONDS     0.000006L      //6us
//#define TICK_COUNTER_PERIOD_SECONDS     0.000004L      //4us
#define TICK_COUNTER_PERIOD_SECONDS     0.000003L        //3us
//#define TICK_COUNTER_PERIOD_SECONDS     0.000002L      //2us

#define SETTINGS_SPEED_MIN_X            0.0L
#define SETTINGS_SPEED_MIN_Y            0.0L
#define SETTINGS_SPEED_MIN_Z            0.0L
#define SETTINGS_SPEED_MIN_A            0.0L
#define SETTINGS_SPEED_MIN_F            0.0L

#ifdef SLOW_MOTION_PROFILE

#define SETTINGS_SPEED_MAX_X            1200.0L             
#define SETTINGS_SPEED_MAX_Y            400.0L              
#define SETTINGS_SPEED_MAX_Z            500.0L              
#define SETTINGS_SPEED_MAX_A            150.0L              
#define SETTINGS_SPEED_MAX_F            500.0L              

#define SETTINGS_ACCELERATION_X         5000.0L             
#define SETTINGS_ACCELERATION_Y         800.0L              
#define SETTINGS_ACCELERATION_Z         1500.0L              
#define SETTINGS_ACCELERATION_A         250.0L            
#define SETTINGS_ACCELERATION_F         250.0L

#endif //SLOW_MOTION_PROFILE

#ifdef FAST_MOTION_PROFILE

#define SETTINGS_SPEED_MAX_X            1500.0L
#define SETTINGS_SPEED_MAX_Y            450.0L
#define SETTINGS_SPEED_MAX_Z            750.0L
#define SETTINGS_SPEED_MAX_A            250.0L
#define SETTINGS_SPEED_MAX_F            200.0L

#define SETTINGS_ACCELERATION_X         5000.0L
#define SETTINGS_ACCELERATION_Y         900.0L
#define SETTINGS_ACCELERATION_Z         1750.0L
#define SETTINGS_ACCELERATION_A         1000.0L
#define SETTINGS_ACCELERATION_F         200.0L

#endif //FAST_MOTION_PROFILE

#ifdef MAX_SPEED_MOTION_PROFILE

#define SETTINGS_SPEED_MAX_X            1500.0L
#define SETTINGS_SPEED_MAX_Y            450.0L
#define SETTINGS_SPEED_MAX_Z            2000.0L
#define SETTINGS_SPEED_MAX_A            400.0L
#define SETTINGS_SPEED_MAX_F            250.0L

#define SETTINGS_ACCELERATION_X         7500.0L
#define SETTINGS_ACCELERATION_Y         1000.0L
#define SETTINGS_ACCELERATION_Z         4000.0L
#define SETTINGS_ACCELERATION_A         1500.0L
#define SETTINGS_ACCELERATION_F         2500.0L

#endif //MAX_SPEED_MOTION_PROFILE

//#define SETTINGS_MM_PER_STEP_X          0.0375L
#define SETTINGS_MM_PER_STEP_X          0.01875L
#define SETTINGS_MM_PER_STEP_Y          0.01875L
#define SETTINGS_MM_PER_STEP_Z          0.020625L
#define SETTINGS_MM_PER_STEP_A          0.1125L
#define SETTINGS_MM_PER_STEP_F          0.1125L

#define SETTINGS_X_HOME_SPEED           100.0L
#define SETTINGS_Y_HOME_SPEED           50.0L
#define SETTINGS_Z_HOME_SPEED           50.0L

#define SETTINGS_X_HOME_COORDINATES     0.0L
#define SETTINGS_Y_HOME_COORDINATES     0.0L
#define SETTINGS_Z_HOME_COORDINATES     40.0L
#define SETTINGS_A_HOME_COORDINATES     0.0L
#define SETTINGS_F_HOME_COORDINATES     0.0L

#define NUMBER_OF_AXES                  12
#define NUMBER_OF_MAIN_AXES             10
#define NUMBER_OF_BLOCKS                5

// *****************************************************************************

typedef struct {
    //Axis settings and flags, fixed or rarely set values
    bool isEnabled; //Motor driver is enabled & initialized
    bool isEmergencyStopable; //settings, is axis affected by Estop(only: X,Yleft,Yright)
    bool ignoreLocks; //settings, cant move until other axes finish (only: A1-A4, Ffront, Frear)
    bool isCircular; //settings, can overflow max coordinates, i.e. after 360� comes 0� (only: A1-A4, Ffront, Frear)
    bool invertDirPin; //settings, invert direction pin if true, default DIR != PNP DIR
    bool invertEnPin; //settings invert enable pin polarity, true for ActiveLow drivers
    bool invertHomePin; //settings invert home pin polarity, true for ActiveLow sensors
    bool isHomed; //Axis is homed 
    bool isMovementPending; //Movement pending, value in destinationPosition
    char axisLetter; //Axisl letter symbol, uppercase (X, Y, Z ...)
    double mmPerStep; //Movement of X mm per step, depends on driver microstepping settings

    //Control pins 
    GPIO_PIN dirPin; //Pin for controlling axis direction
    GPIO_PIN enPin; //Pin for enabling or disabling axis
    GPIO_PIN homePin; //Homing sensor pin
    uint32_t stepMask; //Port J step mask, will pulse all unmasked (high) pins, clears them next engine interrupt

    //default values used for calculation
    double destinationPosition; //Destination position for next move
    double speedMinDefault; //Starting and stopping speed   
    double speedMaxDefault; //Maximum allowed speed
    double accelerationDefault; //Maximum allowed acceleration
    double jerkDefault; //Maximum allowed jerk

    //Axis settings and flags, not fixed
    axle_states_t axisState; //Current state of axis, mask IDLE axes from engine algorithm, locks active (!IDLE) axes from movement planer algorithm
    int32_t currentPositionMachineUnits; //Current position in steps

    //Active engine parameters, loaded from movement block for dynamic calculations of current engine position
    double engSpeedStartng; //Starting speed 
    double engAccHalf; //Acceleration/2
    double engJerkSixth; // Jerk/6
    double engPositionMM; //current engine position
    double engTime; //Current time, incremented by clock ticker 
    uint32_t engRuningTimer; //Timer running while axis is active, used for timeout
    int32_t directionSigned; //+1 for positive or for negative direction -1, for accumulation of steps
    int32_t stepsToMakeThisBlock; //Marks end of block
    int32_t stepsMadeThisBlock; //Counting until end of current block
    axle_states_t blockAxisState; //Type of block, used in engine switcher

    int32_t totalStepsToMove; //Used for movement analysis after finishing movement
    int32_t totalStepsMoved; //Used for movement analysis after finishing movement

} motion_axis_t;

// *****************************************************************************

//Functions

// *****************************************************************************

#endif /* AXES_STRUCT_H */

/* *****************************************************************************
 End of File
 */
