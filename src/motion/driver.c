// *****************************************************************************
/*
  @File Name
    driver.h
 
  @Author
    Flitch

  @Summary
    Motion related hardware abstraction layer.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "driver.h"
#include <fdlmath.h>
#include <math.h>

// *****************************************************************************

//Axis groups
const axle_t axlesAllAxes[] = {axisX, axisY, axisZ, axisU, axisV, axisW, axisA, axisB, axisC, axisD, axisF, axisR};
const axle_t axlesAllMainAxes[] = {axisX, axisY, axisZ, axisU, axisV, axisW, axisA, axisB, axisC, axisD};
const axle_t axlesAllHead[] = {axisZ, axisU, axisV, axisW, axisA, axisB, axisC, axisD};
const axle_t axlesXY[] = {axisX, axisY};
const axle_t axlesAllA[] = {axisA, axisB, axisC, axisD};
const axle_t axlesAllZ[] = {axisZ, axisU, axisV, axisW};
const axle_t axlesAllF[] = {axisF, axisR};

//Settings groups
const axle_t axleSettingsIsCircular[] = {axisA, axisB, axisC, axisD};
const axle_t axleSettingsIsEmergencyStopable[] = {axisX, axisY};
const axle_t axleSettingsIgnoreLocks[] = {axisF, axisR};
const axle_t axleSettingsDefaultInvertDirection[] = {axisU, axisW, axisA, axisB, axisC, axisD, axisF};
const axle_t axleSettingsDefaultInvertEnable[] = {axisX, axisY, axisA, axisB, axisC, axisD, axisZ, axisU, axisV, axisW, axisF, axisR};
const axle_t axleSettingsDefaultInvertHome[] = {axisX, axisY, axisZ, axisU, axisV, axisW};
const axle_t axleSettingsAxisEnabled[] = {axisX, axisY, axisZ, axisA, axisU, axisV, axisC, axisW};

nozzle_t activeNozzle = 0;

// *****************************************************************************

void DriverEnDisAxis(axle_t a, bool disEn);
void DriverEnDisAxisXY(bool disEn, bool trueEnVaule);
void DriverEnDisAxisHead1(bool disEn, bool trueEnVaule);
void DriverEnDisAxisHead2(bool disEn, bool trueEnVaule);
void DriverEnDisAxisHead3(bool disEn, bool trueEnVaule);
void DriverEnDisAxisHead4(bool disEn, bool trueEnVaule);
void DriverEnDisAxisPeeler(bool disEn, bool trueEnVaule);

// *****************************************************************************

//Core function

//Global functions

//Local functions

void DriverInitialize(void) {

    //Declare axis letters
    axis[axisX].axisLetter = 'X';
    axis[axisY].axisLetter = 'Y';

    axis[axisZ].axisLetter = 'Z';
    axis[axisU].axisLetter = 'U';
    axis[axisV].axisLetter = 'V';
    axis[axisW].axisLetter = 'W';

    axis[axisA].axisLetter = 'A';
    axis[axisB].axisLetter = 'B';
    axis[axisC].axisLetter = 'C';
    axis[axisD].axisLetter = 'D';

    axis[axisF].axisLetter = 'F';
    axis[axisR].axisLetter = 'R';


    //Declare endstop pins
    axis[axisX].homePin = END_X_MIN_PIN;
    axis[axisY].homePin = END_YL_MIN_PIN;

    axis[axisZ].homePin = END_A1_PIN;
    axis[axisU].homePin = END_A2_PIN;
    axis[axisV].homePin = END_B1_PIN;
    axis[axisW].homePin = END_B2_PIN;

    axis[axisA].homePin = GPIO_PIN_NONE;
    axis[axisB].homePin = GPIO_PIN_NONE;
    axis[axisC].homePin = GPIO_PIN_NONE;
    axis[axisD].homePin = GPIO_PIN_NONE;

    axis[axisF].homePin = GPIO_PIN_NONE;
    axis[axisR].homePin = GPIO_PIN_NONE;

    //Declare step pins
    axis[axisX].stepMask = STEP_X;
    axis[axisY].stepMask = STEP_Y;

    axis[axisZ].stepMask = STEP_A1;
    axis[axisU].stepMask = STEP_B1;
    axis[axisV].stepMask = STEP_C1;
    axis[axisW].stepMask = STEP_D1;

    axis[axisA].stepMask = STEP_A2;
    axis[axisB].stepMask = STEP_B2;
    axis[axisC].stepMask = STEP_C2;
    axis[axisD].stepMask = STEP_D2;

    axis[axisF].stepMask = STEP_E1;
    axis[axisR].stepMask = STEP_E2;


    for (uint32_t a = 0; a < array_length(axleSettingsIsCircular); a++) {
        axis[axleSettingsIsCircular[a]].isCircular = 1;
    }
    for (uint32_t a = 0; a < array_length(axleSettingsIsEmergencyStopable); a++) {
        axis[axleSettingsIsEmergencyStopable[a]].isEmergencyStopable = 1;
    }
    for (uint32_t a = 0; a < array_length(axleSettingsIgnoreLocks); a++) {
        axis[axleSettingsIgnoreLocks[a]].ignoreLocks = 1;
    }
    for (uint32_t a = 0; a < array_length(axleSettingsDefaultInvertDirection); a++) {
        axis[axleSettingsDefaultInvertDirection[a]].invertDirPin = 1;
    }
    for (uint32_t a = 0; a < array_length(axleSettingsDefaultInvertHome); a++) {
        axis[axleSettingsDefaultInvertHome[a]].invertHomePin = 1;
    }
    for (uint32_t a = 0; a < array_length(axleSettingsDefaultInvertEnable); a++) {
        axis[axleSettingsDefaultInvertEnable[a]].invertEnPin = 1;
    }

    //Initialize default settings
    for (uint32_t a = 0; a < array_length(axlesAllAxes); a++) {
        DriverDisableAxis(axlesAllAxes[a]);
        axis[axlesAllAxes[a]].axisState = AXLE_IDLE;
        axis[axlesAllAxes[a]].currentPositionMachineUnits = 0;
        axis[axlesAllAxes[a]].isCircular = 0;
        axis[axlesAllAxes[a]].isHomed = 0;
        axis[axlesAllAxes[a]].isMovementPending = 0;
    }

    DriverEnableAll();

    //Test Part
    for (uint32_t a = 0; a < array_length(axlesAllAxes); a++) {
        axis[axlesAllAxes[a]].currentPositionMachineUnits = 0;
    }

    axis[axisX].speedMinDefault = SETTINGS_SPEED_MIN_X;
    axis[axisX].speedMaxDefault = SETTINGS_SPEED_MAX_X;
    axis[axisX].accelerationDefault = SETTINGS_ACCELERATION_X;
    axis[axisX].mmPerStep = SETTINGS_MM_PER_STEP_X;

    axis[axisY].speedMinDefault = SETTINGS_SPEED_MIN_Y;
    axis[axisY].speedMaxDefault = SETTINGS_SPEED_MAX_Y;
    axis[axisY].accelerationDefault = SETTINGS_ACCELERATION_Y;
    axis[axisY].mmPerStep = SETTINGS_MM_PER_STEP_Y;

    for (uint32_t a = 0; a < array_length(axlesAllZ); a++) {
        axis[axlesAllZ[a]].speedMinDefault = SETTINGS_SPEED_MIN_Z;
        axis[axlesAllZ[a]].speedMaxDefault = SETTINGS_SPEED_MAX_Z;
        axis[axlesAllZ[a]].accelerationDefault = SETTINGS_ACCELERATION_Z;
        axis[axlesAllZ[a]].mmPerStep = SETTINGS_MM_PER_STEP_Z;
    }

    for (uint32_t a = 0; a < array_length(axlesAllA); a++) {
        axis[axlesAllA[a]].speedMinDefault = SETTINGS_SPEED_MIN_A;
        axis[axlesAllA[a]].speedMaxDefault = SETTINGS_SPEED_MAX_A;
        axis[axlesAllA[a]].accelerationDefault = SETTINGS_ACCELERATION_A;
        axis[axlesAllA[a]].mmPerStep = SETTINGS_MM_PER_STEP_A;
    }
    for (uint32_t a = 0; a < array_length(axlesAllF); a++) {
        axis[axlesAllF[a]].speedMinDefault = SETTINGS_SPEED_MIN_F;
        axis[axlesAllF[a]].speedMaxDefault = SETTINGS_SPEED_MAX_F;
        axis[axlesAllF[a]].accelerationDefault = SETTINGS_ACCELERATION_F;
        axis[axlesAllF[a]].mmPerStep = SETTINGS_MM_PER_STEP_F;
    }
}

bool DriverIsActive(void) {
    bool activity = 0;
    for (axle_t a = 0; a < array_length(axlesAllAxes); a++) {
        activity = activity || (axis[axlesAllAxes[a]].axisState != AXLE_IDLE);
    }
    return (activity);
}

void DriverAxisDirection(axle_t a, bool dir) {
    //Determine direction of movement
    if (dir) {
        axis[a].directionSigned = 1;
    } else {
        axis[a].directionSigned = -1;
    }
    bool trueDirVal = ((dir) ^ (axis[a].invertDirPin));
    switch (a) {
        case axisX:
            GPIO_PinWrite(DRV_X_DIR_PIN, trueDirVal);
            break;
        case axisY:
            GPIO_PinWrite(DRV_Y_DIR_PIN, trueDirVal);
            break;
        case axisZ:
            GPIO_PinWrite(DRV_A1_DIR_PIN, trueDirVal);
            break;
        case axisA:
            GPIO_PinWrite(DRV_A2_DIR_PIN, trueDirVal);
            break;
        case axisU:
            GPIO_PinWrite(DRV_B1_DIR_PIN, trueDirVal);
            break;
        case axisB:
            GPIO_PinWrite(DRV_B2_DIR_PIN, trueDirVal);
            break;
        case axisV:
            GPIO_PinWrite(DRV_C1_DIR_PIN, trueDirVal);
            break;
        case axisC:
            GPIO_PinWrite(DRV_C2_DIR_PIN, trueDirVal);
            break;
        case axisW:
            GPIO_PinWrite(DRV_D1_DIR_PIN, trueDirVal);
            break;
        case axisD:
            GPIO_PinWrite(DRV_D2_DIR_PIN, trueDirVal);
            break;
        case axisF:
            GPIO_PinWrite(DRV_E1_DIR_PIN, trueDirVal);
            break;
        case axisR:
            GPIO_PinWrite(DRV_E2_DIR_PIN, trueDirVal);
            break;
        default:
            break;
    }
    CORETIMER_DelayUs(25);
}

void DriverEnableAll(void) {
    for (uint32_t a = 0; a < array_length(axleSettingsAxisEnabled); a++) {
        DriverEnableAxis(axleSettingsAxisEnabled[a]);
        axis[axleSettingsAxisEnabled[a]].axisState = AXLE_IDLE;
        axis[axleSettingsAxisEnabled[a]].isHomed = 0;
    }
    CORETIMER_DelayMs(2);
}

void DriverDisableAll(void) {
    for (uint32_t a = 0; a < array_length(axlesAllAxes); a++) {
        DriverDisableAxis(axlesAllAxes[a]);
        axis[axlesAllAxes[a]].axisState = AXLE_IDLE;
        axis[axlesAllAxes[a]].isHomed = 0;
    }
    CORETIMER_DelayMs(2);
}

void DriverDisableXY(void) {
    DriverDisableAxis(axisX); //X&Y have same enable pin
    for (uint32_t a = 0; a < array_length(axlesXY); a++) {
        axis[axlesAllAxes[a]].axisState = AXLE_IDLE;
        axis[axlesAllAxes[a]].isHomed = 0;
    }
    CORETIMER_DelayMs(2);
}

void DriverEnableAxis(uint32_t a) {

    DriverEnDisAxis(a, true);
}

void DriverDisableAxis(uint32_t a) {

    DriverEnDisAxis(a, false);
}

void DriverEnDisAxis(axle_t a, bool disEn) {
    bool trueEnValue = disEn ^ axis[a].invertEnPin;
    switch (a) {
        case axisX:
            DriverEnDisAxisXY(disEn, trueEnValue);
            break;
        case axisY:
            DriverEnDisAxisXY(disEn, trueEnValue);
            break;
        case axisZ:
            DriverEnDisAxisHead1(disEn, trueEnValue);
            break;
        case axisA:
            DriverEnDisAxisHead1(disEn, trueEnValue);
            break;
        case axisU:
            DriverEnDisAxisHead2(disEn, trueEnValue);
            break;
        case axisB:
            DriverEnDisAxisHead2(disEn, trueEnValue);
            break;
        case axisV:
            DriverEnDisAxisHead3(disEn, trueEnValue);
            break;
        case axisC:
            DriverEnDisAxisHead3(disEn, trueEnValue);
            break;
        case axisW:
            DriverEnDisAxisHead4(disEn, trueEnValue);
            break;
        case axisD:
            DriverEnDisAxisHead4(disEn, trueEnValue);
            break;
        case axisF:
            DriverEnDisAxisPeeler(disEn, trueEnValue);
            break;
        case axisR:
            DriverEnDisAxisPeeler(disEn, trueEnValue);

            break;
        default:
            break;
    }
}

void DriverEnDisAxisXY(bool disEn, bool trueEnVaule) {

    GPIO_PinWrite(DRV_XYZ_EN_PIN, trueEnVaule);
    axis[axisX].isEnabled = disEn;
    axis[axisY].isEnabled = disEn;

}

void DriverEnDisAxisHead1(bool disEn, bool trueEnVaule) {

    GPIO_PinWrite(DRV_A_EN_PIN, trueEnVaule);
    axis[axisZ].isEnabled = disEn;
    axis[axisA].isEnabled = disEn;
}

void DriverEnDisAxisHead2(bool disEn, bool trueEnVaule) {

    GPIO_PinWrite(DRV_B_EN_PIN, trueEnVaule);
    axis[axisU].isEnabled = disEn;
    axis[axisB].isEnabled = disEn;
}

void DriverEnDisAxisHead3(bool disEn, bool trueEnVaule) {

    GPIO_PinWrite(DRV_C_EN_PIN, trueEnVaule);
    axis[axisV].isEnabled = disEn;
    axis[axisC].isEnabled = disEn;
}

void DriverEnDisAxisHead4(bool disEn, bool trueEnVaule) {

    GPIO_PinWrite(DRV_D_EN_PIN, trueEnVaule);
    axis[axisW].isEnabled = disEn;
    axis[axisD].isEnabled = disEn;
}

void DriverEnDisAxisPeeler(bool disEn, bool trueEnVaule) {

    GPIO_PinWrite(DRV_E_EN_PIN, trueEnVaule);
    axis[axisF].isEnabled = disEn;
    axis[axisR].isEnabled = disEn;
}

void DriverSetActiveNozzle(uint32_t toolId) {
    if (toolId >= 0 && toolId < 5) {
        activeNozzle = toolId;
    } else {

        activeNozzle = 1;
    }
}

nozzle_t DriverGetActiveNozzle(void) {

    return (activeNozzle);
}

double DriverGetHomeCoordinates(axle_t a) {
    switch (a) {
        case axisX:return (SETTINGS_X_HOME_COORDINATES);
            break;
        case axisY:return (SETTINGS_Y_HOME_COORDINATES);
            break;
        case axisZ:return (SETTINGS_Z_HOME_COORDINATES);
            break;
        case axisA:return (SETTINGS_A_HOME_COORDINATES);
            break;
        case axisU:return (SETTINGS_Z_HOME_COORDINATES);
            break;
        case axisB:return (SETTINGS_A_HOME_COORDINATES);
            break;
        case axisV:return (SETTINGS_Z_HOME_COORDINATES);
            break;
        case axisC:return (SETTINGS_A_HOME_COORDINATES);
            break;
        case axisW:return (SETTINGS_Z_HOME_COORDINATES);
            break;
        case axisD:return (SETTINGS_A_HOME_COORDINATES);
            break;
        case axisF:return (SETTINGS_F_HOME_COORDINATES);
            break;
        case axisR:return (SETTINGS_F_HOME_COORDINATES);

            break;
        default: return (0.0L);
            break;
    }
}

double DriverGetHomingSpeed(axle_t a) {
    switch (a) {
        case axisX:return (SETTINGS_X_HOME_SPEED);
            break;
        case axisY:return (SETTINGS_Y_HOME_SPEED);
            break;
        default: return (SETTINGS_Z_HOME_SPEED);

            break;
    }
}

axle_t DriverGetAxleFromLetter(char letter) {
    axle_t result = 0;
    for (uint32_t a = 0; a < NUMBER_OF_AXES; a++) //for all axes
    {
        if (letter == axis[a].axisLetter) {
            result = a;
            break;
        }
    }
    return (result);
}

void DriverSetCurrentCoordinatesReal(axle_t a, double val) {

    axis[a].currentPositionMachineUnits = DriverGetMachineUnitsFromPositionFine(a, val);
}

double DriverGetCurrentCoordinatesReal(axle_t a) {

    return (axis[a].currentPositionMachineUnits * axis[a].mmPerStep);
}

int32_t DriverGetMachineUnitsFromPositionFine(axle_t a, double position) {

    return ((int32_t) ((position + (axis[a].mmPerStep / 2.0d)) / axis[a].mmPerStep));
}

int32_t DriverGetMachineUnitsFromPositionRough(axle_t a, double position) {

    return ((int32_t) floor(position / axis[a].mmPerStep));
}

double DriverGetPositionFromMachineUnits(axle_t a, int32_t mUnits) {
    return ((double) mUnits * axis[a].mmPerStep);
}
/* *****************************************************************************
 End of File
 */
