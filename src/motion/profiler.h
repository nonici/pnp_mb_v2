/* ************************************************************************** */
/*
  @File Name
    profiler.h
 
  @Author
    Flitch

  @Summary
    Motion related profile calculation and planing.

  @Description
    Calculation of motion parameters (speeds, distances, timings).
 */
/* ************************************************************************** */

#ifndef PROFILER_H    /* Guard against multiple inclusion */
#define PROFILER_H

// *****************************************************************************

#include "enums.h"
#include "axes_struct.h"

// *****************************************************************************

//#defines

// *****************************************************************************

extern motion_axis_t axis[NUMBER_OF_AXES];

// *****************************************************************************

void ProfilerPeelFrontForDegrees(double angle);
void ProfilerPeelRearForDegrees(double angle);
//Start peeler axis 

void ProfilerLoadDestination(char a, double val);
void ProfilerRun(void);

void ProfilerSetSpeedPercent(double speed);
//setting max movement speed
//Promjenit u mm/min i prera?unat u machine units

void ProfilerSetAxisSpeed(axle_t letter, double speed);

void ProfilerCalculatePath(uint32_t a, double val);
//arguments: axis, type of movement, value of movement, message counter ID
//checks if axis is bussy
//prepares path values (direction, speed, distance)
//can report current position or movement map
//runs: GenerateMovementMap()

void ProfilerGenerateMovementMap(axle_t a, double targetPos, motion_type_t);
//creates a movement map (pass x delays table)
//table(acceleration, maxspeed, deacceleration)
//enables axis

void ProfilerAnalizeResults(axle_t a);
//Checks number of steps moved

#endif /* PROFILER_H */

/* *****************************************************************************
 End of File
 */
