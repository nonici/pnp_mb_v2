// *****************************************************************************
/*
  @File Name
    serial.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "serial.h"
#include "../configuration.h"

// *****************************************************************************

//Common characters
#define NL_CHAR    0x0A            //New line
#define CR_CHAR    0x0D            //Carriage Return
#define WS_CHAR    0x20            //Whitespace character
#define STR_CHAR   0x24            //$
#define PRC_CHAR   0x25            //%
#define SC_CHAR    0x3B            //;

//Allocate memory for buffer
static uint8_t serialFifoRxMem [SERIAL_FIFO_COMM_SIZE] = {0};
static uint8_t serialFifoTxMem [SERIAL_FIFO_COMM_SIZE] = {0};
static uint8_t serialBuilderMem [SERIAL_FIFO_COMM_SIZE] = {0};

ringu8_t serialFifoRx;
ringu8_t serialFifoTx;
ringu8_t serialBuilder;

bool serialCommEnabled = 0;

// *****************************************************************************

void SerialEcho(void);

void SerialEnable(void);

void SerialDisable(void);

// *****************************************************************************

void SerialInitialize(void) {
    //Create type specific fifo

    RingU8Initialize(&serialFifoRx, serialFifoRxMem, SERIAL_FIFO_COMM_SIZE);
    RingU8Initialize(&serialFifoTx, serialFifoTxMem, SERIAL_FIFO_COMM_SIZE);
    RingU8Initialize(&serialBuilder, serialBuilderMem, SERIAL_FIFO_COMM_SIZE);

    SerialEnable();
}

void SerialHandler(void) {

    if (serialCommEnabled == false) {
        return;
    }

    //transmit new messages
    SerialTransmitFromTxBufferToUart();

    //parse received
    SerialForwardFromRxBufferToParser();

    //transmit messages from builder
    SerialTransmitFromBuilderToTxBuffer();

    //SerialEcho();
}

//Ensure receive is called every 700 us or less, done in sys interrupt (500 us)

void SerialReceiveToBuffer(void) {
    while (UART3_ReceiverIsReady()) {
        UART3_ErrorGet();
        RingU8Write(&serialFifoRx, (uint8_t) (UART3_ReadByte()));
    }
}

void SerialTransmitFromTxBufferToUart(void) {
    while ((UART6_TransmitterIsReady()) && RingU8DataInside(&serialFifoTx)) {
        UART6_WriteByte(RingU8Read(&serialFifoTx));
    }
}

void SerialTransmitFromBuilderToTxBuffer(void) {
    while (RingU8DataInside(&serialBuilder)) {
        RingU8Write(&serialFifoTx, RingU8Read(&serialBuilder));
    }
}

void SerialForwardFromRxBufferToParser(void) {

    if (!ParserAllowNewTransfers()) {
        return;
    }

    //Check if /NL is received 
    if (!RingU8SniffForMatch(&serialFifoRx, '\n')) {
        return;
    }

    uint8_t rcvChar = 0;
    while (rcvChar != '\n') {
        rcvChar = RingU8Read(&serialFifoRx);
        //RingU8Write(&serialFifoTx, rcvChar); //echo
        ParserTransferNewChar(rcvChar);
    }
}

uint32_t SerialSpaceInTxFifo(void) {
    return (RingU8FreeSpace(&serialFifoTx));
}

uint32_t SerialDataInTxFifo(void) {
    return (RingU8DataInside(&serialFifoTx));
}

void SerialMsgAppendCharToTxFifo(uint8_t ch) {
    RingU8Write(&serialFifoTx, ch);
}

void SerialMsgAppendCharToBuilder(uint8_t ch) {
    RingU8Write(&serialBuilder, ch);
}

void SerialMsgAppendStringToTxFifo(char * strArray) {
    RingU8AppendString(&serialFifoTx, strArray);
}

void SerialMsgAppendStringToTxBuilder(char * strArray) {
    RingU8AppendString(&serialBuilder, strArray);
}

void SerialEcho(void) {
    while (RingU8DataInside(&serialFifoRx)) {
        RingU8Write(&serialFifoTx, RingU8Read(&serialFifoRx));
    }
}

void SerialEnable(void) {
    serialCommEnabled = 1;
    SerialMsgAppendStringToTxBuilder("\r\nPNP_V2\r\n");
}

void SerialDisable(void) {
    serialCommEnabled = 0;
}

/* *****************************************************************************
 End of File
 */
