/* ************************************************************************** */
/*
  @File Name
    parser.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef PARSER_H    /* Guard against multiple inclusion */
#define PARSER_H

// *****************************************************************************

#include "system/system.h"
#include "stdlib.h"

#include "gcode.h"
#include "shell.h"
#include "stringbuilder.h"
#include "ring.h"
#include "comm_structs.h" 
#include "../motion/driver.h"
#include <assert.h>

// *****************************************************************************

// *****************************************************************************
void ParserInitialize(void);
void ParserHandler(void);

void ParserTransferNewChar(uint8_t c);
bool ParserAllowNewTransfers(void);

void ParserCommandM400Activate(void);
void ParserCommandM400InternalActivate(void);

bool ParserCommandDwellIsActive(void);
void ParserCommandDwellActivate(uint32_t dwellTime);

// *****************************************************************************

#endif /* PARSER_H */

/* *****************************************************************************
 End of File
 */
