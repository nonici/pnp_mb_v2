// *****************************************************************************
/*
  @File Name
    gcode.c
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "gcode.h"
#include "stringbuilder.h"
#include "parser.h" 
#include "serial.h" 

#include "../log/log.h"

#include "../pnp/signalers.h"
#include "../pnp/cam_led.h"

#include "../motion/driver.h"
#include "../motion/profiler.h"
#include "../motion/movement.h"

#include "../feeders/slots.h"
#include "../pneumatics/pump.h"
#include "../pneumatics/sensors.h"
#include "../pneumatics/solenoids.h"
#include <assert.h>
// *****************************************************************************

// *****************************************************************************

//"G" codes:

void GcodeCommandG0(command_block_t *cmdBlock) {
    //Check for individual axes: X, Y, Z, U, V, W, A, B, C, D, and speed: F
    while (1) {
        if (strchr(MAIN_AXIS_LETTERS, cmdBlock->cmdChar)) {
            ProfilerLoadDestination(cmdBlock->cmdChar, cmdBlock->val);
        }
        if (cmdBlock->cmdChar == 'F') {
            ProfilerSetSpeedPercent(cmdBlock->val);
        }
        if (cmdBlock->cmdChar == '\n' || cmdBlock->cmdChar == '\0') {
            break;
        }
        cmdBlock = cmdBlock + 1;
    }
    ProfilerRun();
    //GcodeRegexCommandConfirm();
    ParserCommandM400InternalActivate();
}

void GcodeCommandG1(command_block_t *cmdBlock) {
    //Driver doesn't support syncronous movements, G1 is redirected to G0
    GcodeCommandG0(cmdBlock);
}

void GcodeCommandG4(command_block_t *cmdBlock) {
    //Check for subcode P (miliseconds) or S (seconds)

    uint32_t dwellInterval = (uint32_t) cmdBlock->val;

    //Change seconds to milliseconds
    if (cmdBlock->cmdChar == 'S') {
        dwellInterval = dwellInterval * 1000;
    }
    //maximum capable delay is 30 seconds (30000 milliseconds)
    if (dwellInterval > 30000) {
        dwellInterval = 30000;
    }
    //run timer here
    ParserCommandDwellActivate(dwellInterval);
}

void GcodeCommandG10(void) {
    RetractZ();
    GcodeCommandM400();
}

void GcodeCommandG21(void) {
    //Connect machine
    GcodeRegexCommandConfirm();
}

void GcodeCommandG22(void) {
    //Disconnect and power off machine
    MachineDisable();
    GcodeRegexCommandConfirm();
}

void GcodeCommandG28(void) {
    //Run homing procedure
    MotionEnable();
}

void GcodeCommandG92(command_block_t *cmdBlock) {
    //Set current coordinates to be value received 
    while (1) {
        if (strchr(MAIN_AXIS_LETTERS, cmdBlock->cmdChar)) {
            DriverSetCurrentCoordinatesReal(DriverGetAxleFromLetter(cmdBlock->cmdChar), cmdBlock->val);
        }
        if (cmdBlock->cmdChar == '\n' || cmdBlock->cmdChar == '\0') {
            break;
        }
        cmdBlock = cmdBlock + 1;
    }
    GcodeRegexCommandConfirm();
}

//"M" codes:

void GcodeCommandM106(void) {
    //Pick command

    PickHeadPickComponent(DriverGetActiveNozzle());
    GcodeRegexCommandConfirm();
}

void GcodeCommandM107(void) {
    //Place command

    PickHeadReleaseComponent(DriverGetActiveNozzle());
    GcodeRegexCommandConfirm();
}

void GcodeCommandM108(void) {
    //Read pressure on active nozzle
    //Command confirming is happening in read with double regex
    double sensVal = SensorsGetValueRealHead(DriverGetActiveNozzle());
    GcodeRegexActuatorReadWithDouble(sensVal);
}

void GcodeCommandM109(void) {
    //Read pressure on active nozzle
    //Command confirming is happening in read with double regex
    double sensVal = SensorsGetValueReal(SensorV);
    GcodeRegexActuatorReadWithDouble(sensVal);
}

void GcodeCommandM110(void) {
    //Read pressure on active nozzle
    //Command confirming is happening in read with double regex
    double sensVal = SensorsGetValueReal(SensorA);
    GcodeRegexActuatorReadWithDouble(sensVal);
}

void GcodeCommandM114(void) {
    //Get position from controller
    GcodeRegexCommandConfirm();
    GcodeRegexPositionReport();
}

void GcodeCommandM115(void) {
    //Get position from controller
    GcodeRegexCommandConfirm();
    GcodeRegexFirmwareVersionReport();
}

void GcodeCommandM400(void) {
    ParserCommandM400Activate();
}

void GcodeCommandM600(command_block_t * cmdBlock) {
    //CAN BUS dispatcher
    DecomposeOpenPNPFeederCommand(cmdBlock->cmdChar, cmdBlock->val);
}

void GcodeCommandM800(void) {
    //Head led control
    CameraLedOff(HeadCam);
    GcodeRegexCommandConfirm();
}

void GcodeCommandM801(void) {
    //Head led control
    CameraLedOn(HeadCam);
    GcodeRegexCommandConfirm();
}

void GcodeCommandM810(void) {
    //Base led control
    CameraLedOff(BaseCam);
    GcodeRegexCommandConfirm();
}

void GcodeCommandM811(void) {
    //Base led control
    CameraLedOn(BaseCam);
    GcodeRegexCommandConfirm();
}

void GcodeCommandM820(void) {
    //Red led control
    SignalLedOff(ledRed);
    GcodeRegexCommandConfirm();
}

void GcodeCommandM821(void) {
    //Red led control
    SignalLedOn(ledRed);
    GcodeRegexCommandConfirm();
}

void GcodeCommandM830(void) {
    //Blue led control
    SignalLedOff(ledBlue);
    GcodeRegexCommandConfirm();
}

void GcodeCommandM831(void) {
    //Blue led control
    SignalLedOn(ledBlue);
    GcodeRegexCommandConfirm();
}

void GcodeCommandM840(void) {
    //Green led control
    SignalLedOff(ledGreen);
    GcodeRegexCommandConfirm();
}

void GcodeCommandM841(void) {
    //Green led control
    SignalLedOn(ledGreen);
    GcodeRegexCommandConfirm();
}

void GcodeCommandM850(void) {
    //Buzzer control
    SignalBuzzShort();
    GcodeRegexCommandConfirm();
}

void GcodeCommandM851(void) {
    //Buzzer control
    SignalBuzzLong();
    GcodeRegexCommandConfirm();
}

void GcodeCommandM860(void) {
    //Pumps control
    PumpsDeactivate();
    GcodeRegexCommandConfirm();
}

void GcodeCommandM861(void) {
    //Pumps control
    PumpsActivate();
    GcodeRegexCommandConfirm();
}

//"T" codes 

void GcodeCommandT(uint32_t num) {
    DriverSetActiveNozzle(num);
    GcodeRegexCommandConfirm();
}

//G-code responses

void GcodeRegexCommandConfirm(void) {
    SerialMsgAppendStringToTxFifo("ok\r\n");
}

void GcodeRegexPositionReport(void) {
    return;
    char textPositionRegexMemory[150];
    stringbuilder_t textPositionRegexBuilder;
    StringbuilderIntialize(&textPositionRegexBuilder, textPositionRegexMemory, 150);
    StringbuilderAppendString(&textPositionRegexBuilder, "MPos:");

    for (uint32_t a = 0; a < NUMBER_OF_MAIN_AXES; a++) {
        StringbuilderAppendChar(&textPositionRegexBuilder, ' ');
        StringbuilderAppendChar(&textPositionRegexBuilder, axis[a].axisLetter);
        StringbuilderAppendChar(&textPositionRegexBuilder, ':');
        StringbuilderAppendDouble(&textPositionRegexBuilder, DriverGetCurrentCoordinatesReal(a));
    }

    StringbuilderAppendCrLf(&textPositionRegexBuilder);
    SerialMsgAppendStringToTxFifo(textPositionRegexMemory);
    __conditional_software_breakpoint(StringbuilderStringLength(&textPositionRegexBuilder) < 150);
}

void GcodeRegexFirmwareVersionReport(void) {
    SerialMsgAppendStringToTxFifo("FIRMWARE: PNP_MB_V2 2.0.0 FIRMWARE_URL: N/A PROTOCOL_VERSION: 2.0 MACHINE_TYPE:Open PNP EXTRUDER_COUNT:4");
}

void GcodeRegexErrorInvalidCommand(void) {
    SerialMsgAppendStringToTxFifo("Error: Invalid Command\r\n");
    LogNewEntry(LogInvalidCommand, LogCallerObjectParser, logError, 0, 0.0);
}

void GcodeRegexErrorFatalError(void) {
    LogNewEntry(LogSystemFatalError, LogCallerObjectParser, logError, 0, 0.0);
    SerialMsgAppendStringToTxBuilder("Error: Fatal Error\r\n");
}

void GcodeRegexErrorEstop(void) {
    SerialMsgAppendStringToTxBuilder("Error: Emergency Stop\r\n");
}

void GcodeRegexErrorFeedFailed(void) {
    SerialMsgAppendStringToTxBuilder("Error: Feed Failed\r\n");
}

void GcodeRegexHomeComplete(void) {
    CORETIMER_DelayMs(2);
    //GcodeRegexCommandConfirm();
    SerialMsgAppendStringToTxFifo("\r\nok\r\n");
}

void GcodeRegexMoveToComplete(void) {
}

void GcodeRegexActuatorReadWithDouble(double val) {
    return;
    char textReadDoubleRegexMemory[50];
    stringbuilder_t textReadDoubleRegexBuilder;
    StringbuilderIntialize(&textReadDoubleRegexBuilder, textReadDoubleRegexMemory, 50);
    StringbuilderAppendString(&textReadDoubleRegexBuilder, "Read:");
    StringbuilderAppendDouble(&textReadDoubleRegexBuilder, val);
    StringbuilderAppendCrLf(&textReadDoubleRegexBuilder);
    SerialMsgAppendStringToTxFifo(textReadDoubleRegexMemory);
    GcodeRegexCommandConfirm();
}

/* *****************************************************************************
 End of File
 */
