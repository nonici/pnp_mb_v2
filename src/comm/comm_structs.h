/* ************************************************************************** */
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef COM_STRUCTS_H    /* Guard against multiple inclusion */
#define COM_STRUCTS_H

// *****************************************************************************

#include <stdint.h>

// *****************************************************************************

typedef struct {
    char cmdChar;
    double val;
} command_block_t;

// *****************************************************************************

//Variables

// *****************************************************************************

//Functions

// *****************************************************************************

#endif /* COM_STRUCTS_H */

/* *****************************************************************************
 End of File
 */
