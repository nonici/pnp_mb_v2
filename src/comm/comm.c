// *****************************************************************************
/*
  @File Name
    comm.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "comm.h"

// *****************************************************************************

//defines
//structs
//varibles
//constants

// *****************************************************************************

//Local function definitions

// *****************************************************************************

void CommInitialize(void) {
    SerialInitialize();
    ParserInitialize();
    FeedersInitialize();
    CanbusInitialize();
}

void CommHandler(void) {

    SerialHandler();
    ParserHandler();
    FeedersHandler();
    FeederSlotHandler();
    CanbusHandler();
}


//Global functions

//Local functions


/* *****************************************************************************
 End of File
 */
