// *****************************************************************************
/*
  @File Name
    parser.c
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "parser.h"

// *****************************************************************************
//new stuff
#define PARSER_RING_SIZE                    256

#define DECOMPOSER_STRING_SIZE              24
#define DECOMPOSER_MESSAGE_ARRAY_SIZE       16

static uint8_t parserRingMem [PARSER_RING_SIZE];
ringu8_t parserRing;

char stringsArray[DECOMPOSER_STRING_SIZE];
stringbuilder_t decomposerStringbuilder;

command_block_t decomposedCommand [DECOMPOSER_MESSAGE_ARRAY_SIZE];

//Global parser flags
bool parserAllowNewTransfers = 1; //When 0, doesnt receive new messages

bool readyForParsing = 0; //Flag that marks full message received, and if parser should start

bool isM400Active = 0; //Flag for M400 command
bool isM400InternalActive = 0; //Flag for M400 internal command

bool isG4Active = 0; //Flag for G4 command
uint32_t dwellStartCount = 0;
uint32_t dwellEndCount = 0;

// *****************************************************************************
//new stuff, Local fn
void ParserMessageDecomposer(void);

void ParserDecomposerCleanup(void);

// *****************************************************************************
//old stuff, Local fn

// *****************************************************************************

void ParserInitialize(void) {
    ParserDecomposerCleanup();

    RingU8Initialize(&parserRing, parserRingMem, PARSER_RING_SIZE);
    StringbuilderIntialize(&decomposerStringbuilder, stringsArray, DECOMPOSER_STRING_SIZE);
}

void ParserHandler(void) {

    //G4 blocks if dwell time has not expired    
    if (isG4Active) {
        //check if timer has elapsed
        if ((CORETIMER_CounterGet() - dwellStartCount) < dwellEndCount) {
            return;
        } else {
            isG4Active = 0;
            dwellStartCount = 0;
            dwellEndCount = 0;
            GcodeRegexCommandConfirm();
        }
    }

    //M400 internal blocks until all movement is completed
    if (isM400InternalActive) {
        if (DriverIsActive()) {
            return;
        } else {
            isM400InternalActive = 0;
            GcodeRegexCommandConfirm();
        }
    }

    //M400 blocks until all movement is completed
    if (isM400Active) {
        if (DriverIsActive()) {
            return;
        } else {
            isM400Active = 0;
            GcodeRegexCommandConfirm();
        }
    }
    
    //Blocks if whole message has not been yet received
    if (!readyForParsing) {
        return;
    }

    //Handle new incoming messages
    ParserMessageDecomposer();
}

void ParserMessageDecomposer(void) {

    //Find end character (\n))
    //Starting from beginning up to end char,
    //scan each char if it's one of command characters or numerical char
    //Command characters (N, G, M, T, $, X, Y, Z, U, V, W, A, B, C, D, F, P, S,)
    //Numerical characters (., -, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
    //If command char, write it to .char in array,
    //If numerical, append it to .string in array,
    //If char again, increment array index, and repeat
    //Convert arguments to numbers (double)
    //Write chars and arguments in structs

    /*
     * input: "G0X59.103553Y77.059688F100.0"
     * output struct array {Char, valD}[MAX]
     * Output struct:
     * {G, 0}[0]
     * {X, 59.103553}[1]
     * {Y, 77.059688}[2]
     * {F, 100.0}[3]
     * {0, 0, 0}[4]
     * {0, 0, 0}[5]
     * {0, 0, 0}[6]
     * {0, 0, 0}[7]
     * {0, 0, 0}[8]
     * {0, 0, 0}[9]
     * {0, 0, 0}[10]
     * {0, 0, 0}[11]
     * {0, 0, 0}[12]
     * {0, 0, 0}[13]
     * {0, 0, 0}[14]
     * {0, 0, 0}[15]
     * 
     */

    //Last read char
    uint8_t ch = 0;

    //Array pointer, points from 0 to end, starts at 0
    uint32_t arrIdx = 0;

    //Guard against multiple command letters per command block
    //bool cmdLetterFound = 0;

    //Initialize SB
    StringbuilderResetBuilder(&decomposerStringbuilder);

    //This loop is running until all characters are read, or whole array has been filed

    //Reads every character from start to end, and sorts it to command letter or it's numerical
    while (true) {

        //Read single character
        ch = RingU8Read(&parserRing);

        //Writes command letter character and it's index
        if ((ch >= 'A' && ch <= 'Z') || (ch == '$') || (ch == '\n')) {
            char *ptr; //ptr required by strtod function
            decomposedCommand[arrIdx].val = strtod(stringsArray, &ptr);
            StringbuilderResetBuilder(&decomposerStringbuilder);
            arrIdx++;
            __conditional_software_breakpoint(arrIdx < DECOMPOSER_MESSAGE_ARRAY_SIZE);
            if (arrIdx == DECOMPOSER_MESSAGE_ARRAY_SIZE) {
                //Parsing finished, reached end of array
                break;
            }
            decomposedCommand[arrIdx].cmdChar = ch;
            StringbuilderResetBuilder(&decomposerStringbuilder);
        }

        //If it's numerical, write it to string array, to be converted later
        if ((ch >= '0' && ch <= '9') || ch == '.' || ch == '-') {
            StringbuilderAppendChar(&decomposerStringbuilder, ch);
        }
        if (ch == '\n' || ch == 0) {
            //Parsing finished, reached end of message
            break;
        }
    }

    //Message is now decomposed, time to parse it

    //Find main command letter (G, M, T, $)

    bool invalidCommand = 0;
    uint32_t commandCode = 0;

    ch = 0;
    uint32_t i = 0;
    //Check from beginning of array up to last written index 
    while (i < arrIdx) {
        ch = decomposedCommand[i].cmdChar;
        if (ch == 'G' || ch == 'M' || ch == 'T' || ch == '$') {
            commandCode = (uint32_t) (decomposedCommand[i].val);
            break;
        }
        if (i == arrIdx) {
            break;
        }
        i++;
    }

    if (ch == 'G') {
        switch (commandCode) {
            case 0: GcodeCommandG0(&decomposedCommand[++i]); //Temp
                break;
            case 1: GcodeCommandG1(&decomposedCommand[++i]);
                break;
            case 4: GcodeCommandG4(&decomposedCommand[++i]);
                break;
            case 10: GcodeCommandG10();
                break;
            case 21: GcodeCommandG21();
                break;
            case 22: GcodeCommandG22();
                break;
            case 28: GcodeCommandG28();
                break;
            case 92: GcodeCommandG92(&decomposedCommand[++i]);
                break;
            default: invalidCommand = 1;
                break;
        }
    }
    if (ch == 'M') {
        switch (commandCode) {
            case 106: GcodeCommandM106();
                break;
            case 107: GcodeCommandM107();
                break;
            case 108: GcodeCommandM108();
                break;
            case 109: GcodeCommandM109();
                break;
            case 110: GcodeCommandM110();
                break;
            case 114: GcodeCommandM114();
                break;
            case 115: GcodeCommandM115();
                break;
            case 400: GcodeCommandM400();
                break;
            case 600: GcodeCommandM600(&decomposedCommand[++i]);
                break;
            case 800: GcodeCommandM800();
                break;
            case 801: GcodeCommandM801();
                break;
            case 810: GcodeCommandM810();
                break;
            case 811: GcodeCommandM811();
                break;
            case 820: GcodeCommandM820();
                break;
            case 821: GcodeCommandM821();
                break;
            case 830: GcodeCommandM830();
                break;
            case 831: GcodeCommandM831();
                break;
            case 840: GcodeCommandM840();
                break;
            case 841: GcodeCommandM841();
                break;
            case 850: GcodeCommandM850();
                break;
            case 851: GcodeCommandM851();
                break;
            case 860: GcodeCommandM860();
                break;
            case 861: GcodeCommandM861();
                break;
            default: invalidCommand = 1;
                break;
        }
    }
    if (ch == 'T') {
        GcodeCommandT(commandCode);
        invalidCommand = 0;
    }
    if (ch == '$') {
        //Move this code to shell>>
        invalidCommand = 0;
        switch (decomposedCommand[++i].cmdChar) {
            case 'L':
                switch (decomposedCommand[++i].cmdChar) {
                    case'E':ShellCommandLog(logError);
                        break;
                    case'W':ShellCommandLog(logWarning);
                        break;
                    case'I':ShellCommandLog(logInfo);
                        break;
                    case'D':ShellCommandLog(logDebug);
                        break;
                    default:
                        ShellCommandLog(logInfo);
                        break;
                }
                break;
            case 'R':
                switch (decomposedCommand[++i].cmdChar) {
                    case'L':ShellCommandReportLog();
                        break;
                    case 'R':
                        GcodeRegexPositionReport();
                        break;
                    default:
                        invalidCommand = 1;
                        break;
                }
                break;
            default:
                invalidCommand = 1;
                break;
        }
        //Move this code to shell<<
    }


    Nop(); //Debug Breakpoint

    if (invalidCommand) {
        GcodeRegexErrorInvalidCommand();
        __conditional_software_breakpoint(false);
    }
    //Cleanup at end
    ParserDecomposerCleanup();
    parserAllowNewTransfers = 1;
    readyForParsing = 0;
}

inline void ParserDecomposerCleanup(void) {
    for (uint32_t i = 0; i < DECOMPOSER_MESSAGE_ARRAY_SIZE; i++) {
        decomposedCommand[i].cmdChar = 0;
        decomposedCommand[i].val = 0;
    }
}

void ParserTransferNewChar(uint8_t c) {
    //Called from serial handler when it has data to output ready and parser is ready to receive


    //Filter all useless characters and comment section out of message
    //Also strips whitespace
    //Later to be considered adding command numeration per Gcode, example "N2501"
    //"N2501 G0 X59.103553 Y77.059688 F100.0; Go to fiducial location\r\n"
    //
    //                                         | --- comment section --- |
    //Example: "G0 X59.103553 Y77.059688 F100.0; Go to fiducial location\r\n"
    //Result:  "G0X59.103553Y77.059688F100.0"

    static bool parserFilterIsCommentSection;
    bool isValidChar = 0;

    // If char ';' all next chars until '\n' are part of comment section
    if (c == ';') {
        parserFilterIsCommentSection = 1;
    }
    //'\n' marks end of comment section
    if (c == '\n') {
        parserFilterIsCommentSection = 0;
    }

    //Skip if comment section
    if (!parserFilterIsCommentSection) {
        //List of valid chars not part of comment section
        if ((c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || (c == '.') || c == '$' || (c == '%') || c == '\n' || (c == '-') || (c == '+') || c == '\r')
            isValidChar = 1; //Valid char
    }

    //If char is valid and not comment, write it
    if (isValidChar) {
        RingU8Write(&parserRing, c); //New
    } //Else skip this char

    //Marks last character in message,it's now ready for parsing
    if (c == '\n') {
        readyForParsing = 1;
        //Block new data from serial buffer
        parserAllowNewTransfers = 0;
    }
}

bool ParserAllowNewTransfers(void) {
    return (parserAllowNewTransfers);
}

// *****************************************************************************
//Maybe reusable

void ParserCommandM400Activate() {
    isM400Active = 1;
}

void ParserCommandM400InternalActivate() {
    isM400InternalActive = 1;
}

void ParserCommandDwellActivate(uint32_t dwellTime) {
    dwellEndCount = dwellTime * (CORETIMER_FrequencyGet() / 1000);
    dwellStartCount = CORETIMER_CounterGet();
    isG4Active = 1;
}

/* *****************************************************************************
 End of File
 */