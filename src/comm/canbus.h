/* ************************************************************************** */
/*
  @File Name
    canbus.h
 
  @Author
    Flitch

  @Summary
    CAN BUS communication protocol.

  @Description
    CAN BUS communication protocol.
 */
/* ************************************************************************** */

#ifndef CANBUS_H    /* Guard against multiple inclusion */
#define CANBUS_H

// *****************************************************************************

//Include 
#include "definitions.h"
#include "enums.h"
#include "can_frame.h"
#include "feeder_dictionary.h"
#include "../feeders/slots.h"
#include "../log/log.h"

// *****************************************************************************

//Define #define 

// *****************************************************************************

//Declare


// *****************************************************************************

//Functions
void CanbusInitialize(void);
void CanbusHandler(void);

void CanbusSendCommandToSlot(feeder_command_t *cmd);
void CanbusParseSlotMessage(canbus_msg_t *rcvMsg);
// *****************************************************************************

#endif /* CANBUS_H */

/* *****************************************************************************
 End of File
 */
