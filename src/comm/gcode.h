/* ************************************************************************** */
/*
  @File Name
    gcode.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef GCODE_H    /* Guard against multiple inclusion */
#define GCODE_H

// *****************************************************************************

#include "comm_structs.h" 
#include "enums.h"
#include "definitions.h"

// *****************************************************************************
/*
 * 
 * "G" codes:
 * 
 * G0 - Uncoordinated movmenet: <X>, <Y>, <Z>, <A>, <F>
 * G1 - Coordinated movmenet: <X>, <Y>, <Z>, <A>, <F>  //Not implemented, reditects to G0
 * G4 - dwell S<seconds> or P<miliseconds>
 * G10 - Retracts (home) head height after nozzle change (resets all heads to upper Z)
 * G21 - Connect machine
 * G22 - Disconnect machine 
 * G28 - Perform homing sequence, Confirmed only after homing is completed
 * G92 - Set coordinates of current position to <X,Y,Z,A>
 * 
 * "M" codes:
 * M106 - Pick on <active nozzle>
 * M107 - Place on <active nozzle>
 * M108 - Read pressure on <active nozzle>
 * M109 - Read pressure on <vacuum pump>
 * M110 - Read pressure on <air pump>
 * M114 - Get position
 * M115 - Get firmware version
 * M400 - Wait until all movement ends
 * M600 - Feeder command <F>/<R> on <slot>
 *
 * M800 - Head camera led off
 * M801 - Head camera led on
 * M810 - Base camera led off
 * M811 - Base camera led on
 * M820 - Red led off
 * M821 - Red led on
 * M830 - Blue led off
 * M831 - Blue led on
 * M840 - Green led off
 * M841 - Green led on
 * M850 - Buzzer short beep
 * M851 - Buzzer long beep
 * M860 - Pumps deactivate
 * M861 - Pumps activate
 *
 * "T" codes 
 * T1-T4 - Select an <active nozzle>
 */
// *****************************************************************************

//"G" codes:
void GcodeCommandG0(command_block_t *cmdBlock);
void GcodeCommandG1(command_block_t *cmdBlock);
void GcodeCommandG4(command_block_t *cmdBlock);
void GcodeCommandG10(void);
void GcodeCommandG21(void);
void GcodeCommandG22(void);
void GcodeCommandG28(void);
void GcodeCommandG92(command_block_t *cmdBlock);

//"M" codes:
void GcodeCommandM106(void);
void GcodeCommandM107(void);
void GcodeCommandM108(void);
void GcodeCommandM109(void);
void GcodeCommandM110(void);
void GcodeCommandM114(void);
void GcodeCommandM115(void);
void GcodeCommandM400(void);
void GcodeCommandM600(command_block_t *cmdBlock);

void GcodeCommandM800(void); //Head camera led off
void GcodeCommandM801(void); //Head camera led on
void GcodeCommandM810(void); //Base camera led off
void GcodeCommandM811(void); //Base camera led on
void GcodeCommandM820(void); //Red led off
void GcodeCommandM821(void); //Red led on
void GcodeCommandM830(void); //Blue led off
void GcodeCommandM831(void); //Blue led on
void GcodeCommandM840(void); //Green led off
void GcodeCommandM841(void); //Green led on
void GcodeCommandM850(void); //Buzzer short beep
void GcodeCommandM851(void); //Buzzer long beep
void GcodeCommandM860(void); //Pump deactivate
void GcodeCommandM861(void); //Pump activate

//"T" codes 
void GcodeCommandT(uint32_t num);

//G-code responses
void GcodeRegexCommandConfirm(void);
void GcodeRegexPositionReport(void);
void GcodeRegexFirmwareVersionReport(void);
void GcodeRegexErrorInvalidCommand(void);
void GcodeRegexErrorFatalError(void);
void GcodeRegexErrorEstop(void);
void GcodeRegexErrorFeedFailed(void);
void GcodeRegexHomeComplete(void);
void GcodeRegexMoveToComplete(void);
void GcodeRegexActuatorReadWithDouble(double val);

// *****************************************************************************

#endif /* GCODE_H */

/* *****************************************************************************
 End of File
 */
