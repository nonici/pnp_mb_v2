/* ************************************************************************** */
/*
  @File Name
    comm.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef COMM_H    /* Guard against multiple inclusion */
#define COMM_H    

// *****************************************************************************

#include "serial.h"
#include "parser.h"
#include "wifi.h"
#include "canbus.h"

#include "../feeders/feeders.h"
#include "../feeders/slots.h"

// *****************************************************************************

//#defines

// *****************************************************************************

//Variables

// *****************************************************************************

void CommInitialize(void);
void CommHandler(void);

// *****************************************************************************

#endif /* COMM_H */

/* *****************************************************************************
 End of File
 */
