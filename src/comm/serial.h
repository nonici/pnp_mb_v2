/* ************************************************************************** */
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef SERIAL_H    /* Guard against multiple inclusion */
#define SERIAL_H

// *****************************************************************************

#include "definitions.h"
#include "ring.h"
#include "parser.h"

// *****************************************************************************

void SerialInitialize(void);

void SerialHandler(void);

void SerialReceiveToBuffer(void);

void SerialTransmitFromTxBufferToUart(void);

void SerialTransmitFromBuilderToTxBuffer(void);

void SerialForwardFromRxBufferToParser(void);

uint32_t SerialSpaceInTxFifo(void);

uint32_t SerialDataInTxFifo(void);

uint32_t SerialSpaceInBuilder(void);

void SerialMsgAppendCharToTxFifo(uint8_t ch);

void SerialMsgAppendCharToBuilder(uint8_t ch);

void SerialMsgAppendStringToTxFifo(char * strArray);

void SerialMsgAppendStringToTxBuilder(char * strArray);

// *****************************************************************************

#endif /* SERIAL_H */

/* *****************************************************************************
 End of File
 */
