/* ************************************************************************** */
/*
  @File Name
    shell.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef SHELL_H    /* Guard against multiple inclusion */
#define SHELL_H

// *****************************************************************************

#include "comm_structs.h" 
#include "../log/log.h"

// *****************************************************************************
/*
 * 
 * '$' Shell commands:
 * 
 * L - log commands
 * 
 * 
 * 
 */
// *****************************************************************************

void ShellCommandLog(log_priority_t p);
void ShellCommandReportLog(void);

void ShellCommandLogNew(command_block_t * cmdBlock);
void ShellCommandReportLogNew(command_block_t * cmdBlock);
// *****************************************************************************

#endif /* SHELL_H */

/* *****************************************************************************
 End of File
 */
