// *****************************************************************************
/*
  @File Name
    canbus.h
 
  @Author
    Flitch

  @Summary
    CAN BUS communication protocol.

  @Description
    CAN BUS communication protocol.
 */
// *****************************************************************************

#include "canbus.h"
#include "pnp/signalers.h"
#include "ms_timer.h"

// *****************************************************************************

//Local variables definitions

#define SYNC_TIMER_TIMEOUT_MS 5000

bool CanbusWriteMessage(canbus_msg_t *msg);
bool CanbusReadMessage(canbus_msg_t *msg);

ms_timer_t syncTimer;

// *****************************************************************************

//Local function definitions

void CanbusSendSyncMessage(void);

// *****************************************************************************

//Core function

void CanbusInitialize(void) {
    //CAN1_MessageAcceptanceFilterSet(0, 0x000);
    //CAN1_MessageAcceptanceFilterMaskSet(0, 0x000);
}

void CanbusHandler(void) {

    //Send out sync message if required
    if (!MsTimerIsTimerActive(&syncTimer)) {
        MsTimerDelayMs(&syncTimer, SYNC_TIMER_TIMEOUT_MS);
        CanbusSendSyncMessage();
    }

    //Receive message if any
    canbus_msg_t rcvMsg;
    if (CanbusReadMessage(&rcvMsg)) {

        //Determine source, for now only feeders exists
        if (rcvMsg.nodeId >= 20 && rcvMsg.nodeId <= 40) {
            CanbusParseSlotMessage(&rcvMsg);
        }
    }
}

//Local function

bool CanbusReadMessage(canbus_msg_t *msg) {
    CAN1_ErrorGet();
    bool status = CAN1_MessageReceive(&msg->cobId, &msg->dlc, &msg->data0, 0, 1, &msg->msgAttr);
    if (status) {
        CanGetFunctionCodeAndNodeIdFromCobId(msg);
    }
    return status;
}

bool CanbusWriteMessage(canbus_msg_t *msg) {
    CAN1_ErrorGet();
    return (CAN1_MessageTransmit(msg->cobId, msg->dlc, &msg->data0, 0, msg->msgAttr));
}

//provjeriti

void CanbusSendCommandToSlot(feeder_command_t *cmd) {
    canbus_msg_t txMsg;
    txMsg.nodeId = CanGetNodeIdFromSlotAddress(cmd->slotNumber);
    txMsg.functionCode = PDO1RX;
    txMsg.dlc = 1;
    txMsg.data0 = cmd->compiledCommand;
    txMsg.msgAttr = CAN_MSG_DATA_FRAME;
    CanGetCobIdFromFunctionCodeAndNodeId(&txMsg);
    CanbusWriteMessage(&txMsg);
    LogNewEntry(LogSlotFeedOnSlotInititated, LogCallerObjectSlot, logInfo, (double) cmd->slotNumber, cmd->compiledCommand);
}

void CanbusSendSyncMessage(void) {
    canbus_msg_t syncMsg;
    syncMsg.nodeId = 0;
    syncMsg.functionCode = SYNC;
    CanGetCobIdFromFunctionCodeAndNodeId(&syncMsg);
    syncMsg.dlc = 0;
    syncMsg.msgAttr = CAN_MSG_DATA_FRAME;
    CanbusWriteMessage(&syncMsg);
}

void CanbusParseSlotMessage(canbus_msg_t *rcvMsg) {

    //Determine context, switch case

    //not yet implemented
    if (rcvMsg->functionCode == EMERGENCY) {
    }

    //Feeder status report, answer to command
    if (rcvMsg->functionCode == PDO1TX) {
        SlotParseReportWord(rcvMsg->nodeId, rcvMsg->data0);
    }

    //Keyboard bitmap, sent after keypad is pressed
    if (rcvMsg->functionCode == PDO2TX) {
        DecomposeKeyboardFeederCommand(rcvMsg->nodeId, rcvMsg->data0);
    }

    //Slot status report, reports detected feeder
    if (rcvMsg->functionCode == PDO3TX) {
    }

    //Heartbeat message, reports slot existence and operation
    if (rcvMsg->functionCode == NMTHB) {
    }
}

//Global functions

//Local functions

// *****************************************************************************



/* *****************************************************************************
 End of File
 */
