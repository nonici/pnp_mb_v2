/* ************************************************************************** */
/*
  @File Name
    configuration.h
 
  @Author
    Flitch

  @Summary
    Configuration values.

  @Description
    Declares configurations, options and default values for project.
 */
/* ************************************************************************** */

#ifndef CONFIGURATION_H    /* Guard against multiple inclusion */
#define CONFIGURATION_H

// *****************************************************************************

//Include #include 

// *****************************************************************************

//Define #define 

// *****************************************************************************
// ***********************        Communication     ****************************
// *****************************************************************************

//Size of buffers for serial communication (RX buffer, TX buffer, TX-interrupt buffer)

#define SERIAL_FIFO_COMM_SIZE              1024

// *****************************************************************************
// **************************        Feeders     *******************************
// *****************************************************************************



#define NUMBER_OF_SLOTS 19
#define NUMBER_OF_LANES 4


//Time at which PNP reports error if no response is received from selected feeder (in ms)
#define FEEDER_TIMEOUT_MS 10000

//Angle of rotation for peeler axis per feed. Total angle = _FIXED + (_PER_PITCH * pitch)
#define PEELER_ROTATION_FIXED   120
#define PEELER_ROTATION_PER_PITCH   15      


// *****************************************************************************
// *****************************      Log     **********************************
// *****************************************************************************

//maximum message size in characters
#define LOG_TEXTUAL_BUFFER_SIZE                 200     

//maximum number of stored messages 
#define LOG_BUFFER_SIZE                         2048  

// *****************************************************************************
// **************************        Motion      *******************************
// *****************************************************************************



// *****************************************************************************
// ************************        Pneumatics      *****************************
// *****************************************************************************


// *****************************************************************************
// *************************       Peripherals      ****************************
// *****************************************************************************


// *****************************************************************************
// *************************        Development      ***************************
// *****************************************************************************


//Detailed log reporting when developing motion module
//#define PNP_DEV_MOTION

//Detailed log reporting when developing communication module
//#define PNP_DEV_COMM

//Detailed log reporting when developing pneumatics module
//#define PNP_DEV_PNEUMATICS

//Detailed log reporting when developing feeders module
//#define PNP_DEV_FEEDERS

//Detailed log reporting when developing parser module
//#define PNP_DEV_PARSER

//When enabled, PNP automatically dumps all logs on serial port
//When disabled, PNP will dump logs only after receiving dump command
//#define PNP_DISABLE_LOGS





// *****************************************************************************

//Declare

// *****************************************************************************

//Functions

// *****************************************************************************

#endif /* CONFIGURATION_H */

/* *****************************************************************************
 End of File
 */
