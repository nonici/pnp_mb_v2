// *****************************************************************************
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "definitions.h"                

#include "feeders/slots.h"
#include "pnp/cam_led.h"
#include "pneumatics/pump.h"
#include "pneumatics/sensors.h"
#include "pneumatics/solenoids.h"

#include "comm/comm.h"
#include "log/log.h"
#include "motion/movement.h" 
#include "pnp/tests.h"

#include "system/system.h"

// *****************************************************************************

int main(void) {
    /* Initialize all modules */
    SYS_Initialize(NULL);

    CommInitialize();
    SolenoidsInitialize();
    PumpsInitialize();
    EngineInitialize();
    CameraLedInitialization();
    SystemIntialize();
    TestsInitialize();
    SensorsIntialize();
    LogInitialize();
    SlotInitialize();

    __builtin_enable_interrupts();

    while (true) {

        TestsHandler();

        CommHandler();

        MotionHandler();

        LogHandler();

        FeederSlotHandler();
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE);
}

/*******************************************************************************
 End of File
 */

