/* ************************************************************************** */
/*
  @File Name
    system.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef SYSTEM_H    /* Guard against multiple inclusion */
#define SYSTEM_H

// *****************************************************************************

#include "stringbuilder.h"

// *****************************************************************************

//#defines
extern volatile char sysDbgLetter;
// *****************************************************************************

void SystemIntialize(void);
void SystemTimerInterrupt(void);

uint32_t SystemTimeGet(void);
void SystemTimeStringGet(stringbuilder_t *targetString, uint32_t time);

void SystemEventStamp(void);
void SystemEventDuration(void);
void SystemSetDBGLetter(char ch);
char SystemGetDBGLetter(void);

// *****************************************************************************

#endif /* SYSTEM_H */

/* *****************************************************************************
 End of File
 */
