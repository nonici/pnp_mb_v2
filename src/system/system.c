// *****************************************************************************
/*
  @File Name
    system.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "system.h"
#include "peripheral/tmr/plib_tmr4.h"
#include "../pneumatics/sensors.h"
#include "../comm/serial.h"
#include "../log/log.h"
#include "../feeders/feeders.h"
#include "pnp/signalers.h"
#include "ms_timer.h"

// *****************************************************************************

uint32_t sysTime;
uint32_t sysCounter = 0;
uint32_t logCounter = 0;
uint32_t eventStamp = 0;

// *****************************************************************************

#define THICKS_PER_MS           2

// *****************************************************************************

void SystemIntialize(void) {
    TMR4_Start();
}

void SystemTimerInterrupt(void) {
    //      This interrupt executes every 500 us
    //      It increments sysTime
    //      It ticks ms_timer every two interrupts
    //      reads all six sensor ADC's,
    //      reads all UART buffers
    //      calculate real values for pressure sensors
    //      Checks for emergency stop

    sysTime++;

    //Tick ms_timer every second pass
    if (sysTime % 2) {
        MsTimerTick();
    }


    //Read ADC
    SensorsRead();

    //receive new messages
    SerialReceiveToBuffer();
    FeedersReceiveFromFeeders();

    SensorsConvertAndFilter();

    //
    //    if (logCounter == 20000) {
    //        LogNewEntry(LogSensorsReal, LogCallerObjectSystem, logDebug, (SensorN1 + 1), SensorsGetValueReal(SensorN1));
    //        LogNewEntry(LogSensorsReal, LogCallerObjectSystem, logDebug, (SensorN3 + 1), SensorsGetValueReal(SensorN3));
    //        LogNewEntry(LogSensorsReal, LogCallerObjectSystem, logDebug, (SensorA + 1), SensorsGetValueReal(SensorA));
    //        logCounter = 0;
    //    }

    if (sysCounter == 4000) {
        //SIG_LED_B_Toggle();
        sysCounter = 0;
    }

    (void) EmergencyStopUpdateAndGet();

    logCounter++;
    sysCounter++;

}

uint32_t SystemTimeGet(void) {
    return (sysTime);
}

void SystemTimeStringGet(stringbuilder_t *targetString, uint32_t time) {
    //max val: 499:59:59:999.5

    uint32_t us = (time % 2) * 5;
    uint32_t ms = time / 2;
    uint32_t sec = 0;
    uint32_t min = 0;
    uint32_t hours = 0;

    if (ms > 1000) {
        sec = ms / (1000);
        ms = ms - (sec * 1000);
    }
    if (sec > 60) {
        min = sec / (60);
        sec = sec - (min * 60);
    }
    if (min > 60) {
        hours = min / (60);
        min = min - (hours * 60);
    }

    StringbuilderAppendU32(targetString, hours);
    StringbuilderAppendChar(targetString, ':');
    StringbuilderAppendU32(targetString, min);
    StringbuilderAppendChar(targetString, ':');
    StringbuilderAppendU32(targetString, sec);
    StringbuilderAppendChar(targetString, ':');
    StringbuilderAppendU32(targetString, ms);
    StringbuilderAppendChar(targetString, '.');
    StringbuilderAppendU32(targetString, us);
}

void SystemEventStamp(void) {
    eventStamp = CORETIMER_CounterGet();
}

void SystemEventDuration(void) {
    double duration = (double) (CORETIMER_CounterGet() - eventStamp)* 0.00793650794d;
    LogNewEntry(LogEventDuration, LogCallerObjectSystem, logDebug, 0, duration);
}

/* *****************************************************************************
 End of File
 */
