/*******************************************************************************
  GPIO PLIB

  Company:
    Microchip Technology Inc.

  File Name:
    plib_gpio.h

  Summary:
    GPIO PLIB Header File

  Description:
    This library provides an interface to control and interact with Parallel
    Input/Output controller (GPIO) module.

*******************************************************************************/

/*******************************************************************************
* Copyright (C) 2019 Microchip Technology Inc. and its subsidiaries.
*
* Subject to your compliance with these terms, you may use Microchip software
* and any derivatives exclusively with Microchip products. It is your
* responsibility to comply with third party license terms applicable to your
* use of third party software (including open source software) that may
* accompany Microchip software.
*
* THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
* EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
* WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
* PARTICULAR PURPOSE.
*
* IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
* INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
* WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
* BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
* FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
* ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
* THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
*******************************************************************************/

#ifndef PLIB_GPIO_H
#define PLIB_GPIO_H

#include <device.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

    extern "C" {

#endif
// DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Data types and constants
// *****************************************************************************
// *****************************************************************************


/*** Macros for END_B2 pin ***/
#define END_B2_Set()               (LATGSET = (1<<15))
#define END_B2_Clear()             (LATGCLR = (1<<15))
#define END_B2_Toggle()            (LATGINV= (1<<15))
#define END_B2_OutputEnable()      (TRISGCLR = (1<<15))
#define END_B2_InputEnable()       (TRISGSET = (1<<15))
#define END_B2_Get()               ((PORTG >> 15) & 0x1)
#define END_B2_PIN                  GPIO_PIN_RG15

/*** Macros for END_B1 pin ***/
#define END_B1_Set()               (LATASET = (1<<5))
#define END_B1_Clear()             (LATACLR = (1<<5))
#define END_B1_Toggle()            (LATAINV= (1<<5))
#define END_B1_OutputEnable()      (TRISACLR = (1<<5))
#define END_B1_InputEnable()       (TRISASET = (1<<5))
#define END_B1_Get()               ((PORTA >> 5) & 0x1)
#define END_B1_PIN                  GPIO_PIN_RA5

/*** Macros for __FIX__UART1_RX pin ***/
#define __FIX__UART1_RX_Get()               ((PORTE >> 5) & 0x1)
#define __FIX__UART1_RX_PIN                  GPIO_PIN_RE5

/*** Macros for DRV_B1_DIR pin ***/
#define DRV_B1_DIR_Set()               (LATESET = (1<<6))
#define DRV_B1_DIR_Clear()             (LATECLR = (1<<6))
#define DRV_B1_DIR_Toggle()            (LATEINV= (1<<6))
#define DRV_B1_DIR_OutputEnable()      (TRISECLR = (1<<6))
#define DRV_B1_DIR_InputEnable()       (TRISESET = (1<<6))
#define DRV_B1_DIR_Get()               ((PORTE >> 6) & 0x1)
#define DRV_B1_DIR_PIN                  GPIO_PIN_RE6

/*** Macros for DRV_B_EN pin ***/
#define DRV_B_EN_Set()               (LATESET = (1<<7))
#define DRV_B_EN_Clear()             (LATECLR = (1<<7))
#define DRV_B_EN_Toggle()            (LATEINV= (1<<7))
#define DRV_B_EN_OutputEnable()      (TRISECLR = (1<<7))
#define DRV_B_EN_InputEnable()       (TRISESET = (1<<7))
#define DRV_B_EN_Get()               ((PORTE >> 7) & 0x1)
#define DRV_B_EN_PIN                  GPIO_PIN_RE7

/*** Macros for FET_CRTL_21 pin ***/
#define FET_CRTL_21_Set()               (LATCSET = (1<<1))
#define FET_CRTL_21_Clear()             (LATCCLR = (1<<1))
#define FET_CRTL_21_Toggle()            (LATCINV= (1<<1))
#define FET_CRTL_21_OutputEnable()      (TRISCCLR = (1<<1))
#define FET_CRTL_21_InputEnable()       (TRISCSET = (1<<1))
#define FET_CRTL_21_Get()               ((PORTC >> 1) & 0x1)
#define FET_CRTL_21_PIN                  GPIO_PIN_RC1

/*** Macros for DRV_B1_STP pin ***/
#define DRV_B1_STP_Set()               (LATJSET = (1<<8))
#define DRV_B1_STP_Clear()             (LATJCLR = (1<<8))
#define DRV_B1_STP_Toggle()            (LATJINV= (1<<8))
#define DRV_B1_STP_OutputEnable()      (TRISJCLR = (1<<8))
#define DRV_B1_STP_InputEnable()       (TRISJSET = (1<<8))
#define DRV_B1_STP_Get()               ((PORTJ >> 8) & 0x1)
#define DRV_B1_STP_PIN                  GPIO_PIN_RJ8

/*** Macros for DRV_B2_STP pin ***/
#define DRV_B2_STP_Set()               (LATJSET = (1<<9))
#define DRV_B2_STP_Clear()             (LATJCLR = (1<<9))
#define DRV_B2_STP_Toggle()            (LATJINV= (1<<9))
#define DRV_B2_STP_OutputEnable()      (TRISJCLR = (1<<9))
#define DRV_B2_STP_InputEnable()       (TRISJSET = (1<<9))
#define DRV_B2_STP_Get()               ((PORTJ >> 9) & 0x1)
#define DRV_B2_STP_PIN                  GPIO_PIN_RJ9

/*** Macros for DRV_C1_STP pin ***/
#define DRV_C1_STP_Set()               (LATJSET = (1<<12))
#define DRV_C1_STP_Clear()             (LATJCLR = (1<<12))
#define DRV_C1_STP_Toggle()            (LATJINV= (1<<12))
#define DRV_C1_STP_OutputEnable()      (TRISJCLR = (1<<12))
#define DRV_C1_STP_InputEnable()       (TRISJSET = (1<<12))
#define DRV_C1_STP_Get()               ((PORTJ >> 12) & 0x1)
#define DRV_C1_STP_PIN                  GPIO_PIN_RJ12

/*** Macros for DRV_C2_STP pin ***/
#define DRV_C2_STP_Set()               (LATJSET = (1<<10))
#define DRV_C2_STP_Clear()             (LATJCLR = (1<<10))
#define DRV_C2_STP_Toggle()            (LATJINV= (1<<10))
#define DRV_C2_STP_OutputEnable()      (TRISJCLR = (1<<10))
#define DRV_C2_STP_InputEnable()       (TRISJSET = (1<<10))
#define DRV_C2_STP_Get()               ((PORTJ >> 10) & 0x1)
#define DRV_C2_STP_PIN                  GPIO_PIN_RJ10

/*** Macros for FET_CRTL_20 pin ***/
#define FET_CRTL_20_Set()               (LATCSET = (1<<2))
#define FET_CRTL_20_Clear()             (LATCCLR = (1<<2))
#define FET_CRTL_20_Toggle()            (LATCINV= (1<<2))
#define FET_CRTL_20_OutputEnable()      (TRISCCLR = (1<<2))
#define FET_CRTL_20_InputEnable()       (TRISCSET = (1<<2))
#define FET_CRTL_20_Get()               ((PORTC >> 2) & 0x1)
#define FET_CRTL_20_PIN                  GPIO_PIN_RC2

/*** Macros for FET_CRTL_19 pin ***/
#define FET_CRTL_19_Set()               (LATCSET = (1<<3))
#define FET_CRTL_19_Clear()             (LATCCLR = (1<<3))
#define FET_CRTL_19_Toggle()            (LATCINV= (1<<3))
#define FET_CRTL_19_OutputEnable()      (TRISCCLR = (1<<3))
#define FET_CRTL_19_InputEnable()       (TRISCSET = (1<<3))
#define FET_CRTL_19_Get()               ((PORTC >> 3) & 0x1)
#define FET_CRTL_19_PIN                  GPIO_PIN_RC3

/*** Macros for FET_CRTL_18 pin ***/
#define FET_CRTL_18_Set()               (LATCSET = (1<<4))
#define FET_CRTL_18_Clear()             (LATCCLR = (1<<4))
#define FET_CRTL_18_Toggle()            (LATCINV= (1<<4))
#define FET_CRTL_18_OutputEnable()      (TRISCCLR = (1<<4))
#define FET_CRTL_18_InputEnable()       (TRISCSET = (1<<4))
#define FET_CRTL_18_Get()               ((PORTC >> 4) & 0x1)
#define FET_CRTL_18_PIN                  GPIO_PIN_RC4

/*** Macros for FET_CRTL_17 pin ***/
#define FET_CRTL_17_Set()               (LATGSET = (1<<6))
#define FET_CRTL_17_Clear()             (LATGCLR = (1<<6))
#define FET_CRTL_17_Toggle()            (LATGINV= (1<<6))
#define FET_CRTL_17_OutputEnable()      (TRISGCLR = (1<<6))
#define FET_CRTL_17_InputEnable()       (TRISGSET = (1<<6))
#define FET_CRTL_17_Get()               ((PORTG >> 6) & 0x1)
#define FET_CRTL_17_PIN                  GPIO_PIN_RG6

/*** Macros for FET_CRTL_16 pin ***/
#define FET_CRTL_16_Set()               (LATGSET = (1<<7))
#define FET_CRTL_16_Clear()             (LATGCLR = (1<<7))
#define FET_CRTL_16_Toggle()            (LATGINV= (1<<7))
#define FET_CRTL_16_OutputEnable()      (TRISGCLR = (1<<7))
#define FET_CRTL_16_InputEnable()       (TRISGSET = (1<<7))
#define FET_CRTL_16_Get()               ((PORTG >> 7) & 0x1)
#define FET_CRTL_16_PIN                  GPIO_PIN_RG7

/*** Macros for FET_CRTL_15 pin ***/
#define FET_CRTL_15_Set()               (LATGSET = (1<<8))
#define FET_CRTL_15_Clear()             (LATGCLR = (1<<8))
#define FET_CRTL_15_Toggle()            (LATGINV= (1<<8))
#define FET_CRTL_15_OutputEnable()      (TRISGCLR = (1<<8))
#define FET_CRTL_15_InputEnable()       (TRISGSET = (1<<8))
#define FET_CRTL_15_Get()               ((PORTG >> 8) & 0x1)
#define FET_CRTL_15_PIN                  GPIO_PIN_RG8

/*** Macros for DRV_B1_ALM pin ***/
#define DRV_B1_ALM_Set()               (LATKSET = (1<<0))
#define DRV_B1_ALM_Clear()             (LATKCLR = (1<<0))
#define DRV_B1_ALM_Toggle()            (LATKINV= (1<<0))
#define DRV_B1_ALM_OutputEnable()      (TRISKCLR = (1<<0))
#define DRV_B1_ALM_InputEnable()       (TRISKSET = (1<<0))
#define DRV_B1_ALM_Get()               ((PORTK >> 0) & 0x1)
#define DRV_B1_ALM_PIN                  GPIO_PIN_RK0

/*** Macros for FET_CRTL_14 pin ***/
#define FET_CRTL_14_Set()               (LATGSET = (1<<9))
#define FET_CRTL_14_Clear()             (LATGCLR = (1<<9))
#define FET_CRTL_14_Toggle()            (LATGINV= (1<<9))
#define FET_CRTL_14_OutputEnable()      (TRISGCLR = (1<<9))
#define FET_CRTL_14_InputEnable()       (TRISGSET = (1<<9))
#define FET_CRTL_14_Get()               ((PORTG >> 9) & 0x1)
#define FET_CRTL_14_PIN                  GPIO_PIN_RG9

/*** Macros for DRV_B2_ALM pin ***/
#define DRV_B2_ALM_Set()               (LATASET = (1<<0))
#define DRV_B2_ALM_Clear()             (LATACLR = (1<<0))
#define DRV_B2_ALM_Toggle()            (LATAINV= (1<<0))
#define DRV_B2_ALM_OutputEnable()      (TRISACLR = (1<<0))
#define DRV_B2_ALM_InputEnable()       (TRISASET = (1<<0))
#define DRV_B2_ALM_Get()               ((PORTA >> 0) & 0x1)
#define DRV_B2_ALM_PIN                  GPIO_PIN_RA0

/*** Macros for FET_CRTL_01 pin ***/
#define FET_CRTL_01_Set()               (LATESET = (1<<8))
#define FET_CRTL_01_Clear()             (LATECLR = (1<<8))
#define FET_CRTL_01_Toggle()            (LATEINV= (1<<8))
#define FET_CRTL_01_OutputEnable()      (TRISECLR = (1<<8))
#define FET_CRTL_01_InputEnable()       (TRISESET = (1<<8))
#define FET_CRTL_01_Get()               ((PORTE >> 8) & 0x1)
#define FET_CRTL_01_PIN                  GPIO_PIN_RE8

/*** Macros for FET_CRTL_02 pin ***/
#define FET_CRTL_02_Set()               (LATESET = (1<<9))
#define FET_CRTL_02_Clear()             (LATECLR = (1<<9))
#define FET_CRTL_02_Toggle()            (LATEINV= (1<<9))
#define FET_CRTL_02_OutputEnable()      (TRISECLR = (1<<9))
#define FET_CRTL_02_InputEnable()       (TRISESET = (1<<9))
#define FET_CRTL_02_Get()               ((PORTE >> 9) & 0x1)
#define FET_CRTL_02_PIN                  GPIO_PIN_RE9

/*** Macros for FET_CRTL_13 pin ***/
#define FET_CRTL_13_Set()               (LATBSET = (1<<5))
#define FET_CRTL_13_Clear()             (LATBCLR = (1<<5))
#define FET_CRTL_13_Toggle()            (LATBINV= (1<<5))
#define FET_CRTL_13_OutputEnable()      (TRISBCLR = (1<<5))
#define FET_CRTL_13_InputEnable()       (TRISBSET = (1<<5))
#define FET_CRTL_13_Get()               ((PORTB >> 5) & 0x1)
#define FET_CRTL_13_PIN                  GPIO_PIN_RB5

/*** Macros for DRV_C1_DIR pin ***/
#define DRV_C1_DIR_Set()               (LATBSET = (1<<4))
#define DRV_C1_DIR_Clear()             (LATBCLR = (1<<4))
#define DRV_C1_DIR_Toggle()            (LATBINV= (1<<4))
#define DRV_C1_DIR_OutputEnable()      (TRISBCLR = (1<<4))
#define DRV_C1_DIR_InputEnable()       (TRISBSET = (1<<4))
#define DRV_C1_DIR_Get()               ((PORTB >> 4) & 0x1)
#define DRV_C1_DIR_PIN                  GPIO_PIN_RB4

/*** Macros for DRV_D1_STP pin ***/
#define DRV_D1_STP_Set()               (LATJSET = (1<<11))
#define DRV_D1_STP_Clear()             (LATJCLR = (1<<11))
#define DRV_D1_STP_Toggle()            (LATJINV= (1<<11))
#define DRV_D1_STP_OutputEnable()      (TRISJCLR = (1<<11))
#define DRV_D1_STP_InputEnable()       (TRISJSET = (1<<11))
#define DRV_D1_STP_Get()               ((PORTJ >> 11) & 0x1)
#define DRV_D1_STP_PIN                  GPIO_PIN_RJ11

/*** Macros for DRV_D2_STP pin ***/
#define DRV_D2_STP_Set()               (LATJSET = (1<<13))
#define DRV_D2_STP_Clear()             (LATJCLR = (1<<13))
#define DRV_D2_STP_Toggle()            (LATJINV= (1<<13))
#define DRV_D2_STP_OutputEnable()      (TRISJCLR = (1<<13))
#define DRV_D2_STP_InputEnable()       (TRISJSET = (1<<13))
#define DRV_D2_STP_Get()               ((PORTJ >> 13) & 0x1)
#define DRV_D2_STP_PIN                  GPIO_PIN_RJ13

/*** Macros for DRV_E1_STP pin ***/
#define DRV_E1_STP_Set()               (LATJSET = (1<<14))
#define DRV_E1_STP_Clear()             (LATJCLR = (1<<14))
#define DRV_E1_STP_Toggle()            (LATJINV= (1<<14))
#define DRV_E1_STP_OutputEnable()      (TRISJCLR = (1<<14))
#define DRV_E1_STP_InputEnable()       (TRISJSET = (1<<14))
#define DRV_E1_STP_Get()               ((PORTJ >> 14) & 0x1)
#define DRV_E1_STP_PIN                  GPIO_PIN_RJ14

/*** Macros for DRV_E2_STP pin ***/
#define DRV_E2_STP_Set()               (LATJSET = (1<<15))
#define DRV_E2_STP_Clear()             (LATJCLR = (1<<15))
#define DRV_E2_STP_Toggle()            (LATJINV= (1<<15))
#define DRV_E2_STP_OutputEnable()      (TRISJCLR = (1<<15))
#define DRV_E2_STP_InputEnable()       (TRISJSET = (1<<15))
#define DRV_E2_STP_Get()               ((PORTJ >> 15) & 0x1)
#define DRV_E2_STP_PIN                  GPIO_PIN_RJ15

/*** Macros for FET_CRTL_12 pin ***/
#define FET_CRTL_12_Set()               (LATBSET = (1<<3))
#define FET_CRTL_12_Clear()             (LATBCLR = (1<<3))
#define FET_CRTL_12_Toggle()            (LATBINV= (1<<3))
#define FET_CRTL_12_OutputEnable()      (TRISBCLR = (1<<3))
#define FET_CRTL_12_InputEnable()       (TRISBSET = (1<<3))
#define FET_CRTL_12_Get()               ((PORTB >> 3) & 0x1)
#define FET_CRTL_12_PIN                  GPIO_PIN_RB3

/*** Macros for FET_CRTL_11 pin ***/
#define FET_CRTL_11_Set()               (LATBSET = (1<<2))
#define FET_CRTL_11_Clear()             (LATBCLR = (1<<2))
#define FET_CRTL_11_Toggle()            (LATBINV= (1<<2))
#define FET_CRTL_11_OutputEnable()      (TRISBCLR = (1<<2))
#define FET_CRTL_11_InputEnable()       (TRISBSET = (1<<2))
#define FET_CRTL_11_Get()               ((PORTB >> 2) & 0x1)
#define FET_CRTL_11_PIN                  GPIO_PIN_RB2

/*** Macros for FET_CRTL_10 pin ***/
#define FET_CRTL_10_Set()               (LATBSET = (1<<6))
#define FET_CRTL_10_Clear()             (LATBCLR = (1<<6))
#define FET_CRTL_10_Toggle()            (LATBINV= (1<<6))
#define FET_CRTL_10_OutputEnable()      (TRISBCLR = (1<<6))
#define FET_CRTL_10_InputEnable()       (TRISBSET = (1<<6))
#define FET_CRTL_10_Get()               ((PORTB >> 6) & 0x1)
#define FET_CRTL_10_PIN                  GPIO_PIN_RB6

/*** Macros for FET_CRTL_09 pin ***/
#define FET_CRTL_09_Set()               (LATBSET = (1<<7))
#define FET_CRTL_09_Clear()             (LATBCLR = (1<<7))
#define FET_CRTL_09_Toggle()            (LATBINV= (1<<7))
#define FET_CRTL_09_OutputEnable()      (TRISBCLR = (1<<7))
#define FET_CRTL_09_InputEnable()       (TRISBSET = (1<<7))
#define FET_CRTL_09_Get()               ((PORTB >> 7) & 0x1)
#define FET_CRTL_09_PIN                  GPIO_PIN_RB7

/*** Macros for DRV_C_EN pin ***/
#define DRV_C_EN_Set()               (LATASET = (1<<9))
#define DRV_C_EN_Clear()             (LATACLR = (1<<9))
#define DRV_C_EN_Toggle()            (LATAINV= (1<<9))
#define DRV_C_EN_OutputEnable()      (TRISACLR = (1<<9))
#define DRV_C_EN_InputEnable()       (TRISASET = (1<<9))
#define DRV_C_EN_Get()               ((PORTA >> 9) & 0x1)
#define DRV_C_EN_PIN                  GPIO_PIN_RA9

/*** Macros for DRV_D2_ALM pin ***/
#define DRV_D2_ALM_Set()               (LATASET = (1<<10))
#define DRV_D2_ALM_Clear()             (LATACLR = (1<<10))
#define DRV_D2_ALM_Toggle()            (LATAINV= (1<<10))
#define DRV_D2_ALM_OutputEnable()      (TRISACLR = (1<<10))
#define DRV_D2_ALM_InputEnable()       (TRISASET = (1<<10))
#define DRV_D2_ALM_Get()               ((PORTA >> 10) & 0x1)
#define DRV_D2_ALM_PIN                  GPIO_PIN_RA10

/*** Macros for DRV_D_EN pin ***/
#define DRV_D_EN_Set()               (LATHSET = (1<<0))
#define DRV_D_EN_Clear()             (LATHCLR = (1<<0))
#define DRV_D_EN_Toggle()            (LATHINV= (1<<0))
#define DRV_D_EN_OutputEnable()      (TRISHCLR = (1<<0))
#define DRV_D_EN_InputEnable()       (TRISHSET = (1<<0))
#define DRV_D_EN_Get()               ((PORTH >> 0) & 0x1)
#define DRV_D_EN_PIN                  GPIO_PIN_RH0

/*** Macros for END_D1 pin ***/
#define END_D1_Set()               (LATHSET = (1<<1))
#define END_D1_Clear()             (LATHCLR = (1<<1))
#define END_D1_Toggle()            (LATHINV= (1<<1))
#define END_D1_OutputEnable()      (TRISHCLR = (1<<1))
#define END_D1_InputEnable()       (TRISHSET = (1<<1))
#define END_D1_Get()               ((PORTH >> 1) & 0x1)
#define END_D1_PIN                  GPIO_PIN_RH1

/*** Macros for DRV_D1_DIR pin ***/
#define DRV_D1_DIR_Set()               (LATHSET = (1<<2))
#define DRV_D1_DIR_Clear()             (LATHCLR = (1<<2))
#define DRV_D1_DIR_Toggle()            (LATHINV= (1<<2))
#define DRV_D1_DIR_OutputEnable()      (TRISHCLR = (1<<2))
#define DRV_D1_DIR_InputEnable()       (TRISHSET = (1<<2))
#define DRV_D1_DIR_Get()               ((PORTH >> 2) & 0x1)
#define DRV_D1_DIR_PIN                  GPIO_PIN_RH2

/*** Macros for END_D2 pin ***/
#define END_D2_Set()               (LATHSET = (1<<3))
#define END_D2_Clear()             (LATHCLR = (1<<3))
#define END_D2_Toggle()            (LATHINV= (1<<3))
#define END_D2_OutputEnable()      (TRISHCLR = (1<<3))
#define END_D2_InputEnable()       (TRISHSET = (1<<3))
#define END_D2_Get()               ((PORTH >> 3) & 0x1)
#define END_D2_PIN                  GPIO_PIN_RH3

/*** Macros for FET_CRTL_08 pin ***/
#define FET_CRTL_08_Set()               (LATBSET = (1<<8))
#define FET_CRTL_08_Clear()             (LATBCLR = (1<<8))
#define FET_CRTL_08_Toggle()            (LATBINV= (1<<8))
#define FET_CRTL_08_OutputEnable()      (TRISBCLR = (1<<8))
#define FET_CRTL_08_InputEnable()       (TRISBSET = (1<<8))
#define FET_CRTL_08_Get()               ((PORTB >> 8) & 0x1)
#define FET_CRTL_08_PIN                  GPIO_PIN_RB8

/*** Macros for FET_CRTL_07 pin ***/
#define FET_CRTL_07_Set()               (LATBSET = (1<<9))
#define FET_CRTL_07_Clear()             (LATBCLR = (1<<9))
#define FET_CRTL_07_Toggle()            (LATBINV= (1<<9))
#define FET_CRTL_07_OutputEnable()      (TRISBCLR = (1<<9))
#define FET_CRTL_07_InputEnable()       (TRISBSET = (1<<9))
#define FET_CRTL_07_Get()               ((PORTB >> 9) & 0x1)
#define FET_CRTL_07_PIN                  GPIO_PIN_RB9

/*** Macros for PNEUM_SENSOR_S2 pin ***/
#define PNEUM_SENSOR_S2_Get()               ((PORTB >> 10) & 0x1)
#define PNEUM_SENSOR_S2_PIN                  GPIO_PIN_RB10

/*** Macros for PNEUM_SENSOR_S5 pin ***/
#define PNEUM_SENSOR_S5_Get()               ((PORTB >> 11) & 0x1)
#define PNEUM_SENSOR_S5_PIN                  GPIO_PIN_RB11

/*** Macros for DRV_D2_DIR pin ***/
#define DRV_D2_DIR_Set()               (LATKSET = (1<<1))
#define DRV_D2_DIR_Clear()             (LATKCLR = (1<<1))
#define DRV_D2_DIR_Toggle()            (LATKINV= (1<<1))
#define DRV_D2_DIR_OutputEnable()      (TRISKCLR = (1<<1))
#define DRV_D2_DIR_InputEnable()       (TRISKSET = (1<<1))
#define DRV_D2_DIR_Get()               ((PORTK >> 1) & 0x1)
#define DRV_D2_DIR_PIN                  GPIO_PIN_RK1

/*** Macros for DRV_D1_ALM pin ***/
#define DRV_D1_ALM_Set()               (LATKSET = (1<<2))
#define DRV_D1_ALM_Clear()             (LATKCLR = (1<<2))
#define DRV_D1_ALM_Toggle()            (LATKINV= (1<<2))
#define DRV_D1_ALM_OutputEnable()      (TRISKCLR = (1<<2))
#define DRV_D1_ALM_InputEnable()       (TRISKSET = (1<<2))
#define DRV_D1_ALM_Get()               ((PORTK >> 2) & 0x1)
#define DRV_D1_ALM_PIN                  GPIO_PIN_RK2

/*** Macros for END_E1 pin ***/
#define END_E1_Set()               (LATKSET = (1<<3))
#define END_E1_Clear()             (LATKCLR = (1<<3))
#define END_E1_Toggle()            (LATKINV= (1<<3))
#define END_E1_OutputEnable()      (TRISKCLR = (1<<3))
#define END_E1_InputEnable()       (TRISKSET = (1<<3))
#define END_E1_Get()               ((PORTK >> 3) & 0x1)
#define END_E1_PIN                  GPIO_PIN_RK3

/*** Macros for DRV_E1_DIR pin ***/
#define DRV_E1_DIR_Set()               (LATASET = (1<<1))
#define DRV_E1_DIR_Clear()             (LATACLR = (1<<1))
#define DRV_E1_DIR_Toggle()            (LATAINV= (1<<1))
#define DRV_E1_DIR_OutputEnable()      (TRISACLR = (1<<1))
#define DRV_E1_DIR_InputEnable()       (TRISASET = (1<<1))
#define DRV_E1_DIR_Get()               ((PORTA >> 1) & 0x1)
#define DRV_E1_DIR_PIN                  GPIO_PIN_RA1

/*** Macros for FET_CRTL_06 pin ***/
#define FET_CRTL_06_Set()               (LATFSET = (1<<13))
#define FET_CRTL_06_Clear()             (LATFCLR = (1<<13))
#define FET_CRTL_06_Toggle()            (LATFINV= (1<<13))
#define FET_CRTL_06_OutputEnable()      (TRISFCLR = (1<<13))
#define FET_CRTL_06_InputEnable()       (TRISFSET = (1<<13))
#define FET_CRTL_06_Get()               ((PORTF >> 13) & 0x1)
#define FET_CRTL_06_PIN                  GPIO_PIN_RF13

/*** Macros for FET_CRTL_05 pin ***/
#define FET_CRTL_05_Set()               (LATFSET = (1<<12))
#define FET_CRTL_05_Clear()             (LATFCLR = (1<<12))
#define FET_CRTL_05_Toggle()            (LATFINV= (1<<12))
#define FET_CRTL_05_OutputEnable()      (TRISFCLR = (1<<12))
#define FET_CRTL_05_InputEnable()       (TRISFSET = (1<<12))
#define FET_CRTL_05_Get()               ((PORTF >> 12) & 0x1)
#define FET_CRTL_05_PIN                  GPIO_PIN_RF12

/*** Macros for PNEUM_SENSOR_S3 pin ***/
#define PNEUM_SENSOR_S3_Get()               ((PORTB >> 12) & 0x1)
#define PNEUM_SENSOR_S3_PIN                  GPIO_PIN_RB12

/*** Macros for PNEUM_SENSOR_S6 pin ***/
#define PNEUM_SENSOR_S6_Get()               ((PORTB >> 13) & 0x1)
#define PNEUM_SENSOR_S6_PIN                  GPIO_PIN_RB13

/*** Macros for PNEUM_SENSOR_S1 pin ***/
#define PNEUM_SENSOR_S1_Get()               ((PORTB >> 14) & 0x1)
#define PNEUM_SENSOR_S1_PIN                  GPIO_PIN_RB14

/*** Macros for PNEUM_SENSOR_S4 pin ***/
#define PNEUM_SENSOR_S4_Get()               ((PORTB >> 15) & 0x1)
#define PNEUM_SENSOR_S4_PIN                  GPIO_PIN_RB15

/*** Macros for DRV_E_EN pin ***/
#define DRV_E_EN_Set()               (LATHSET = (1<<4))
#define DRV_E_EN_Clear()             (LATHCLR = (1<<4))
#define DRV_E_EN_Toggle()            (LATHINV= (1<<4))
#define DRV_E_EN_OutputEnable()      (TRISHCLR = (1<<4))
#define DRV_E_EN_InputEnable()       (TRISHSET = (1<<4))
#define DRV_E_EN_Get()               ((PORTH >> 4) & 0x1)
#define DRV_E_EN_PIN                  GPIO_PIN_RH4

/*** Macros for END_E2 pin ***/
#define END_E2_Set()               (LATHSET = (1<<5))
#define END_E2_Clear()             (LATHCLR = (1<<5))
#define END_E2_Toggle()            (LATHINV= (1<<5))
#define END_E2_OutputEnable()      (TRISHCLR = (1<<5))
#define END_E2_InputEnable()       (TRISHSET = (1<<5))
#define END_E2_Get()               ((PORTH >> 5) & 0x1)
#define END_E2_PIN                  GPIO_PIN_RH5

/*** Macros for DRV_E2_DIR pin ***/
#define DRV_E2_DIR_Set()               (LATHSET = (1<<6))
#define DRV_E2_DIR_Clear()             (LATHCLR = (1<<6))
#define DRV_E2_DIR_Toggle()            (LATHINV= (1<<6))
#define DRV_E2_DIR_OutputEnable()      (TRISHCLR = (1<<6))
#define DRV_E2_DIR_InputEnable()       (TRISHSET = (1<<6))
#define DRV_E2_DIR_Get()               ((PORTH >> 6) & 0x1)
#define DRV_E2_DIR_PIN                  GPIO_PIN_RH6

/*** Macros for DRV_E1_ALM pin ***/
#define DRV_E1_ALM_Set()               (LATHSET = (1<<7))
#define DRV_E1_ALM_Clear()             (LATHCLR = (1<<7))
#define DRV_E1_ALM_Toggle()            (LATHINV= (1<<7))
#define DRV_E1_ALM_OutputEnable()      (TRISHCLR = (1<<7))
#define DRV_E1_ALM_InputEnable()       (TRISHSET = (1<<7))
#define DRV_E1_ALM_Get()               ((PORTH >> 7) & 0x1)
#define DRV_E1_ALM_PIN                  GPIO_PIN_RH7

/*** Macros for FET_CRTL_04 pin ***/
#define FET_CRTL_04_Set()               (LATDSET = (1<<14))
#define FET_CRTL_04_Clear()             (LATDCLR = (1<<14))
#define FET_CRTL_04_Toggle()            (LATDINV= (1<<14))
#define FET_CRTL_04_OutputEnable()      (TRISDCLR = (1<<14))
#define FET_CRTL_04_InputEnable()       (TRISDSET = (1<<14))
#define FET_CRTL_04_Get()               ((PORTD >> 14) & 0x1)
#define FET_CRTL_04_PIN                  GPIO_PIN_RD14

/*** Macros for FET_CRTL_03 pin ***/
#define FET_CRTL_03_Set()               (LATDSET = (1<<15))
#define FET_CRTL_03_Clear()             (LATDCLR = (1<<15))
#define FET_CRTL_03_Toggle()            (LATDINV= (1<<15))
#define FET_CRTL_03_OutputEnable()      (TRISDCLR = (1<<15))
#define FET_CRTL_03_InputEnable()       (TRISDSET = (1<<15))
#define FET_CRTL_03_Get()               ((PORTD >> 15) & 0x1)
#define FET_CRTL_03_PIN                  GPIO_PIN_RD15

/*** Macros for CLK_12MHZ pin ***/
#define CLK_12MHZ_Get()               ((PORTC >> 12) & 0x1)
#define CLK_12MHZ_PIN                  GPIO_PIN_RC12

/*** Macros for DRV_E2_ALM pin ***/
#define DRV_E2_ALM_Set()               (LATCSET = (1<<15))
#define DRV_E2_ALM_Clear()             (LATCCLR = (1<<15))
#define DRV_E2_ALM_Toggle()            (LATCINV= (1<<15))
#define DRV_E2_ALM_OutputEnable()      (TRISCCLR = (1<<15))
#define DRV_E2_ALM_InputEnable()       (TRISCSET = (1<<15))
#define DRV_E2_ALM_Get()               ((PORTC >> 15) & 0x1)
#define DRV_E2_ALM_PIN                  GPIO_PIN_RC15

/*** Macros for DRV_C2_ALM pin ***/
#define DRV_C2_ALM_Set()               (LATFSET = (1<<3))
#define DRV_C2_ALM_Clear()             (LATFCLR = (1<<3))
#define DRV_C2_ALM_Toggle()            (LATFINV= (1<<3))
#define DRV_C2_ALM_OutputEnable()      (TRISFCLR = (1<<3))
#define DRV_C2_ALM_InputEnable()       (TRISFSET = (1<<3))
#define DRV_C2_ALM_Get()               ((PORTF >> 3) & 0x1)
#define DRV_C2_ALM_PIN                  GPIO_PIN_RF3

/*** Macros for END_YR_MIN pin ***/
#define END_YR_MIN_Set()               (LATFSET = (1<<2))
#define END_YR_MIN_Clear()             (LATFCLR = (1<<2))
#define END_YR_MIN_Toggle()            (LATFINV= (1<<2))
#define END_YR_MIN_OutputEnable()      (TRISFCLR = (1<<2))
#define END_YR_MIN_InputEnable()       (TRISFSET = (1<<2))
#define END_YR_MIN_Get()               ((PORTF >> 2) & 0x1)
#define END_YR_MIN_PIN                  GPIO_PIN_RF2

/*** Macros for END_YR_MAX pin ***/
#define END_YR_MAX_Set()               (LATFSET = (1<<8))
#define END_YR_MAX_Clear()             (LATFCLR = (1<<8))
#define END_YR_MAX_Toggle()            (LATFINV= (1<<8))
#define END_YR_MAX_OutputEnable()      (TRISFCLR = (1<<8))
#define END_YR_MAX_InputEnable()       (TRISFSET = (1<<8))
#define END_YR_MAX_Get()               ((PORTF >> 8) & 0x1)
#define END_YR_MAX_PIN                  GPIO_PIN_RF8

/*** Macros for DRV_C1_ALM pin ***/
#define DRV_C1_ALM_Set()               (LATHSET = (1<<8))
#define DRV_C1_ALM_Clear()             (LATHCLR = (1<<8))
#define DRV_C1_ALM_Toggle()            (LATHINV= (1<<8))
#define DRV_C1_ALM_OutputEnable()      (TRISHCLR = (1<<8))
#define DRV_C1_ALM_InputEnable()       (TRISHSET = (1<<8))
#define DRV_C1_ALM_Get()               ((PORTH >> 8) & 0x1)
#define DRV_C1_ALM_PIN                  GPIO_PIN_RH8

/*** Macros for DRV_C2_DIR pin ***/
#define DRV_C2_DIR_Set()               (LATHSET = (1<<9))
#define DRV_C2_DIR_Clear()             (LATHCLR = (1<<9))
#define DRV_C2_DIR_Toggle()            (LATHINV= (1<<9))
#define DRV_C2_DIR_OutputEnable()      (TRISHCLR = (1<<9))
#define DRV_C2_DIR_InputEnable()       (TRISHSET = (1<<9))
#define DRV_C2_DIR_Get()               ((PORTH >> 9) & 0x1)
#define DRV_C2_DIR_PIN                  GPIO_PIN_RH9

/*** Macros for END_C1 pin ***/
#define END_C1_Set()               (LATHSET = (1<<10))
#define END_C1_Clear()             (LATHCLR = (1<<10))
#define END_C1_Toggle()            (LATHINV= (1<<10))
#define END_C1_OutputEnable()      (TRISHCLR = (1<<10))
#define END_C1_InputEnable()       (TRISHSET = (1<<10))
#define END_C1_Get()               ((PORTH >> 10) & 0x1)
#define END_C1_PIN                  GPIO_PIN_RH10

/*** Macros for END_C2 pin ***/
#define END_C2_Set()               (LATHSET = (1<<11))
#define END_C2_Clear()             (LATHCLR = (1<<11))
#define END_C2_Toggle()            (LATHINV= (1<<11))
#define END_C2_OutputEnable()      (TRISHCLR = (1<<11))
#define END_C2_InputEnable()       (TRISHSET = (1<<11))
#define END_C2_Get()               ((PORTH >> 11) & 0x1)
#define END_C2_PIN                  GPIO_PIN_RH11

/*** Macros for ENC_YR_Z pin ***/
#define ENC_YR_Z_Set()               (LATASET = (1<<2))
#define ENC_YR_Z_Clear()             (LATACLR = (1<<2))
#define ENC_YR_Z_Toggle()            (LATAINV= (1<<2))
#define ENC_YR_Z_OutputEnable()      (TRISACLR = (1<<2))
#define ENC_YR_Z_InputEnable()       (TRISASET = (1<<2))
#define ENC_YR_Z_Get()               ((PORTA >> 2) & 0x1)
#define ENC_YR_Z_PIN                  GPIO_PIN_RA2

/*** Macros for ENC_YR_B pin ***/
#define ENC_YR_B_Set()               (LATASET = (1<<3))
#define ENC_YR_B_Clear()             (LATACLR = (1<<3))
#define ENC_YR_B_Toggle()            (LATAINV= (1<<3))
#define ENC_YR_B_OutputEnable()      (TRISACLR = (1<<3))
#define ENC_YR_B_InputEnable()       (TRISASET = (1<<3))
#define ENC_YR_B_Get()               ((PORTA >> 3) & 0x1)
#define ENC_YR_B_PIN                  GPIO_PIN_RA3

/*** Macros for DRV_YR_ALM pin ***/
#define DRV_YR_ALM_Set()               (LATASET = (1<<4))
#define DRV_YR_ALM_Clear()             (LATACLR = (1<<4))
#define DRV_YR_ALM_Toggle()            (LATAINV= (1<<4))
#define DRV_YR_ALM_OutputEnable()      (TRISACLR = (1<<4))
#define DRV_YR_ALM_InputEnable()       (TRISASET = (1<<4))
#define DRV_YR_ALM_Get()               ((PORTA >> 4) & 0x1)
#define DRV_YR_ALM_PIN                  GPIO_PIN_RA4

/*** Macros for ENC_YR_A pin ***/
#define ENC_YR_A_Set()               (LATFSET = (1<<4))
#define ENC_YR_A_Clear()             (LATFCLR = (1<<4))
#define ENC_YR_A_Toggle()            (LATFINV= (1<<4))
#define ENC_YR_A_OutputEnable()      (TRISFCLR = (1<<4))
#define ENC_YR_A_InputEnable()       (TRISFSET = (1<<4))
#define ENC_YR_A_Get()               ((PORTF >> 4) & 0x1)
#define ENC_YR_A_PIN                  GPIO_PIN_RF4

/*** Macros for END_YL_MIN pin ***/
#define END_YL_MIN_Set()               (LATFSET = (1<<5))
#define END_YL_MIN_Clear()             (LATFCLR = (1<<5))
#define END_YL_MIN_Toggle()            (LATFINV= (1<<5))
#define END_YL_MIN_OutputEnable()      (TRISFCLR = (1<<5))
#define END_YL_MIN_InputEnable()       (TRISFSET = (1<<5))
#define END_YL_MIN_Get()               ((PORTF >> 5) & 0x1)
#define END_YL_MIN_PIN                  GPIO_PIN_RF5

/*** Macros for DRV_YR_PEN pin ***/
#define DRV_YR_PEN_Set()               (LATKSET = (1<<4))
#define DRV_YR_PEN_Clear()             (LATKCLR = (1<<4))
#define DRV_YR_PEN_Toggle()            (LATKINV= (1<<4))
#define DRV_YR_PEN_OutputEnable()      (TRISKCLR = (1<<4))
#define DRV_YR_PEN_InputEnable()       (TRISKSET = (1<<4))
#define DRV_YR_PEN_Get()               ((PORTK >> 4) & 0x1)
#define DRV_YR_PEN_PIN                  GPIO_PIN_RK4

/*** Macros for DRV_YL_ALM pin ***/
#define DRV_YL_ALM_Set()               (LATKSET = (1<<5))
#define DRV_YL_ALM_Clear()             (LATKCLR = (1<<5))
#define DRV_YL_ALM_Toggle()            (LATKINV= (1<<5))
#define DRV_YL_ALM_OutputEnable()      (TRISKCLR = (1<<5))
#define DRV_YL_ALM_InputEnable()       (TRISKSET = (1<<5))
#define DRV_YL_ALM_Get()               ((PORTK >> 5) & 0x1)
#define DRV_YL_ALM_PIN                  GPIO_PIN_RK5

/*** Macros for DRV_YL_PEN pin ***/
#define DRV_YL_PEN_Set()               (LATKSET = (1<<6))
#define DRV_YL_PEN_Clear()             (LATKCLR = (1<<6))
#define DRV_YL_PEN_Toggle()            (LATKINV= (1<<6))
#define DRV_YL_PEN_OutputEnable()      (TRISKCLR = (1<<6))
#define DRV_YL_PEN_InputEnable()       (TRISKSET = (1<<6))
#define DRV_YL_PEN_Get()               ((PORTK >> 6) & 0x1)
#define DRV_YL_PEN_PIN                  GPIO_PIN_RK6

/*** Macros for I2C_SCL pin ***/
#define I2C_SCL_Get()               ((PORTA >> 14) & 0x1)
#define I2C_SCL_PIN                  GPIO_PIN_RA14

/*** Macros for I2C_SDA pin ***/
#define I2C_SDA_Get()               ((PORTA >> 15) & 0x1)
#define I2C_SDA_PIN                  GPIO_PIN_RA15

/*** Macros for END_YL_MAX pin ***/
#define END_YL_MAX_Set()               (LATDSET = (1<<9))
#define END_YL_MAX_Clear()             (LATDCLR = (1<<9))
#define END_YL_MAX_Toggle()            (LATDINV= (1<<9))
#define END_YL_MAX_OutputEnable()      (TRISDCLR = (1<<9))
#define END_YL_MAX_InputEnable()       (TRISDSET = (1<<9))
#define END_YL_MAX_Get()               ((PORTD >> 9) & 0x1)
#define END_YL_MAX_PIN                  GPIO_PIN_RD9

/*** Macros for ENC_YL_Z pin ***/
#define ENC_YL_Z_Set()               (LATDSET = (1<<10))
#define ENC_YL_Z_Clear()             (LATDCLR = (1<<10))
#define ENC_YL_Z_Toggle()            (LATDINV= (1<<10))
#define ENC_YL_Z_OutputEnable()      (TRISDCLR = (1<<10))
#define ENC_YL_Z_InputEnable()       (TRISDSET = (1<<10))
#define ENC_YL_Z_Get()               ((PORTD >> 10) & 0x1)
#define ENC_YL_Z_PIN                  GPIO_PIN_RD10

/*** Macros for ENC_YL_B pin ***/
#define ENC_YL_B_Set()               (LATDSET = (1<<11))
#define ENC_YL_B_Clear()             (LATDCLR = (1<<11))
#define ENC_YL_B_Toggle()            (LATDINV= (1<<11))
#define ENC_YL_B_OutputEnable()      (TRISDCLR = (1<<11))
#define ENC_YL_B_InputEnable()       (TRISDSET = (1<<11))
#define ENC_YL_B_Get()               ((PORTD >> 11) & 0x1)
#define ENC_YL_B_PIN                  GPIO_PIN_RD11

/*** Macros for DRV_XYZ_EN pin ***/
#define DRV_XYZ_EN_Set()               (LATHSET = (1<<12))
#define DRV_XYZ_EN_Clear()             (LATHCLR = (1<<12))
#define DRV_XYZ_EN_Toggle()            (LATHINV= (1<<12))
#define DRV_XYZ_EN_OutputEnable()      (TRISHCLR = (1<<12))
#define DRV_XYZ_EN_InputEnable()       (TRISHSET = (1<<12))
#define DRV_XYZ_EN_Get()               ((PORTH >> 12) & 0x1)
#define DRV_XYZ_EN_PIN                  GPIO_PIN_RH12

/*** Macros for DRV_Y_DIR pin ***/
#define DRV_Y_DIR_Set()               (LATHSET = (1<<13))
#define DRV_Y_DIR_Clear()             (LATHCLR = (1<<13))
#define DRV_Y_DIR_Toggle()            (LATHINV= (1<<13))
#define DRV_Y_DIR_OutputEnable()      (TRISHCLR = (1<<13))
#define DRV_Y_DIR_InputEnable()       (TRISHSET = (1<<13))
#define DRV_Y_DIR_Get()               ((PORTH >> 13) & 0x1)
#define DRV_Y_DIR_PIN                  GPIO_PIN_RH13

/*** Macros for DRV_X_ALM pin ***/
#define DRV_X_ALM_Set()               (LATHSET = (1<<14))
#define DRV_X_ALM_Clear()             (LATHCLR = (1<<14))
#define DRV_X_ALM_Toggle()            (LATHINV= (1<<14))
#define DRV_X_ALM_OutputEnable()      (TRISHCLR = (1<<14))
#define DRV_X_ALM_InputEnable()       (TRISHSET = (1<<14))
#define DRV_X_ALM_Get()               ((PORTH >> 14) & 0x1)
#define DRV_X_ALM_PIN                  GPIO_PIN_RH14

/*** Macros for DRV_X_PEN pin ***/
#define DRV_X_PEN_Set()               (LATHSET = (1<<15))
#define DRV_X_PEN_Clear()             (LATHCLR = (1<<15))
#define DRV_X_PEN_Toggle()            (LATHINV= (1<<15))
#define DRV_X_PEN_OutputEnable()      (TRISHCLR = (1<<15))
#define DRV_X_PEN_InputEnable()       (TRISHSET = (1<<15))
#define DRV_X_PEN_Get()               ((PORTH >> 15) & 0x1)
#define DRV_X_PEN_PIN                  GPIO_PIN_RH15

/*** Macros for SIG_E_STOP pin ***/
#define SIG_E_STOP_Set()               (LATDSET = (1<<0))
#define SIG_E_STOP_Clear()             (LATDCLR = (1<<0))
#define SIG_E_STOP_Toggle()            (LATDINV= (1<<0))
#define SIG_E_STOP_OutputEnable()      (TRISDCLR = (1<<0))
#define SIG_E_STOP_InputEnable()       (TRISDSET = (1<<0))
#define SIG_E_STOP_Get()               ((PORTD >> 0) & 0x1)
#define SIG_E_STOP_PIN                  GPIO_PIN_RD0

/*** Macros for CLK_32768HZ pin ***/
#define CLK_32768HZ_Get()               ((PORTC >> 13) & 0x1)
#define CLK_32768HZ_PIN                  GPIO_PIN_RC13

/*** Macros for ENC_YL_A pin ***/
#define ENC_YL_A_Set()               (LATCSET = (1<<14))
#define ENC_YL_A_Clear()             (LATCCLR = (1<<14))
#define ENC_YL_A_Toggle()            (LATCINV= (1<<14))
#define ENC_YL_A_OutputEnable()      (TRISCCLR = (1<<14))
#define ENC_YL_A_InputEnable()       (TRISCSET = (1<<14))
#define ENC_YL_A_Get()               ((PORTC >> 14) & 0x1)
#define ENC_YL_A_PIN                  GPIO_PIN_RC14

/*** Macros for END_X_MIN pin ***/
#define END_X_MIN_Set()               (LATDSET = (1<<1))
#define END_X_MIN_Clear()             (LATDCLR = (1<<1))
#define END_X_MIN_Toggle()            (LATDINV= (1<<1))
#define END_X_MIN_OutputEnable()      (TRISDCLR = (1<<1))
#define END_X_MIN_InputEnable()       (TRISDSET = (1<<1))
#define END_X_MIN_Get()               ((PORTD >> 1) & 0x1)
#define END_X_MIN_PIN                  GPIO_PIN_RD1

/*** Macros for END_X_MAX pin ***/
#define END_X_MAX_Set()               (LATDSET = (1<<2))
#define END_X_MAX_Clear()             (LATDCLR = (1<<2))
#define END_X_MAX_Toggle()            (LATDINV= (1<<2))
#define END_X_MAX_OutputEnable()      (TRISDCLR = (1<<2))
#define END_X_MAX_InputEnable()       (TRISDSET = (1<<2))
#define END_X_MAX_Get()               ((PORTD >> 2) & 0x1)
#define END_X_MAX_PIN                  GPIO_PIN_RD2

/*** Macros for ENC_X_Z pin ***/
#define ENC_X_Z_Set()               (LATDSET = (1<<3))
#define ENC_X_Z_Clear()             (LATDCLR = (1<<3))
#define ENC_X_Z_Toggle()            (LATDINV= (1<<3))
#define ENC_X_Z_OutputEnable()      (TRISDCLR = (1<<3))
#define ENC_X_Z_InputEnable()       (TRISDSET = (1<<3))
#define ENC_X_Z_Get()               ((PORTD >> 3) & 0x1)
#define ENC_X_Z_PIN                  GPIO_PIN_RD3

/*** Macros for ENC_X_A pin ***/
#define ENC_X_A_Set()               (LATDSET = (1<<12))
#define ENC_X_A_Clear()             (LATDCLR = (1<<12))
#define ENC_X_A_Toggle()            (LATDINV= (1<<12))
#define ENC_X_A_OutputEnable()      (TRISDCLR = (1<<12))
#define ENC_X_A_InputEnable()       (TRISDSET = (1<<12))
#define ENC_X_A_Get()               ((PORTD >> 12) & 0x1)
#define ENC_X_A_PIN                  GPIO_PIN_RD12

/*** Macros for DRV_X_DIR pin ***/
#define DRV_X_DIR_Set()               (LATDSET = (1<<13))
#define DRV_X_DIR_Clear()             (LATDCLR = (1<<13))
#define DRV_X_DIR_Toggle()            (LATDINV= (1<<13))
#define DRV_X_DIR_OutputEnable()      (TRISDCLR = (1<<13))
#define DRV_X_DIR_InputEnable()       (TRISDSET = (1<<13))
#define DRV_X_DIR_Get()               ((PORTD >> 13) & 0x1)
#define DRV_X_DIR_PIN                  GPIO_PIN_RD13

/*** Macros for DRV_YR_STP pin ***/
#define DRV_YR_STP_Set()               (LATJSET = (1<<0))
#define DRV_YR_STP_Clear()             (LATJCLR = (1<<0))
#define DRV_YR_STP_Toggle()            (LATJINV= (1<<0))
#define DRV_YR_STP_OutputEnable()      (TRISJCLR = (1<<0))
#define DRV_YR_STP_InputEnable()       (TRISJSET = (1<<0))
#define DRV_YR_STP_Get()               ((PORTJ >> 0) & 0x1)
#define DRV_YR_STP_PIN                  GPIO_PIN_RJ0

/*** Macros for DRV_YL_STP pin ***/
#define DRV_YL_STP_Set()               (LATJSET = (1<<1))
#define DRV_YL_STP_Clear()             (LATJCLR = (1<<1))
#define DRV_YL_STP_Toggle()            (LATJINV= (1<<1))
#define DRV_YL_STP_OutputEnable()      (TRISJCLR = (1<<1))
#define DRV_YL_STP_InputEnable()       (TRISJSET = (1<<1))
#define DRV_YL_STP_Get()               ((PORTJ >> 1) & 0x1)
#define DRV_YL_STP_PIN                  GPIO_PIN_RJ1

/*** Macros for DRV_X_STP pin ***/
#define DRV_X_STP_Set()               (LATJSET = (1<<2))
#define DRV_X_STP_Clear()             (LATJCLR = (1<<2))
#define DRV_X_STP_Toggle()            (LATJINV= (1<<2))
#define DRV_X_STP_OutputEnable()      (TRISJCLR = (1<<2))
#define DRV_X_STP_InputEnable()       (TRISJSET = (1<<2))
#define DRV_X_STP_Get()               ((PORTJ >> 2) & 0x1)
#define DRV_X_STP_PIN                  GPIO_PIN_RJ2

/*** Macros for CAN1_STBY pin ***/
#define CAN1_STBY_Set()               (LATJSET = (1<<3))
#define CAN1_STBY_Clear()             (LATJCLR = (1<<3))
#define CAN1_STBY_Toggle()            (LATJINV= (1<<3))
#define CAN1_STBY_OutputEnable()      (TRISJCLR = (1<<3))
#define CAN1_STBY_InputEnable()       (TRISJSET = (1<<3))
#define CAN1_STBY_Get()               ((PORTJ >> 3) & 0x1)
#define CAN1_STBY_PIN                  GPIO_PIN_RJ3

/*** Macros for ESP_TX pin ***/
#define ESP_TX_Get()               ((PORTD >> 4) & 0x1)
#define ESP_TX_PIN                  GPIO_PIN_RD4

/*** Macros for ESP_RX pin ***/
#define ESP_RX_Get()               ((PORTD >> 5) & 0x1)
#define ESP_RX_PIN                  GPIO_PIN_RD5

/*** Macros for __fejl__CAN1_RX pin ***/
#define __fejl__CAN1_RX_Get()               ((PORTD >> 6) & 0x1)
#define __fejl__CAN1_RX_PIN                  GPIO_PIN_RD6

/*** Macros for ENC_X_B pin ***/
#define ENC_X_B_Set()               (LATDSET = (1<<7))
#define ENC_X_B_Clear()             (LATDCLR = (1<<7))
#define ENC_X_B_Toggle()            (LATDINV= (1<<7))
#define ENC_X_B_OutputEnable()      (TRISDCLR = (1<<7))
#define ENC_X_B_InputEnable()       (TRISDSET = (1<<7))
#define ENC_X_B_Get()               ((PORTD >> 7) & 0x1)
#define ENC_X_B_PIN                  GPIO_PIN_RD7

/*** Macros for __fejl__CAN1_TX pin ***/
#define __fejl__CAN1_TX_Get()               ((PORTF >> 0) & 0x1)
#define __fejl__CAN1_TX_PIN                  GPIO_PIN_RF0

/*** Macros for FDR_PWR_FIX pin ***/
#define FDR_PWR_FIX_Set()               (LATFSET = (1<<1))
#define FDR_PWR_FIX_Clear()             (LATFCLR = (1<<1))
#define FDR_PWR_FIX_Toggle()            (LATFINV= (1<<1))
#define FDR_PWR_FIX_OutputEnable()      (TRISFCLR = (1<<1))
#define FDR_PWR_FIX_InputEnable()       (TRISFSET = (1<<1))
#define FDR_PWR_FIX_Get()               ((PORTF >> 1) & 0x1)
#define FDR_PWR_FIX_PIN                  GPIO_PIN_RF1

/*** Macros for DRV_B2_DIR pin ***/
#define DRV_B2_DIR_Set()               (LATKSET = (1<<7))
#define DRV_B2_DIR_Clear()             (LATKCLR = (1<<7))
#define DRV_B2_DIR_Toggle()            (LATKINV= (1<<7))
#define DRV_B2_DIR_OutputEnable()      (TRISKCLR = (1<<7))
#define DRV_B2_DIR_InputEnable()       (TRISKSET = (1<<7))
#define DRV_B2_DIR_Get()               ((PORTK >> 7) & 0x1)
#define DRV_B2_DIR_PIN                  GPIO_PIN_RK7

/*** Macros for GPIO_RG1 pin ***/
#define GPIO_RG1_Set()               (LATGSET = (1<<1))
#define GPIO_RG1_Clear()             (LATGCLR = (1<<1))
#define GPIO_RG1_Toggle()            (LATGINV= (1<<1))
#define GPIO_RG1_OutputEnable()      (TRISGCLR = (1<<1))
#define GPIO_RG1_InputEnable()       (TRISGSET = (1<<1))
#define GPIO_RG1_Get()               ((PORTG >> 1) & 0x1)
#define GPIO_RG1_PIN                  GPIO_PIN_RG1

/*** Macros for SIG_BUZZ pin ***/
#define SIG_BUZZ_Set()               (LATASET = (1<<6))
#define SIG_BUZZ_Clear()             (LATACLR = (1<<6))
#define SIG_BUZZ_Toggle()            (LATAINV= (1<<6))
#define SIG_BUZZ_OutputEnable()      (TRISACLR = (1<<6))
#define SIG_BUZZ_InputEnable()       (TRISASET = (1<<6))
#define SIG_BUZZ_Get()               ((PORTA >> 6) & 0x1)
#define SIG_BUZZ_PIN                  GPIO_PIN_RA6

/*** Macros for SIG_LED_R pin ***/
#define SIG_LED_R_Set()               (LATASET = (1<<7))
#define SIG_LED_R_Clear()             (LATACLR = (1<<7))
#define SIG_LED_R_Toggle()            (LATAINV= (1<<7))
#define SIG_LED_R_OutputEnable()      (TRISACLR = (1<<7))
#define SIG_LED_R_InputEnable()       (TRISASET = (1<<7))
#define SIG_LED_R_Get()               ((PORTA >> 7) & 0x1)
#define SIG_LED_R_PIN                  GPIO_PIN_RA7

/*** Macros for SIG_LED_G pin ***/
#define SIG_LED_G_Set()               (LATJSET = (1<<4))
#define SIG_LED_G_Clear()             (LATJCLR = (1<<4))
#define SIG_LED_G_Toggle()            (LATJINV= (1<<4))
#define SIG_LED_G_OutputEnable()      (TRISJCLR = (1<<4))
#define SIG_LED_G_InputEnable()       (TRISJSET = (1<<4))
#define SIG_LED_G_Get()               ((PORTJ >> 4) & 0x1)
#define SIG_LED_G_PIN                  GPIO_PIN_RJ4

/*** Macros for SIG_LED_B pin ***/
#define SIG_LED_B_Set()               (LATJSET = (1<<5))
#define SIG_LED_B_Clear()             (LATJCLR = (1<<5))
#define SIG_LED_B_Toggle()            (LATJINV= (1<<5))
#define SIG_LED_B_OutputEnable()      (TRISJCLR = (1<<5))
#define SIG_LED_B_InputEnable()       (TRISJSET = (1<<5))
#define SIG_LED_B_Get()               ((PORTJ >> 5) & 0x1)
#define SIG_LED_B_PIN                  GPIO_PIN_RJ5

/*** Macros for DRV_A1_STP pin ***/
#define DRV_A1_STP_Set()               (LATJSET = (1<<6))
#define DRV_A1_STP_Clear()             (LATJCLR = (1<<6))
#define DRV_A1_STP_Toggle()            (LATJINV= (1<<6))
#define DRV_A1_STP_OutputEnable()      (TRISJCLR = (1<<6))
#define DRV_A1_STP_InputEnable()       (TRISJSET = (1<<6))
#define DRV_A1_STP_Get()               ((PORTJ >> 6) & 0x1)
#define DRV_A1_STP_PIN                  GPIO_PIN_RJ6

/*** Macros for DRV_A2_STP pin ***/
#define DRV_A2_STP_Set()               (LATJSET = (1<<7))
#define DRV_A2_STP_Clear()             (LATJCLR = (1<<7))
#define DRV_A2_STP_Toggle()            (LATJINV= (1<<7))
#define DRV_A2_STP_OutputEnable()      (TRISJCLR = (1<<7))
#define DRV_A2_STP_InputEnable()       (TRISJSET = (1<<7))
#define DRV_A2_STP_Get()               ((PORTJ >> 7) & 0x1)
#define DRV_A2_STP_PIN                  GPIO_PIN_RJ7

/*** Macros for END_A2 pin ***/
#define END_A2_Set()               (LATESET = (1<<0))
#define END_A2_Clear()             (LATECLR = (1<<0))
#define END_A2_Toggle()            (LATEINV= (1<<0))
#define END_A2_OutputEnable()      (TRISECLR = (1<<0))
#define END_A2_InputEnable()       (TRISESET = (1<<0))
#define END_A2_Get()               ((PORTE >> 0) & 0x1)
#define END_A2_PIN                  GPIO_PIN_RE0

/*** Macros for END_A1 pin ***/
#define END_A1_Set()               (LATESET = (1<<1))
#define END_A1_Clear()             (LATECLR = (1<<1))
#define END_A1_Toggle()            (LATEINV= (1<<1))
#define END_A1_OutputEnable()      (TRISECLR = (1<<1))
#define END_A1_InputEnable()       (TRISESET = (1<<1))
#define END_A1_Get()               ((PORTE >> 1) & 0x1)
#define END_A1_PIN                  GPIO_PIN_RE1

/*** Macros for DRV_A1_DIR pin ***/
#define DRV_A1_DIR_Set()               (LATGSET = (1<<14))
#define DRV_A1_DIR_Clear()             (LATGCLR = (1<<14))
#define DRV_A1_DIR_Toggle()            (LATGINV= (1<<14))
#define DRV_A1_DIR_OutputEnable()      (TRISGCLR = (1<<14))
#define DRV_A1_DIR_InputEnable()       (TRISGSET = (1<<14))
#define DRV_A1_DIR_Get()               ((PORTG >> 14) & 0x1)
#define DRV_A1_DIR_PIN                  GPIO_PIN_RG14

/*** Macros for DRV_A_EN pin ***/
#define DRV_A_EN_Set()               (LATGSET = (1<<12))
#define DRV_A_EN_Clear()             (LATGCLR = (1<<12))
#define DRV_A_EN_Toggle()            (LATGINV= (1<<12))
#define DRV_A_EN_OutputEnable()      (TRISGCLR = (1<<12))
#define DRV_A_EN_InputEnable()       (TRISGSET = (1<<12))
#define DRV_A_EN_Get()               ((PORTG >> 12) & 0x1)
#define DRV_A_EN_PIN                  GPIO_PIN_RG12

/*** Macros for DRV_A2_DIR pin ***/
#define DRV_A2_DIR_Set()               (LATGSET = (1<<13))
#define DRV_A2_DIR_Clear()             (LATGCLR = (1<<13))
#define DRV_A2_DIR_Toggle()            (LATGINV= (1<<13))
#define DRV_A2_DIR_OutputEnable()      (TRISGCLR = (1<<13))
#define DRV_A2_DIR_InputEnable()       (TRISGSET = (1<<13))
#define DRV_A2_DIR_Get()               ((PORTG >> 13) & 0x1)
#define DRV_A2_DIR_PIN                  GPIO_PIN_RG13

/*** Macros for DRV_A1_ALM pin ***/
#define DRV_A1_ALM_Set()               (LATESET = (1<<2))
#define DRV_A1_ALM_Clear()             (LATECLR = (1<<2))
#define DRV_A1_ALM_Toggle()            (LATEINV= (1<<2))
#define DRV_A1_ALM_OutputEnable()      (TRISECLR = (1<<2))
#define DRV_A1_ALM_InputEnable()       (TRISESET = (1<<2))
#define DRV_A1_ALM_Get()               ((PORTE >> 2) & 0x1)
#define DRV_A1_ALM_PIN                  GPIO_PIN_RE2

/*** Macros for __FIX__UART1_TX pin ***/
#define __FIX__UART1_TX_Get()               ((PORTE >> 3) & 0x1)
#define __FIX__UART1_TX_PIN                  GPIO_PIN_RE3

/*** Macros for DRV_A2_ALM pin ***/
#define DRV_A2_ALM_Set()               (LATESET = (1<<4))
#define DRV_A2_ALM_Clear()             (LATECLR = (1<<4))
#define DRV_A2_ALM_Toggle()            (LATEINV= (1<<4))
#define DRV_A2_ALM_OutputEnable()      (TRISECLR = (1<<4))
#define DRV_A2_ALM_InputEnable()       (TRISESET = (1<<4))
#define DRV_A2_ALM_Get()               ((PORTE >> 4) & 0x1)
#define DRV_A2_ALM_PIN                  GPIO_PIN_RE4


// *****************************************************************************
/* GPIO Port

  Summary:
    Identifies the available GPIO Ports.

  Description:
    This enumeration identifies the available GPIO Ports.

  Remarks:
    The caller should not rely on the specific numbers assigned to any of
    these values as they may change from one processor to the next.

    Not all ports are available on all devices.  Refer to the specific
    device data sheet to determine which ports are supported.
*/

typedef enum
{
    GPIO_PORT_A = 0,
    GPIO_PORT_B = 1,
    GPIO_PORT_C = 2,
    GPIO_PORT_D = 3,
    GPIO_PORT_E = 4,
    GPIO_PORT_F = 5,
    GPIO_PORT_G = 6,
    GPIO_PORT_H = 7,
    GPIO_PORT_J = 8,
    GPIO_PORT_K = 9,
} GPIO_PORT;

// *****************************************************************************
/* GPIO Port Pins

  Summary:
    Identifies the available GPIO port pins.

  Description:
    This enumeration identifies the available GPIO port pins.

  Remarks:
    The caller should not rely on the specific numbers assigned to any of
    these values as they may change from one processor to the next.

    Not all pins are available on all devices.  Refer to the specific
    device data sheet to determine which pins are supported.
*/

typedef enum
{
    GPIO_PIN_RA0 = 0,
    GPIO_PIN_RA1 = 1,
    GPIO_PIN_RA2 = 2,
    GPIO_PIN_RA3 = 3,
    GPIO_PIN_RA4 = 4,
    GPIO_PIN_RA5 = 5,
    GPIO_PIN_RA6 = 6,
    GPIO_PIN_RA7 = 7,
    GPIO_PIN_RA9 = 9,
    GPIO_PIN_RA10 = 10,
    GPIO_PIN_RA14 = 14,
    GPIO_PIN_RA15 = 15,
    GPIO_PIN_RB0 = 16,
    GPIO_PIN_RB1 = 17,
    GPIO_PIN_RB2 = 18,
    GPIO_PIN_RB3 = 19,
    GPIO_PIN_RB4 = 20,
    GPIO_PIN_RB5 = 21,
    GPIO_PIN_RB6 = 22,
    GPIO_PIN_RB7 = 23,
    GPIO_PIN_RB8 = 24,
    GPIO_PIN_RB9 = 25,
    GPIO_PIN_RB10 = 26,
    GPIO_PIN_RB11 = 27,
    GPIO_PIN_RB12 = 28,
    GPIO_PIN_RB13 = 29,
    GPIO_PIN_RB14 = 30,
    GPIO_PIN_RB15 = 31,
    GPIO_PIN_RC1 = 33,
    GPIO_PIN_RC2 = 34,
    GPIO_PIN_RC3 = 35,
    GPIO_PIN_RC4 = 36,
    GPIO_PIN_RC12 = 44,
    GPIO_PIN_RC13 = 45,
    GPIO_PIN_RC14 = 46,
    GPIO_PIN_RC15 = 47,
    GPIO_PIN_RD0 = 48,
    GPIO_PIN_RD1 = 49,
    GPIO_PIN_RD2 = 50,
    GPIO_PIN_RD3 = 51,
    GPIO_PIN_RD4 = 52,
    GPIO_PIN_RD5 = 53,
    GPIO_PIN_RD6 = 54,
    GPIO_PIN_RD7 = 55,
    GPIO_PIN_RD9 = 57,
    GPIO_PIN_RD10 = 58,
    GPIO_PIN_RD11 = 59,
    GPIO_PIN_RD12 = 60,
    GPIO_PIN_RD13 = 61,
    GPIO_PIN_RD14 = 62,
    GPIO_PIN_RD15 = 63,
    GPIO_PIN_RE0 = 64,
    GPIO_PIN_RE1 = 65,
    GPIO_PIN_RE2 = 66,
    GPIO_PIN_RE3 = 67,
    GPIO_PIN_RE4 = 68,
    GPIO_PIN_RE5 = 69,
    GPIO_PIN_RE6 = 70,
    GPIO_PIN_RE7 = 71,
    GPIO_PIN_RE8 = 72,
    GPIO_PIN_RE9 = 73,
    GPIO_PIN_RF0 = 80,
    GPIO_PIN_RF1 = 81,
    GPIO_PIN_RF2 = 82,
    GPIO_PIN_RF3 = 83,
    GPIO_PIN_RF4 = 84,
    GPIO_PIN_RF5 = 85,
    GPIO_PIN_RF8 = 88,
    GPIO_PIN_RF12 = 92,
    GPIO_PIN_RF13 = 93,
    GPIO_PIN_RG0 = 96,
    GPIO_PIN_RG1 = 97,
    GPIO_PIN_RG6 = 102,
    GPIO_PIN_RG7 = 103,
    GPIO_PIN_RG8 = 104,
    GPIO_PIN_RG9 = 105,
    GPIO_PIN_RG12 = 108,
    GPIO_PIN_RG13 = 109,
    GPIO_PIN_RG14 = 110,
    GPIO_PIN_RG15 = 111,
    GPIO_PIN_RH0 = 112,
    GPIO_PIN_RH1 = 113,
    GPIO_PIN_RH2 = 114,
    GPIO_PIN_RH3 = 115,
    GPIO_PIN_RH4 = 116,
    GPIO_PIN_RH5 = 117,
    GPIO_PIN_RH6 = 118,
    GPIO_PIN_RH7 = 119,
    GPIO_PIN_RH8 = 120,
    GPIO_PIN_RH9 = 121,
    GPIO_PIN_RH10 = 122,
    GPIO_PIN_RH11 = 123,
    GPIO_PIN_RH12 = 124,
    GPIO_PIN_RH13 = 125,
    GPIO_PIN_RH14 = 126,
    GPIO_PIN_RH15 = 127,
    GPIO_PIN_RJ0 = 128,
    GPIO_PIN_RJ1 = 129,
    GPIO_PIN_RJ2 = 130,
    GPIO_PIN_RJ3 = 131,
    GPIO_PIN_RJ4 = 132,
    GPIO_PIN_RJ5 = 133,
    GPIO_PIN_RJ6 = 134,
    GPIO_PIN_RJ7 = 135,
    GPIO_PIN_RJ8 = 136,
    GPIO_PIN_RJ9 = 137,
    GPIO_PIN_RJ10 = 138,
    GPIO_PIN_RJ11 = 139,
    GPIO_PIN_RJ12 = 140,
    GPIO_PIN_RJ13 = 141,
    GPIO_PIN_RJ14 = 142,
    GPIO_PIN_RJ15 = 143,
    GPIO_PIN_RK0 = 144,
    GPIO_PIN_RK1 = 145,
    GPIO_PIN_RK2 = 146,
    GPIO_PIN_RK3 = 147,
    GPIO_PIN_RK4 = 148,
    GPIO_PIN_RK5 = 149,
    GPIO_PIN_RK6 = 150,
    GPIO_PIN_RK7 = 151,

    /* This element should not be used in any of the GPIO APIs.
       It will be used by other modules or application to denote that none of the GPIO Pin is used */
    GPIO_PIN_NONE = -1

} GPIO_PIN;


void GPIO_Initialize(void);

// *****************************************************************************
// *****************************************************************************
// Section: GPIO Functions which operates on multiple pins of a port
// *****************************************************************************
// *****************************************************************************

uint32_t GPIO_PortRead(GPIO_PORT port);

void GPIO_PortWrite(GPIO_PORT port, uint32_t mask, uint32_t value);

uint32_t GPIO_PortLatchRead ( GPIO_PORT port );

void GPIO_PortSet(GPIO_PORT port, uint32_t mask);

void GPIO_PortClear(GPIO_PORT port, uint32_t mask);

void GPIO_PortToggle(GPIO_PORT port, uint32_t mask);

void GPIO_PortInputEnable(GPIO_PORT port, uint32_t mask);

void GPIO_PortOutputEnable(GPIO_PORT port, uint32_t mask);

// *****************************************************************************
// *****************************************************************************
// Section: GPIO Functions which operates on one pin at a time
// *****************************************************************************
// *****************************************************************************

static inline void GPIO_PinWrite(GPIO_PIN pin, bool value)
{
    GPIO_PortWrite((GPIO_PORT)(pin>>4), (uint32_t)(0x1) << (pin & 0xF), (uint32_t)(value) << (pin & 0xF));
}

static inline bool GPIO_PinRead(GPIO_PIN pin)
{
    return (bool)(((GPIO_PortRead((GPIO_PORT)(pin>>4))) >> (pin & 0xF)) & 0x1);
}

static inline bool GPIO_PinLatchRead(GPIO_PIN pin)
{
    return (bool)((GPIO_PortLatchRead((GPIO_PORT)(pin>>4)) >> (pin & 0xF)) & 0x1);
}

static inline void GPIO_PinToggle(GPIO_PIN pin)
{
    GPIO_PortToggle((GPIO_PORT)(pin>>4), 0x1 << (pin & 0xF));
}

static inline void GPIO_PinSet(GPIO_PIN pin)
{
    GPIO_PortSet((GPIO_PORT)(pin>>4), 0x1 << (pin & 0xF));
}

static inline void GPIO_PinClear(GPIO_PIN pin)
{
    GPIO_PortClear((GPIO_PORT)(pin>>4), 0x1 << (pin & 0xF));
}

static inline void GPIO_PinInputEnable(GPIO_PIN pin)
{
    GPIO_PortInputEnable((GPIO_PORT)(pin>>4), 0x1 << (pin & 0xF));
}

static inline void GPIO_PinOutputEnable(GPIO_PIN pin)
{
    GPIO_PortOutputEnable((GPIO_PORT)(pin>>4), 0x1 << (pin & 0xF));
}


// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

    }

#endif
// DOM-IGNORE-END
#endif // PLIB_GPIO_H
