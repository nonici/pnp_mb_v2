/* ************************************************************************** */
/*
  @File Name
    psu.h
 
  @Author
    Flitch

  @Summary
    Power on / power off control of power supply.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef PSU_H    /* Guard against multiple inclusion */
#define PSU_H

// *****************************************************************************

#include "definitions.h"

// *****************************************************************************

void PowerSupplyOff(void);
void PowerSupplyOn(void);

// *****************************************************************************

#endif /* PSU_H */

/* *****************************************************************************
 End of File
 */
