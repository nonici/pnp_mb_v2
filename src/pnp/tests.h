/* ************************************************************************** */
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef TESTS_H    /* Guard against multiple inclusion */
#define TESTS_H

// *****************************************************************************

#include "definitions.h"
#include "ms_timer.h"
#include "signalers.h"

#include "../comm/comm.h"

// *****************************************************************************


// *****************************************************************************

void TestsInitialize(void);

void TestsHandler(void);

void CanDump(void);

#endif /* TESTS_H */

/* *****************************************************************************
 End of File
 */
