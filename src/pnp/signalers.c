// *****************************************************************************
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "signalers.h"
#include "psu.h"
#include "motion/movement.h"
#include "comm/gcode.h"

// *****************************************************************************

void SignalBuzzShort(void) {
    SignalBuzz(SETTINGS_SIG_BUZZ_TIME_SHORT);
}

void SignalBuzzLong(void) {
    SignalBuzz(SETTINGS_SIG_BUZZ_TIME_LONG);
}

void SignalBuzz(uint32_t time) {
    SIG_BUZZ_Set();
    CORETIMER_DelayMs(time);
    SIG_BUZZ_Clear();
}

void SignalLedToggle(ledColor led) {
    if (led == ledRed) {
        SIG_LED_R_Toggle();
    }
    if (led == ledGreen) {
        SIG_LED_G_Toggle();
    }
    if (led == ledBlue) {
        SIG_LED_B_Toggle();
    }
    if (led == ledAll) {
        SIG_LED_R_Toggle();
        SIG_LED_G_Toggle();
        SIG_LED_B_Toggle();
    }
}

void SignalLedOn(ledColor led) {
    if (led == ledRed) {
        SIG_LED_R_Set();
    }
    if (led == ledGreen) {
        SIG_LED_G_Set();
    }
    if (led == ledBlue) {
        SIG_LED_B_Set();
    }
    if (led == ledAll) {
        SIG_LED_R_Set();
        SIG_LED_G_Set();
        SIG_LED_B_Set();
    }
}

void SignalLedOff(ledColor led) {
    if (led == ledRed) {
        SIG_LED_R_Clear();
    }
    if (led == ledGreen) {
        SIG_LED_G_Clear();
    }
    if (led == ledBlue) {
        SIG_LED_B_Clear();
    }
    if (led == ledAll) {
        SIG_LED_R_Clear();
        SIG_LED_G_Clear();
        SIG_LED_B_Clear();
    }
}



void SignalLedFlashShort(ledColor led) {
    SignalLedFlash(led, SETTINGS_SIG_LED_TIME_SHORT);
}

void SignalLedFlashLong(ledColor led) {
    SignalLedFlash(led, SETTINGS_SIG_LED_TIME_LONG);
}

void SignalLedFlash(ledColor led, uint32_t time) {
    SignalLedOn(led);
    CORETIMER_DelayMs(time);
    SignalLedOff(led);
}

void SignalLedBlinkSlow(ledColor led) {
    //Permanently blink led slowly until stopped
}

void SignalLedBlinkFast(ledColor led) {
    //Permanently blink led fast until stopped
}

void SignalLedBlinkStop(ledColor led) {
    //Stop blining leds
}

bool EmergencyStopUpdateAndGet(void) {
    static bool eStopPreviousState;
    static bool notFirstTime;
    bool eStop = SIG_E_STOP_Get();
    //Execute only if state has changed from last entry
    //PSU will not turn on together with machine if Estop is pressed on power up time
    //Suitable for late night and early morning hours
    if ((eStop != eStopPreviousState || (!notFirstTime))) {
        //remember last state
        eStopPreviousState = eStop;
        if (eStop) {
            SignalLedOn(ledRed);
            SignalLedOff(ledGreen);
            DriverDisableXY();
            GcodeRegexErrorEstop();
        } else {
            SignalLedOff(ledRed);
            SignalLedOn(ledGreen);
            PowerSupplyOn();
        }
    }
    notFirstTime = true;
    return (eStop);
}
/* *****************************************************************************
 End of File
 */
