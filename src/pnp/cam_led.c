// *****************************************************************************
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "cam_led.h"

// *****************************************************************************

typedef GPIO_PIN camera_pin_t;

camera_pin_t cameraPin[2];

// *****************************************************************************

void CameraLedInitialization(void) {
    cameraPin [BaseCam] = FET_CRTL_20_PIN;
    cameraPin [HeadCam] = FET_CRTL_19_PIN;
    CameraLedOn(HeadCam);
    CameraLedOn(BaseCam);
}

void CameraLedOn(camera_t camera) {
    GPIO_PinWrite(cameraPin[camera], CAMERA_LED_ON);
}

void CameraLedOff(camera_t camera) {
    GPIO_PinWrite(cameraPin[camera], CAMERA_LED_OFF);
}
/* *****************************************************************************
 End of File
 */
