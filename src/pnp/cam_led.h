/* ************************************************************************** */
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef CAM_LED_H    /* Guard against multiple inclusion */
#define CAM_LED_H

// *****************************************************************************

#include "definitions.h"
#include "enums.h"

// *****************************************************************************

#define     CAMERA_LED_ON       1
#define     CAMERA_LED_OFF      0

// *****************************************************************************


// *****************************************************************************

void CameraLedInitialization(void);

void CameraLedOn(camera_t camera);

void CameraLedOff(camera_t camera);

#endif /* CAM_LED_H */

/* *****************************************************************************
 End of File
 */
