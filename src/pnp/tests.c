// *****************************************************************************
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "tests.h"

// *****************************************************************************

ms_timer_t timerLedOn = {0};
ms_timer_t timerLedOff = {0};
ms_timer_t timerPositionReport = {0};

// *****************************************************************************

void TestsInitialize(void) {

}

void TestsHandler(void) {

    if (!MsTimerIsTimerActive(&timerLedOff)) {
        MsTimerDelayMs(&timerLedOn, 2500);
        MsTimerDelayMs(&timerLedOff, 5000);
        SignalLedOff(ledBlue);
    }
    if (!MsTimerIsTimerActive(&timerLedOn)) {
        SignalLedOn(ledBlue);
    }

    if (!MsTimerIsTimerActive(&timerPositionReport)) {
        MsTimerDelayMs(&timerPositionReport, 60000);
        GcodeRegexPositionReport();
    }


}

/* *****************************************************************************
 End of File
 */
