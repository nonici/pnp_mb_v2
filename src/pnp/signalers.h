/* ************************************************************************** */
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */


#ifndef SIGNALERS_H    /* Guard against multiple inclusion */
#define SIGNALERS_H

// *****************************************************************************

#include "definitions.h"
#include "enums.h"

// *****************************************************************************

#define SETTINGS_SIG_BUZZ_TIME_LONG 500U
#define SETTINGS_SIG_BUZZ_TIME_SHORT 25U

#define SETTINGS_SIG_LED_TIME_SHORT 250U
#define SETTINGS_SIG_LED_TIME_LONG 750U

typedef enum {
    ledRed = 0,
    ledGreen = 1,
    ledBlue = 2,
    ledAll = 3,
} ledColor;

// *****************************************************************************

void SignalBuzzShort(void);
void SignalBuzzLong(void);
void SignalBuzz(uint32_t time);

void SignalLedToggle(ledColor led);
void SignalLedOn(ledColor led);
void SignalLedOff(ledColor led);

void SignalLedFlashShort(ledColor led);
void SignalLedFlashLong(ledColor led);
void SignalLedFlash(ledColor led, uint32_t time);

void SignalLedBlinkSlow(ledColor led);
void SignalLedBlinkFast(ledColor led);
void SignalLedBlinkStop(ledColor led);

bool EmergencyStopUpdateAndGet(void);

#endif /* SIGNALERS_H */

/* *****************************************************************************
 End of File
 */
