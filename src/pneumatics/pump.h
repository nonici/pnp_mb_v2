/* ************************************************************************** */
/*
  @File Name
    pump.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef PUMP_H    /* Guard against multiple inclusion */
#define PUMP_H    

// *****************************************************************************

#include "definitions.h"

// *****************************************************************************

//#defines

// *****************************************************************************

//Variables

// *****************************************************************************

void PumpsInitialize(void);
void PumpsActivate(void);
void PumpsDeactivate(void);
void PumpAirSetPWM(double percentage);

// *****************************************************************************

#endif /* PUMP_H */

/* *****************************************************************************
 End of File
 */
