/* ************************************************************************** */
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef SOLENOIDS_H    /* Guard against multiple inclusion */
#define SOLENOIDS_H

// *****************************************************************************

#include "definitions.h"
#include "enums.h"

// *****************************************************************************

//    Triple solenoid nozzle schematic:
//    
//          SenseNozzCH       SenseVacTankComm
//                X                   X
//                |        _____      |
//NozzleCH <------+---+----o   o------+---VacTank
//                    |  SolVaccCH
//                    |
//                    |       SenseAirTankComm
//                    |               X
//                    |    _____      |
//                    +----o   o------+---AirTank
//                    |   SolAirCH
//                    |
//                    |
//                    |    _____              
//                    +----o   o----------OutToAthmosphere
//                       SolOutCH
//  
//  
//    Double solenoid nozzle schematic:
//    
//          SenseNozzCH       SenseVacTankComm
//                X                   X
//                |        _____      |
//NozzleCH <------+---+----o   o------+---VacTank
//                    |  SolVaccCH
//                    |
//                    |       SenseAirTankComm
//                    |               X
//                    |             _____      |
//                    +-------+----o   o------+---AirTank
//                            |   SolAirCH
//                            |
//                            |
//                            |    _____              
//                            +----o   o----------OutToAthmosphere
//                               SolOutCH
//
//
//
//List of solenoids and sensors, channel & common
//SolVaccCH
//SolAirCH
//SolOutCH
//SenseNozzCh
//SenseVacTankComm
//SenseAirTankComm
//
//Pick sequence
//SolAirCH open
//SolOutCH open
//SolVacc close
//wait solDecompressTime
//Done
//
//Release (place) sequence (default)
//SolVacc open
//SolAir close
//wait solCompressTime
//SolAir open
//SolOut close
////wait solOutTimeout        //Should be called 1 sec later, not wait here
////SolOut open               //
//
// *****************************************************************************

#define SETTINGS_SOLENOID_SWITCH_TIME_MS    10U

#define SETTINGS_SOL_OPEN                   0
#define SETTINGS_SOL_CLOSE                  1

typedef struct {
    GPIO_PIN solenoidVaccPin;
    GPIO_PIN solenoidAirPin;
    GPIO_PIN solenoidExhaustPin;

    uint32_t solDecompressTimeMs;
    uint32_t solCompressTimeMs;
    uint32_t solPostPickTimeMs;
} pickhead_t;

// *****************************************************************************

pickhead_t pickHead[5];

// *****************************************************************************

void SolenoidsInitialize(void);
void PickHeadPickComponent(nozzle_t headId);
void PickHeadReleaseComponent(nozzle_t headId);

// *****************************************************************************

#endif /* SOLENOIDS_H */

/* *****************************************************************************
 End of File
 */
