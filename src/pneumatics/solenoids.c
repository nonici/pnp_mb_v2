/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#include "solenoids.h"

/* ************************************************************************** */

#define SOLENOID_COMPRESS_TIME_MS           70
#define SOLENOID_DECOMPRESS_TIME_MS         50
#define SOLENOID_POST_PICK_WAIT_TIME_MS     0

/* ************************************************************************** */

void PickHeadDualIdle(nozzle_t headId);
void PickHeadDualGrab(nozzle_t headId);
void PickHeadDualThrow(nozzle_t headId);

void SolenoidsInitialize(void) {

    pickHead[Nozzle1].solenoidVaccPin = FET_CRTL_01_PIN;
    pickHead[Nozzle1].solenoidAirPin = FET_CRTL_02_PIN;
    pickHead[Nozzle1].solenoidExhaustPin = FET_CRTL_03_PIN;

    pickHead[Nozzle1].solCompressTimeMs = SOLENOID_COMPRESS_TIME_MS;
    pickHead[Nozzle1].solDecompressTimeMs = SOLENOID_DECOMPRESS_TIME_MS;
    pickHead[Nozzle1].solPostPickTimeMs = SOLENOID_POST_PICK_WAIT_TIME_MS;
    PickHeadReleaseComponent(Nozzle1);

    pickHead[Nozzle2].solenoidVaccPin = FET_CRTL_04_PIN;
    pickHead[Nozzle2].solenoidAirPin = FET_CRTL_05_PIN;
    pickHead[Nozzle2].solenoidExhaustPin = FET_CRTL_06_PIN;

    pickHead[Nozzle2].solCompressTimeMs = SOLENOID_COMPRESS_TIME_MS;
    pickHead[Nozzle2].solDecompressTimeMs = SOLENOID_DECOMPRESS_TIME_MS;
    pickHead[Nozzle2].solPostPickTimeMs = SOLENOID_POST_PICK_WAIT_TIME_MS;
    PickHeadReleaseComponent(Nozzle2);

    pickHead[Nozzle3].solenoidVaccPin = FET_CRTL_07_PIN;
    pickHead[Nozzle3].solenoidAirPin = FET_CRTL_08_PIN;
    pickHead[Nozzle3].solenoidExhaustPin = FET_CRTL_09_PIN;

    pickHead[Nozzle3].solCompressTimeMs = SOLENOID_COMPRESS_TIME_MS;
    pickHead[Nozzle3].solDecompressTimeMs = SOLENOID_DECOMPRESS_TIME_MS;
    pickHead[Nozzle3].solPostPickTimeMs = SOLENOID_POST_PICK_WAIT_TIME_MS;
    PickHeadReleaseComponent(Nozzle3);

    pickHead[Nozzle4].solenoidVaccPin = FET_CRTL_10_PIN;
    pickHead[Nozzle4].solenoidAirPin = FET_CRTL_11_PIN;
    pickHead[Nozzle4].solenoidExhaustPin = FET_CRTL_12_PIN;

    pickHead[Nozzle4].solCompressTimeMs = SOLENOID_COMPRESS_TIME_MS;
    pickHead[Nozzle4].solDecompressTimeMs = SOLENOID_DECOMPRESS_TIME_MS;
    pickHead[Nozzle4].solPostPickTimeMs = SOLENOID_POST_PICK_WAIT_TIME_MS;
    PickHeadReleaseComponent(Nozzle4);
}

void PickHeadPickComponent(nozzle_t headId) {
    //Pick sequence
    //SolAirCH open
    //SolOutCH open
    //SolVacc close
    //wait solDecompressTime
    //Done

    //Triple solenoid solution
    //GPIO_PinWrite(pickHead[headId].solenoidVaccPin, SETTINGS_SOL_OPEN);
    //GPIO_PinWrite(pickHead[headId].solenoidAirPin, SETTINGS_SOL_OPEN);
    //GPIO_PinWrite(pickHead[headId].solenoidExhaustPin, SETTINGS_SOL_OPEN);
    //GPIO_PinWrite(pickHead[headId].solenoidVaccPin, SETTINGS_SOL_CLOSE);
    //CORETIMER_DelayMs(pickHead[headId].solDecompressTimeMs);
    //CORETIMER_DelayMs(pickHead[headId].solPostPickTimeMs);

    //Double solenoid solution
    PickHeadDualGrab(headId);
    CORETIMER_DelayMs(pickHead[headId].solDecompressTimeMs);
}

void PickHeadReleaseComponent(nozzle_t headId) {
    //Release (place) sequence (default)
    //SolVacc open
    //SolAir close
    //wait solCompressTime
    //SolAir open
    //SolOut close

    //Triple solenoid solution
    //GPIO_PinWrite(pickHead[headId].solenoidVaccPin, SETTINGS_SOL_OPEN);
    //GPIO_PinWrite(pickHead[headId].solenoidAirPin, SETTINGS_SOL_CLOSE);
    //CORETIMER_DelayMs(pickHead[headId].solCompressTimeMs);
    //GPIO_PinWrite(pickHead[headId].solenoidAirPin, SETTINGS_SOL_OPEN);
    //GPIO_PinWrite(pickHead[headId].solenoidExhaustPin, SETTINGS_SOL_CLOSE);

    //Dual solenoid solution
    PickHeadDualThrow(headId);
    CORETIMER_DelayMs(pickHead[headId].solCompressTimeMs);
    PickHeadDualIdle(headId);
}

void PickHeadDualIdle(nozzle_t headId) {
    GPIO_PinWrite(pickHead[headId].solenoidVaccPin, SETTINGS_SOL_OPEN);
    GPIO_PinWrite(pickHead[headId].solenoidAirPin, SETTINGS_SOL_OPEN);
}

void PickHeadDualGrab(nozzle_t headId) {
    GPIO_PinWrite(pickHead[headId].solenoidVaccPin, SETTINGS_SOL_CLOSE);
    GPIO_PinWrite(pickHead[headId].solenoidAirPin, SETTINGS_SOL_OPEN);
}

void PickHeadDualThrow(nozzle_t headId) {
    GPIO_PinWrite(pickHead[headId].solenoidVaccPin, SETTINGS_SOL_OPEN);
    GPIO_PinWrite(pickHead[headId].solenoidAirPin, SETTINGS_SOL_CLOSE);
}

/* *****************************************************************************
 End of File
 */
