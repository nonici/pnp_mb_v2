// *****************************************************************************
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    measurement, reading, and saving sensor values.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "sensors.h"

// *****************************************************************************

#define SENSOR_VALUE_CHAR_ARRAY_SIZE        100
#define SENSOR_CALIB_GAIN_VALUE             9.719699073d
#define SENSOR_FILTER_FACTOR                0.9d
//Sensor filter factor takes 90% of old signal, and 10% of new

//715 ns to scan all 6 channels
//#define FIFO_SENSE_SIZE 20

// *****************************************************************************

typedef struct {
    uint32_t sensorResultsRaw;
    double sensorResultsPa;
    double sensorResultsDifferencePa;
    ADCHS_CHANNEL_NUM sensorAdcChannel;
} pneum_sensor_t;

pneum_sensor_t pneumSensor[6];

// *****************************************************************************

// *****************************************************************************

void SensorsIntialize(void) {
    pneumSensor[SensorN1].sensorAdcChannel = ADCHS_CH10;
    pneumSensor[SensorN2].sensorAdcChannel = ADCHS_CH9;
    pneumSensor[SensorN3].sensorAdcChannel = ADCHS_CH6;
    pneumSensor[SensorN4].sensorAdcChannel = ADCHS_CH5;
    pneumSensor[SensorA].sensorAdcChannel = ADCHS_CH7;
    pneumSensor[SensorV].sensorAdcChannel = ADCHS_CH8;



    ADCHS_ModulesEnable(ADCHS_MODULE7_MASK);
    ADCHS_GlobalLevelConversionStart();
}

void SensorsRead(void) {
    //Read all sensor and save their values respectively
    for (uint32_t i = 0; i < 6; i++) {
        if (ADCHS_ChannelResultIsReady(pneumSensor[i].sensorAdcChannel)) {
            pneumSensor[i].sensorResultsRaw = ADCHS_ChannelResultGet(pneumSensor[i].sensorAdcChannel);
        } else {
            pneumSensor[i].sensorResultsRaw = 0;
        }
    }
    // RingU32Write(&sensorRing, pneumSensor[SensorA].sensorResultsRaw);
}

void SensorsConvertAndFilter(void) {
    for (uint32_t i = 0; i < 6; i++) {
        double new = ((double) pneumSensor[i].sensorResultsRaw) * SENSOR_CALIB_GAIN_VALUE * (1 - SENSOR_FILTER_FACTOR);
        pneumSensor[i].sensorResultsPa = pneumSensor[i].sensorResultsPa * SENSOR_FILTER_FACTOR + new;
        pneumSensor[i].sensorResultsDifferencePa = pneumSensor[i].sensorResultsPa - pneumSensor[SensorV].sensorResultsPa;
        if (pneumSensor[i].sensorResultsDifferencePa < 0) {
            pneumSensor[i].sensorResultsDifferencePa = 0.0;
        }
    }
}

uint32_t SensorsGetValueRaw(sensors_t sensorId) {
    return (pneumSensor[sensorId].sensorResultsRaw);
}

double SensorsGetValueReal(sensors_t sensorId) {
    return (pneumSensor[sensorId].sensorResultsPa);
}

double SensorsGetValueRealHead(nozzle_t nozz) {
    sensors_t sensorId = (sensors_t) nozz;
#ifdef SENSOR_RETURN_DIFFERENCE    
    return (pneumSensor[sensorId].sensorResultsDifferencePa);
#else
    return (pneumSensor[sensorId].sensorResultsPa);
#endif
}
/* *****************************************************************************
 End of File
 */
