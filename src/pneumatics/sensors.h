/* ************************************************************************** */
/*
  @File Name
    sensors.h
 
  @Author
    Flitch

  @Summary
    measurement, reading, and saving sensor values.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */
/*
Sensor position, pin and ADC channel on PCB
[Target, RAXX, ANXX]

                                [N1, RA03, AN10, 1]
   
       [Vac, RA05, AN8, 0]      [N2, RA00, AN9,  2]
   
       [Air, RA02, AN7, 5]      [N3, RA04, AN6,  3]
   
                                [N4, RA01, AN5,  4]
  
  
 */
// *****************************************************************************

#ifndef SENSORS_H    /* Guard against multiple inclusion */
#define SENSORS_H

#define SENSOR_RETURN_DIFFERENCE

// *****************************************************************************

#include "definitions.h"
#include "enums.h"
#include "../comm/serial.h"

// *****************************************************************************

//#defines

// *****************************************************************************

//Variables

// *****************************************************************************

//Functions

void SensorsIntialize(void);
void SensorsRead(void);
void SensorsConvertAndFilter(void);

uint32_t SensorsGetValueRaw(sensors_t sensorId);
double SensorsGetValueReal(sensors_t sensorId);
double SensorsGetValueRealHead(nozzle_t nozz);

// *****************************************************************************

#endif /* SENSORS_H */

/* *****************************************************************************
 End of File
 */
