// *****************************************************************************
/*
  @File Name
    pump.c
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "pump.h"

// *****************************************************************************

#define PUMP_TIMER_PERIOD       42000U

#define PUMP_PERCENTAGE         1.0d
//defines
//structs
//varibles
//constants

// *****************************************************************************

//Core function

void PumpsInitialize(void) {
    //    FET_CRTL_13_OutputEnable();
    //    FET_CRTL_13_Set();
    TMR3_Start();
    OCMP3_Enable();
    PumpsDeactivate();
}

void PumpsActivate(void) {
    OCMP3_Enable();
}

void PumpsDeactivate(void) {
    OCMP3_Disable();
}

void PumpAirSetPWM(double percentage) {
    //    double value = percentage * PUMP_TIMER_PERIOD;
    double value = PUMP_PERCENTAGE * PUMP_TIMER_PERIOD;
    OCMP3_CompareSecondaryValueSet((uint16_t) value);
}

//Global functions

//Local functions


/* *****************************************************************************
 End of File
 */
