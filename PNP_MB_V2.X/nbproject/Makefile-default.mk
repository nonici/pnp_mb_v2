#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/PNP_MB_V2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/PNP_MB_V2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../src/comm/parser.c ../src/comm/serial.c ../src/comm/wifi.c ../src/comm/gcode.c ../src/comm/shell.c ../src/comm/comm.c ../src/comm/canbus.c ../src/config/default/peripheral/adchs/plib_adchs.c ../src/config/default/peripheral/cache/plib_cache.c ../src/config/default/peripheral/cache/plib_cache_pic32mz.S ../src/config/default/peripheral/can/plib_can1.c ../src/config/default/peripheral/clk/plib_clk.c ../src/config/default/peripheral/coretimer/plib_coretimer.c ../src/config/default/peripheral/evic/plib_evic.c ../src/config/default/peripheral/gpio/plib_gpio.c ../src/config/default/peripheral/ocmp/plib_ocmp3.c ../src/config/default/peripheral/tmr/plib_tmr2.c ../src/config/default/peripheral/tmr/plib_tmr4.c ../src/config/default/peripheral/tmr/plib_tmr3.c ../src/config/default/peripheral/uart/plib_uart5.c ../src/config/default/peripheral/uart/plib_uart3.c ../src/config/default/peripheral/uart/plib_uart4.c ../src/config/default/peripheral/uart/plib_uart2.c ..\src\config\default\peripheral\uart\plib_uart6.c ../src/config/default/stdio/xc32_monitor.c ../src/config/default/initialization.c ../src/config/default/interrupts.c ../src/config/default/exceptions.c ../src/config/default/exceptionsHandler.S ../src/feeders/feeders.c ../src/feeders/slots.c ../src/log/log.c ../src/motion/engine.c ../src/motion/driver.c ../src/motion/movement.c ../src/motion/profiler.c ../src/motion/blocks.c ../src/motion/axes_struct.c ../src/pneumatics/solenoids.c ../src/pneumatics/pump.c ../src/pneumatics/sensors.c ../src/pnp/signalers.c ../src/pnp/cam_led.c ../src/pnp/tests.c ../src/pnp/psu.c ../src/system/system.c ../../../tools/printf.c ../../../tools/ms_timer.c ../src/main.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1019403322/parser.o ${OBJECTDIR}/_ext/1019403322/serial.o ${OBJECTDIR}/_ext/1019403322/wifi.o ${OBJECTDIR}/_ext/1019403322/gcode.o ${OBJECTDIR}/_ext/1019403322/shell.o ${OBJECTDIR}/_ext/1019403322/comm.o ${OBJECTDIR}/_ext/1019403322/canbus.o ${OBJECTDIR}/_ext/1982400153/plib_adchs.o ${OBJECTDIR}/_ext/1984157808/plib_cache.o ${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o ${OBJECTDIR}/_ext/60165182/plib_can1.o ${OBJECTDIR}/_ext/60165520/plib_clk.o ${OBJECTDIR}/_ext/1249264884/plib_coretimer.o ${OBJECTDIR}/_ext/1865200349/plib_evic.o ${OBJECTDIR}/_ext/1865254177/plib_gpio.o ${OBJECTDIR}/_ext/1865480137/plib_ocmp3.o ${OBJECTDIR}/_ext/60181895/plib_tmr2.o ${OBJECTDIR}/_ext/60181895/plib_tmr4.o ${OBJECTDIR}/_ext/60181895/plib_tmr3.o ${OBJECTDIR}/_ext/1865657120/plib_uart5.o ${OBJECTDIR}/_ext/1865657120/plib_uart3.o ${OBJECTDIR}/_ext/1865657120/plib_uart4.o ${OBJECTDIR}/_ext/1865657120/plib_uart2.o ${OBJECTDIR}/_ext/631034509/plib_uart6.o ${OBJECTDIR}/_ext/163028504/xc32_monitor.o ${OBJECTDIR}/_ext/1171490990/initialization.o ${OBJECTDIR}/_ext/1171490990/interrupts.o ${OBJECTDIR}/_ext/1171490990/exceptions.o ${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o ${OBJECTDIR}/_ext/1256919026/feeders.o ${OBJECTDIR}/_ext/1256919026/slots.o ${OBJECTDIR}/_ext/659861322/log.o ${OBJECTDIR}/_ext/107549200/engine.o ${OBJECTDIR}/_ext/107549200/driver.o ${OBJECTDIR}/_ext/107549200/movement.o ${OBJECTDIR}/_ext/107549200/profiler.o ${OBJECTDIR}/_ext/107549200/blocks.o ${OBJECTDIR}/_ext/107549200/axes_struct.o ${OBJECTDIR}/_ext/13954849/solenoids.o ${OBJECTDIR}/_ext/13954849/pump.o ${OBJECTDIR}/_ext/13954849/sensors.o ${OBJECTDIR}/_ext/659865144/signalers.o ${OBJECTDIR}/_ext/659865144/cam_led.o ${OBJECTDIR}/_ext/659865144/tests.o ${OBJECTDIR}/_ext/659865144/psu.o ${OBJECTDIR}/_ext/73441385/system.o ${OBJECTDIR}/_ext/1014815340/printf.o ${OBJECTDIR}/_ext/1014815340/ms_timer.o ${OBJECTDIR}/_ext/1360937237/main.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1019403322/parser.o.d ${OBJECTDIR}/_ext/1019403322/serial.o.d ${OBJECTDIR}/_ext/1019403322/wifi.o.d ${OBJECTDIR}/_ext/1019403322/gcode.o.d ${OBJECTDIR}/_ext/1019403322/shell.o.d ${OBJECTDIR}/_ext/1019403322/comm.o.d ${OBJECTDIR}/_ext/1019403322/canbus.o.d ${OBJECTDIR}/_ext/1982400153/plib_adchs.o.d ${OBJECTDIR}/_ext/1984157808/plib_cache.o.d ${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o.d ${OBJECTDIR}/_ext/60165182/plib_can1.o.d ${OBJECTDIR}/_ext/60165520/plib_clk.o.d ${OBJECTDIR}/_ext/1249264884/plib_coretimer.o.d ${OBJECTDIR}/_ext/1865200349/plib_evic.o.d ${OBJECTDIR}/_ext/1865254177/plib_gpio.o.d ${OBJECTDIR}/_ext/1865480137/plib_ocmp3.o.d ${OBJECTDIR}/_ext/60181895/plib_tmr2.o.d ${OBJECTDIR}/_ext/60181895/plib_tmr4.o.d ${OBJECTDIR}/_ext/60181895/plib_tmr3.o.d ${OBJECTDIR}/_ext/1865657120/plib_uart5.o.d ${OBJECTDIR}/_ext/1865657120/plib_uart3.o.d ${OBJECTDIR}/_ext/1865657120/plib_uart4.o.d ${OBJECTDIR}/_ext/1865657120/plib_uart2.o.d ${OBJECTDIR}/_ext/631034509/plib_uart6.o.d ${OBJECTDIR}/_ext/163028504/xc32_monitor.o.d ${OBJECTDIR}/_ext/1171490990/initialization.o.d ${OBJECTDIR}/_ext/1171490990/interrupts.o.d ${OBJECTDIR}/_ext/1171490990/exceptions.o.d ${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o.d ${OBJECTDIR}/_ext/1256919026/feeders.o.d ${OBJECTDIR}/_ext/1256919026/slots.o.d ${OBJECTDIR}/_ext/659861322/log.o.d ${OBJECTDIR}/_ext/107549200/engine.o.d ${OBJECTDIR}/_ext/107549200/driver.o.d ${OBJECTDIR}/_ext/107549200/movement.o.d ${OBJECTDIR}/_ext/107549200/profiler.o.d ${OBJECTDIR}/_ext/107549200/blocks.o.d ${OBJECTDIR}/_ext/107549200/axes_struct.o.d ${OBJECTDIR}/_ext/13954849/solenoids.o.d ${OBJECTDIR}/_ext/13954849/pump.o.d ${OBJECTDIR}/_ext/13954849/sensors.o.d ${OBJECTDIR}/_ext/659865144/signalers.o.d ${OBJECTDIR}/_ext/659865144/cam_led.o.d ${OBJECTDIR}/_ext/659865144/tests.o.d ${OBJECTDIR}/_ext/659865144/psu.o.d ${OBJECTDIR}/_ext/73441385/system.o.d ${OBJECTDIR}/_ext/1014815340/printf.o.d ${OBJECTDIR}/_ext/1014815340/ms_timer.o.d ${OBJECTDIR}/_ext/1360937237/main.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1019403322/parser.o ${OBJECTDIR}/_ext/1019403322/serial.o ${OBJECTDIR}/_ext/1019403322/wifi.o ${OBJECTDIR}/_ext/1019403322/gcode.o ${OBJECTDIR}/_ext/1019403322/shell.o ${OBJECTDIR}/_ext/1019403322/comm.o ${OBJECTDIR}/_ext/1019403322/canbus.o ${OBJECTDIR}/_ext/1982400153/plib_adchs.o ${OBJECTDIR}/_ext/1984157808/plib_cache.o ${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o ${OBJECTDIR}/_ext/60165182/plib_can1.o ${OBJECTDIR}/_ext/60165520/plib_clk.o ${OBJECTDIR}/_ext/1249264884/plib_coretimer.o ${OBJECTDIR}/_ext/1865200349/plib_evic.o ${OBJECTDIR}/_ext/1865254177/plib_gpio.o ${OBJECTDIR}/_ext/1865480137/plib_ocmp3.o ${OBJECTDIR}/_ext/60181895/plib_tmr2.o ${OBJECTDIR}/_ext/60181895/plib_tmr4.o ${OBJECTDIR}/_ext/60181895/plib_tmr3.o ${OBJECTDIR}/_ext/1865657120/plib_uart5.o ${OBJECTDIR}/_ext/1865657120/plib_uart3.o ${OBJECTDIR}/_ext/1865657120/plib_uart4.o ${OBJECTDIR}/_ext/1865657120/plib_uart2.o ${OBJECTDIR}/_ext/631034509/plib_uart6.o ${OBJECTDIR}/_ext/163028504/xc32_monitor.o ${OBJECTDIR}/_ext/1171490990/initialization.o ${OBJECTDIR}/_ext/1171490990/interrupts.o ${OBJECTDIR}/_ext/1171490990/exceptions.o ${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o ${OBJECTDIR}/_ext/1256919026/feeders.o ${OBJECTDIR}/_ext/1256919026/slots.o ${OBJECTDIR}/_ext/659861322/log.o ${OBJECTDIR}/_ext/107549200/engine.o ${OBJECTDIR}/_ext/107549200/driver.o ${OBJECTDIR}/_ext/107549200/movement.o ${OBJECTDIR}/_ext/107549200/profiler.o ${OBJECTDIR}/_ext/107549200/blocks.o ${OBJECTDIR}/_ext/107549200/axes_struct.o ${OBJECTDIR}/_ext/13954849/solenoids.o ${OBJECTDIR}/_ext/13954849/pump.o ${OBJECTDIR}/_ext/13954849/sensors.o ${OBJECTDIR}/_ext/659865144/signalers.o ${OBJECTDIR}/_ext/659865144/cam_led.o ${OBJECTDIR}/_ext/659865144/tests.o ${OBJECTDIR}/_ext/659865144/psu.o ${OBJECTDIR}/_ext/73441385/system.o ${OBJECTDIR}/_ext/1014815340/printf.o ${OBJECTDIR}/_ext/1014815340/ms_timer.o ${OBJECTDIR}/_ext/1360937237/main.o

# Source Files
SOURCEFILES=../src/comm/parser.c ../src/comm/serial.c ../src/comm/wifi.c ../src/comm/gcode.c ../src/comm/shell.c ../src/comm/comm.c ../src/comm/canbus.c ../src/config/default/peripheral/adchs/plib_adchs.c ../src/config/default/peripheral/cache/plib_cache.c ../src/config/default/peripheral/cache/plib_cache_pic32mz.S ../src/config/default/peripheral/can/plib_can1.c ../src/config/default/peripheral/clk/plib_clk.c ../src/config/default/peripheral/coretimer/plib_coretimer.c ../src/config/default/peripheral/evic/plib_evic.c ../src/config/default/peripheral/gpio/plib_gpio.c ../src/config/default/peripheral/ocmp/plib_ocmp3.c ../src/config/default/peripheral/tmr/plib_tmr2.c ../src/config/default/peripheral/tmr/plib_tmr4.c ../src/config/default/peripheral/tmr/plib_tmr3.c ../src/config/default/peripheral/uart/plib_uart5.c ../src/config/default/peripheral/uart/plib_uart3.c ../src/config/default/peripheral/uart/plib_uart4.c ../src/config/default/peripheral/uart/plib_uart2.c ..\src\config\default\peripheral\uart\plib_uart6.c ../src/config/default/stdio/xc32_monitor.c ../src/config/default/initialization.c ../src/config/default/interrupts.c ../src/config/default/exceptions.c ../src/config/default/exceptionsHandler.S ../src/feeders/feeders.c ../src/feeders/slots.c ../src/log/log.c ../src/motion/engine.c ../src/motion/driver.c ../src/motion/movement.c ../src/motion/profiler.c ../src/motion/blocks.c ../src/motion/axes_struct.c ../src/pneumatics/solenoids.c ../src/pneumatics/pump.c ../src/pneumatics/sensors.c ../src/pnp/signalers.c ../src/pnp/cam_led.c ../src/pnp/tests.c ../src/pnp/psu.c ../src/system/system.c ../../../tools/printf.c ../../../tools/ms_timer.c ../src/main.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

# The following macros may be used in the pre and post step lines
Device=PIC32MZ2048EFH144
ProjectDir="C:\Elektronika\firmware\PNP_MB_V2\firmware\PNP_MB_V2.X"
ProjectName=PNP_MB_V2
ConfName=default
ImagePath="dist\default\${IMAGE_TYPE}\PNP_MB_V2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}"
ImageDir="dist\default\${IMAGE_TYPE}"
ImageName="PNP_MB_V2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}"
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IsDebug="true"
else
IsDebug="false"
endif

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/PNP_MB_V2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
	@echo "--------------------------------------"
	@echo "User defined post-build step: [${MP_CC_DIR}\xc32-objdump -m mips -d -S ${ImageDir}\${PROJECTNAME}.${IMAGE_TYPE}.elf > list.txt]"
	@${MP_CC_DIR}\xc32-objdump -m mips -d -S ${ImageDir}\${PROJECTNAME}.${IMAGE_TYPE}.elf > list.txt
	@echo "--------------------------------------"

MP_PROCESSOR_OPTION=32MZ2048EFH144
MP_LINKER_FILE_OPTION=,--script="..\src\config\default\p32MZ2048EFH144.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o: ../src/config/default/peripheral/cache/plib_cache_pic32mz.S  .generated_files/f0b06c7dfb486e10ddb4971a49729fceb7bbe52a.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1984157808" 
	@${RM} ${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o 
	@${RM} ${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o.ok ${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o.err 
	${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o.d"  -o ${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o ../src/config/default/peripheral/cache/plib_cache_pic32mz.S  -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -std=c99 -Wall -Wdouble-promotion -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1 -I"../../../tools" -I"../src",-ah -mdfp="${DFP_DIR}"
	@${FIXDEPS} "${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o.d" "${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o: ../src/config/default/exceptionsHandler.S  .generated_files/6d0898cedd4564b954cebb89819afeea8dde0d96.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1171490990" 
	@${RM} ${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o.d 
	@${RM} ${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o 
	@${RM} ${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o.ok ${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o.err 
	${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o.d"  -o ${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o ../src/config/default/exceptionsHandler.S  -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -std=c99 -Wall -Wdouble-promotion -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1 -I"../../../tools" -I"../src",-ah -mdfp="${DFP_DIR}"
	@${FIXDEPS} "${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o.d" "${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o: ../src/config/default/peripheral/cache/plib_cache_pic32mz.S  .generated_files/2d591013cbd705c051d0acc48e68bb64ee4698da.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1984157808" 
	@${RM} ${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o 
	@${RM} ${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o.ok ${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o.err 
	${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o.d"  -o ${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o ../src/config/default/peripheral/cache/plib_cache_pic32mz.S  -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -std=c99 -Wall -Wdouble-promotion -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o.asm.d",--gdwarf-2 -I"../../../tools" -I"../src",-ah -mdfp="${DFP_DIR}"
	@${FIXDEPS} "${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o.d" "${OBJECTDIR}/_ext/1984157808/plib_cache_pic32mz.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o: ../src/config/default/exceptionsHandler.S  .generated_files/753a6113c4a7731b43c3927cf1935647eef35074.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1171490990" 
	@${RM} ${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o.d 
	@${RM} ${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o 
	@${RM} ${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o.ok ${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o.err 
	${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o.d"  -o ${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o ../src/config/default/exceptionsHandler.S  -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -std=c99 -Wall -Wdouble-promotion -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o.asm.d",--gdwarf-2 -I"../../../tools" -I"../src",-ah -mdfp="${DFP_DIR}"
	@${FIXDEPS} "${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o.d" "${OBJECTDIR}/_ext/1171490990/exceptionsHandler.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1019403322/parser.o: ../src/comm/parser.c  .generated_files/f5f5f474b880c51cef289359236db314331dd8a3.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1019403322" 
	@${RM} ${OBJECTDIR}/_ext/1019403322/parser.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019403322/parser.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1019403322/parser.o.d" -o ${OBJECTDIR}/_ext/1019403322/parser.o ../src/comm/parser.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1019403322/serial.o: ../src/comm/serial.c  .generated_files/44a3e315a3ac8862b365a2fdbfe283e73548b520.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1019403322" 
	@${RM} ${OBJECTDIR}/_ext/1019403322/serial.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019403322/serial.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1019403322/serial.o.d" -o ${OBJECTDIR}/_ext/1019403322/serial.o ../src/comm/serial.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1019403322/wifi.o: ../src/comm/wifi.c  .generated_files/6c6ea5ba74af93b74ec68f1f6ea2434b1a937202.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1019403322" 
	@${RM} ${OBJECTDIR}/_ext/1019403322/wifi.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019403322/wifi.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1019403322/wifi.o.d" -o ${OBJECTDIR}/_ext/1019403322/wifi.o ../src/comm/wifi.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1019403322/gcode.o: ../src/comm/gcode.c  .generated_files/89c56b069691e343b08eec2db509050acd03f0c7.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1019403322" 
	@${RM} ${OBJECTDIR}/_ext/1019403322/gcode.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019403322/gcode.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1019403322/gcode.o.d" -o ${OBJECTDIR}/_ext/1019403322/gcode.o ../src/comm/gcode.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1019403322/shell.o: ../src/comm/shell.c  .generated_files/172f5debc54288933903ccc7039991c21309ef58.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1019403322" 
	@${RM} ${OBJECTDIR}/_ext/1019403322/shell.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019403322/shell.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1019403322/shell.o.d" -o ${OBJECTDIR}/_ext/1019403322/shell.o ../src/comm/shell.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1019403322/comm.o: ../src/comm/comm.c  .generated_files/7c30d856ea1226ee4e99aac02cb8ebdfe2eb5950.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1019403322" 
	@${RM} ${OBJECTDIR}/_ext/1019403322/comm.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019403322/comm.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1019403322/comm.o.d" -o ${OBJECTDIR}/_ext/1019403322/comm.o ../src/comm/comm.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1019403322/canbus.o: ../src/comm/canbus.c  .generated_files/8db9bdc87c946827b649ab24779f546f0b79f03c.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1019403322" 
	@${RM} ${OBJECTDIR}/_ext/1019403322/canbus.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019403322/canbus.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1019403322/canbus.o.d" -o ${OBJECTDIR}/_ext/1019403322/canbus.o ../src/comm/canbus.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1982400153/plib_adchs.o: ../src/config/default/peripheral/adchs/plib_adchs.c  .generated_files/f4049da3f048e17804bdb2a446af5590a0da607e.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1982400153" 
	@${RM} ${OBJECTDIR}/_ext/1982400153/plib_adchs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1982400153/plib_adchs.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1982400153/plib_adchs.o.d" -o ${OBJECTDIR}/_ext/1982400153/plib_adchs.o ../src/config/default/peripheral/adchs/plib_adchs.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1984157808/plib_cache.o: ../src/config/default/peripheral/cache/plib_cache.c  .generated_files/108e37cfb66cd321809cc619164d7910ff87be17.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1984157808" 
	@${RM} ${OBJECTDIR}/_ext/1984157808/plib_cache.o.d 
	@${RM} ${OBJECTDIR}/_ext/1984157808/plib_cache.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1984157808/plib_cache.o.d" -o ${OBJECTDIR}/_ext/1984157808/plib_cache.o ../src/config/default/peripheral/cache/plib_cache.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/60165182/plib_can1.o: ../src/config/default/peripheral/can/plib_can1.c  .generated_files/a851f109baf04b887e7256046adcf4c5e15a0ee9.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/60165182" 
	@${RM} ${OBJECTDIR}/_ext/60165182/plib_can1.o.d 
	@${RM} ${OBJECTDIR}/_ext/60165182/plib_can1.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/60165182/plib_can1.o.d" -o ${OBJECTDIR}/_ext/60165182/plib_can1.o ../src/config/default/peripheral/can/plib_can1.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/60165520/plib_clk.o: ../src/config/default/peripheral/clk/plib_clk.c  .generated_files/5e0169a17a2cec433790c9ed5f615566d7e5e4a.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/60165520" 
	@${RM} ${OBJECTDIR}/_ext/60165520/plib_clk.o.d 
	@${RM} ${OBJECTDIR}/_ext/60165520/plib_clk.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/60165520/plib_clk.o.d" -o ${OBJECTDIR}/_ext/60165520/plib_clk.o ../src/config/default/peripheral/clk/plib_clk.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1249264884/plib_coretimer.o: ../src/config/default/peripheral/coretimer/plib_coretimer.c  .generated_files/5df1679841abb9c6cc2f7ce031646cfd0c209e3.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1249264884" 
	@${RM} ${OBJECTDIR}/_ext/1249264884/plib_coretimer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1249264884/plib_coretimer.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1249264884/plib_coretimer.o.d" -o ${OBJECTDIR}/_ext/1249264884/plib_coretimer.o ../src/config/default/peripheral/coretimer/plib_coretimer.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1865200349/plib_evic.o: ../src/config/default/peripheral/evic/plib_evic.c  .generated_files/437d89fb641d3aab58e8424dfe40ba0697c72fda.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1865200349" 
	@${RM} ${OBJECTDIR}/_ext/1865200349/plib_evic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865200349/plib_evic.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1865200349/plib_evic.o.d" -o ${OBJECTDIR}/_ext/1865200349/plib_evic.o ../src/config/default/peripheral/evic/plib_evic.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1865254177/plib_gpio.o: ../src/config/default/peripheral/gpio/plib_gpio.c  .generated_files/49d784661cde752029a131bc11a17c122e3845d9.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1865254177" 
	@${RM} ${OBJECTDIR}/_ext/1865254177/plib_gpio.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865254177/plib_gpio.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1865254177/plib_gpio.o.d" -o ${OBJECTDIR}/_ext/1865254177/plib_gpio.o ../src/config/default/peripheral/gpio/plib_gpio.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1865480137/plib_ocmp3.o: ../src/config/default/peripheral/ocmp/plib_ocmp3.c  .generated_files/e7df5516082eb10e0f3ebe6671f55cb1d7963df2.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1865480137" 
	@${RM} ${OBJECTDIR}/_ext/1865480137/plib_ocmp3.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865480137/plib_ocmp3.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1865480137/plib_ocmp3.o.d" -o ${OBJECTDIR}/_ext/1865480137/plib_ocmp3.o ../src/config/default/peripheral/ocmp/plib_ocmp3.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/60181895/plib_tmr2.o: ../src/config/default/peripheral/tmr/plib_tmr2.c  .generated_files/d33b3ea629352eea183f8b94aea2409730ba9a3.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/60181895" 
	@${RM} ${OBJECTDIR}/_ext/60181895/plib_tmr2.o.d 
	@${RM} ${OBJECTDIR}/_ext/60181895/plib_tmr2.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/60181895/plib_tmr2.o.d" -o ${OBJECTDIR}/_ext/60181895/plib_tmr2.o ../src/config/default/peripheral/tmr/plib_tmr2.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/60181895/plib_tmr4.o: ../src/config/default/peripheral/tmr/plib_tmr4.c  .generated_files/e9d3922c2983d51c70e223614072d5a4d57e1d7a.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/60181895" 
	@${RM} ${OBJECTDIR}/_ext/60181895/plib_tmr4.o.d 
	@${RM} ${OBJECTDIR}/_ext/60181895/plib_tmr4.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/60181895/plib_tmr4.o.d" -o ${OBJECTDIR}/_ext/60181895/plib_tmr4.o ../src/config/default/peripheral/tmr/plib_tmr4.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/60181895/plib_tmr3.o: ../src/config/default/peripheral/tmr/plib_tmr3.c  .generated_files/699a2736a708db35ba3d78b6802037dfde7641d5.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/60181895" 
	@${RM} ${OBJECTDIR}/_ext/60181895/plib_tmr3.o.d 
	@${RM} ${OBJECTDIR}/_ext/60181895/plib_tmr3.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/60181895/plib_tmr3.o.d" -o ${OBJECTDIR}/_ext/60181895/plib_tmr3.o ../src/config/default/peripheral/tmr/plib_tmr3.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1865657120/plib_uart5.o: ../src/config/default/peripheral/uart/plib_uart5.c  .generated_files/a079f7f4b8623e05fed63c2a4a3a891995ec5de1.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1865657120" 
	@${RM} ${OBJECTDIR}/_ext/1865657120/plib_uart5.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865657120/plib_uart5.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1865657120/plib_uart5.o.d" -o ${OBJECTDIR}/_ext/1865657120/plib_uart5.o ../src/config/default/peripheral/uart/plib_uart5.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1865657120/plib_uart3.o: ../src/config/default/peripheral/uart/plib_uart3.c  .generated_files/f70feb41d1a866522b2ba238c5199e21e7a91004.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1865657120" 
	@${RM} ${OBJECTDIR}/_ext/1865657120/plib_uart3.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865657120/plib_uart3.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1865657120/plib_uart3.o.d" -o ${OBJECTDIR}/_ext/1865657120/plib_uart3.o ../src/config/default/peripheral/uart/plib_uart3.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1865657120/plib_uart4.o: ../src/config/default/peripheral/uart/plib_uart4.c  .generated_files/c2b4979f00fc58ca3114a47922f134085843e3b2.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1865657120" 
	@${RM} ${OBJECTDIR}/_ext/1865657120/plib_uart4.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865657120/plib_uart4.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1865657120/plib_uart4.o.d" -o ${OBJECTDIR}/_ext/1865657120/plib_uart4.o ../src/config/default/peripheral/uart/plib_uart4.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1865657120/plib_uart2.o: ../src/config/default/peripheral/uart/plib_uart2.c  .generated_files/494e8cc7d0ac6aebf2123666c116fc3d93f66a5e.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1865657120" 
	@${RM} ${OBJECTDIR}/_ext/1865657120/plib_uart2.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865657120/plib_uart2.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1865657120/plib_uart2.o.d" -o ${OBJECTDIR}/_ext/1865657120/plib_uart2.o ../src/config/default/peripheral/uart/plib_uart2.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/631034509/plib_uart6.o: ..\src\config\default\peripheral\uart\plib_uart6.c  .generated_files/6941b1b52da9953ce2a036ef42adcce404d9447.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/631034509" 
	@${RM} ${OBJECTDIR}/_ext/631034509/plib_uart6.o.d 
	@${RM} ${OBJECTDIR}/_ext/631034509/plib_uart6.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/631034509/plib_uart6.o.d" -o ${OBJECTDIR}/_ext/631034509/plib_uart6.o ..\src\config\default\peripheral\uart\plib_uart6.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/163028504/xc32_monitor.o: ../src/config/default/stdio/xc32_monitor.c  .generated_files/b7d2d53d512bf213440fc0894fe07138fa48922c.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/163028504" 
	@${RM} ${OBJECTDIR}/_ext/163028504/xc32_monitor.o.d 
	@${RM} ${OBJECTDIR}/_ext/163028504/xc32_monitor.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/163028504/xc32_monitor.o.d" -o ${OBJECTDIR}/_ext/163028504/xc32_monitor.o ../src/config/default/stdio/xc32_monitor.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1171490990/initialization.o: ../src/config/default/initialization.c  .generated_files/db7d9974f457996f1cc01ac972ff4b4883497aa9.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1171490990" 
	@${RM} ${OBJECTDIR}/_ext/1171490990/initialization.o.d 
	@${RM} ${OBJECTDIR}/_ext/1171490990/initialization.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1171490990/initialization.o.d" -o ${OBJECTDIR}/_ext/1171490990/initialization.o ../src/config/default/initialization.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1171490990/interrupts.o: ../src/config/default/interrupts.c  .generated_files/a79e2b66976216fcbbb4f101880b0bebfa01768b.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1171490990" 
	@${RM} ${OBJECTDIR}/_ext/1171490990/interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/1171490990/interrupts.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1171490990/interrupts.o.d" -o ${OBJECTDIR}/_ext/1171490990/interrupts.o ../src/config/default/interrupts.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1171490990/exceptions.o: ../src/config/default/exceptions.c  .generated_files/5248e427533237c83be3260450475b4893b8d6c7.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1171490990" 
	@${RM} ${OBJECTDIR}/_ext/1171490990/exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1171490990/exceptions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1171490990/exceptions.o.d" -o ${OBJECTDIR}/_ext/1171490990/exceptions.o ../src/config/default/exceptions.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1256919026/feeders.o: ../src/feeders/feeders.c  .generated_files/56b0e52a17c146e01c0d68f812e82e738aa33e18.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1256919026" 
	@${RM} ${OBJECTDIR}/_ext/1256919026/feeders.o.d 
	@${RM} ${OBJECTDIR}/_ext/1256919026/feeders.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1256919026/feeders.o.d" -o ${OBJECTDIR}/_ext/1256919026/feeders.o ../src/feeders/feeders.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1256919026/slots.o: ../src/feeders/slots.c  .generated_files/4c64a8304aca27abe0531b774fb4a1283ae460f5.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1256919026" 
	@${RM} ${OBJECTDIR}/_ext/1256919026/slots.o.d 
	@${RM} ${OBJECTDIR}/_ext/1256919026/slots.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1256919026/slots.o.d" -o ${OBJECTDIR}/_ext/1256919026/slots.o ../src/feeders/slots.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/659861322/log.o: ../src/log/log.c  .generated_files/ffb14880e115685fd4356d7c5b479fd112ed034e.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/659861322" 
	@${RM} ${OBJECTDIR}/_ext/659861322/log.o.d 
	@${RM} ${OBJECTDIR}/_ext/659861322/log.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/659861322/log.o.d" -o ${OBJECTDIR}/_ext/659861322/log.o ../src/log/log.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/107549200/engine.o: ../src/motion/engine.c  .generated_files/a81560a719a0dcdddfe555e85763a19d5b15f692.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/107549200" 
	@${RM} ${OBJECTDIR}/_ext/107549200/engine.o.d 
	@${RM} ${OBJECTDIR}/_ext/107549200/engine.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/107549200/engine.o.d" -o ${OBJECTDIR}/_ext/107549200/engine.o ../src/motion/engine.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/107549200/driver.o: ../src/motion/driver.c  .generated_files/47559d990f819426383febaac797343e956b6421.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/107549200" 
	@${RM} ${OBJECTDIR}/_ext/107549200/driver.o.d 
	@${RM} ${OBJECTDIR}/_ext/107549200/driver.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/107549200/driver.o.d" -o ${OBJECTDIR}/_ext/107549200/driver.o ../src/motion/driver.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/107549200/movement.o: ../src/motion/movement.c  .generated_files/fa8f38cc5e65c1ed1965200460efcbc075193cfb.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/107549200" 
	@${RM} ${OBJECTDIR}/_ext/107549200/movement.o.d 
	@${RM} ${OBJECTDIR}/_ext/107549200/movement.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/107549200/movement.o.d" -o ${OBJECTDIR}/_ext/107549200/movement.o ../src/motion/movement.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/107549200/profiler.o: ../src/motion/profiler.c  .generated_files/a20a0b77fd39025843e829147173359d34b31371.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/107549200" 
	@${RM} ${OBJECTDIR}/_ext/107549200/profiler.o.d 
	@${RM} ${OBJECTDIR}/_ext/107549200/profiler.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/107549200/profiler.o.d" -o ${OBJECTDIR}/_ext/107549200/profiler.o ../src/motion/profiler.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/107549200/blocks.o: ../src/motion/blocks.c  .generated_files/5577bc36777d7357bdca4604810662b23ea700f1.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/107549200" 
	@${RM} ${OBJECTDIR}/_ext/107549200/blocks.o.d 
	@${RM} ${OBJECTDIR}/_ext/107549200/blocks.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/107549200/blocks.o.d" -o ${OBJECTDIR}/_ext/107549200/blocks.o ../src/motion/blocks.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/107549200/axes_struct.o: ../src/motion/axes_struct.c  .generated_files/b8a210236b9c1629be7daaf6c6cdac2f113c8c61.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/107549200" 
	@${RM} ${OBJECTDIR}/_ext/107549200/axes_struct.o.d 
	@${RM} ${OBJECTDIR}/_ext/107549200/axes_struct.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/107549200/axes_struct.o.d" -o ${OBJECTDIR}/_ext/107549200/axes_struct.o ../src/motion/axes_struct.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/13954849/solenoids.o: ../src/pneumatics/solenoids.c  .generated_files/85e0d428e733e4c4b5f0a89c0a25b8c0277c78d0.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/13954849" 
	@${RM} ${OBJECTDIR}/_ext/13954849/solenoids.o.d 
	@${RM} ${OBJECTDIR}/_ext/13954849/solenoids.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/13954849/solenoids.o.d" -o ${OBJECTDIR}/_ext/13954849/solenoids.o ../src/pneumatics/solenoids.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/13954849/pump.o: ../src/pneumatics/pump.c  .generated_files/1101b9e4aac293024870311ba7fd8e6750ecac52.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/13954849" 
	@${RM} ${OBJECTDIR}/_ext/13954849/pump.o.d 
	@${RM} ${OBJECTDIR}/_ext/13954849/pump.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/13954849/pump.o.d" -o ${OBJECTDIR}/_ext/13954849/pump.o ../src/pneumatics/pump.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/13954849/sensors.o: ../src/pneumatics/sensors.c  .generated_files/c249a4a8c9571799b51cc90ee20e6c2eee117d06.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/13954849" 
	@${RM} ${OBJECTDIR}/_ext/13954849/sensors.o.d 
	@${RM} ${OBJECTDIR}/_ext/13954849/sensors.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/13954849/sensors.o.d" -o ${OBJECTDIR}/_ext/13954849/sensors.o ../src/pneumatics/sensors.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/659865144/signalers.o: ../src/pnp/signalers.c  .generated_files/6c0163316793b440b6341f5a4996bb0c80aab856.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/659865144" 
	@${RM} ${OBJECTDIR}/_ext/659865144/signalers.o.d 
	@${RM} ${OBJECTDIR}/_ext/659865144/signalers.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/659865144/signalers.o.d" -o ${OBJECTDIR}/_ext/659865144/signalers.o ../src/pnp/signalers.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/659865144/cam_led.o: ../src/pnp/cam_led.c  .generated_files/5f405ac059e0aa59565d3a00f47ec3d73d1857e2.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/659865144" 
	@${RM} ${OBJECTDIR}/_ext/659865144/cam_led.o.d 
	@${RM} ${OBJECTDIR}/_ext/659865144/cam_led.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/659865144/cam_led.o.d" -o ${OBJECTDIR}/_ext/659865144/cam_led.o ../src/pnp/cam_led.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/659865144/tests.o: ../src/pnp/tests.c  .generated_files/1809f3672bb26fb672190f1dbf8842b7ff1a750d.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/659865144" 
	@${RM} ${OBJECTDIR}/_ext/659865144/tests.o.d 
	@${RM} ${OBJECTDIR}/_ext/659865144/tests.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/659865144/tests.o.d" -o ${OBJECTDIR}/_ext/659865144/tests.o ../src/pnp/tests.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/659865144/psu.o: ../src/pnp/psu.c  .generated_files/7c7d2d4291dc4d4b04559c7d4db60a2dce03632b.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/659865144" 
	@${RM} ${OBJECTDIR}/_ext/659865144/psu.o.d 
	@${RM} ${OBJECTDIR}/_ext/659865144/psu.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/659865144/psu.o.d" -o ${OBJECTDIR}/_ext/659865144/psu.o ../src/pnp/psu.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/73441385/system.o: ../src/system/system.c  .generated_files/e57b03ae244d0473634a4ffdca0da03c2ea497d1.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/73441385" 
	@${RM} ${OBJECTDIR}/_ext/73441385/system.o.d 
	@${RM} ${OBJECTDIR}/_ext/73441385/system.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/73441385/system.o.d" -o ${OBJECTDIR}/_ext/73441385/system.o ../src/system/system.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1014815340/printf.o: ../../../tools/printf.c  .generated_files/3c1bc5230103ebfdd55056fb790473836be4bdba.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1014815340" 
	@${RM} ${OBJECTDIR}/_ext/1014815340/printf.o.d 
	@${RM} ${OBJECTDIR}/_ext/1014815340/printf.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1014815340/printf.o.d" -o ${OBJECTDIR}/_ext/1014815340/printf.o ../../../tools/printf.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1014815340/ms_timer.o: ../../../tools/ms_timer.c  .generated_files/52f82bb5513d8f19fbcea776dfed8a2d6fd7e8c2.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1014815340" 
	@${RM} ${OBJECTDIR}/_ext/1014815340/ms_timer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1014815340/ms_timer.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1014815340/ms_timer.o.d" -o ${OBJECTDIR}/_ext/1014815340/ms_timer.o ../../../tools/ms_timer.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  .generated_files/e69792d26dba157efd063131006e547eeb619333.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
else
${OBJECTDIR}/_ext/1019403322/parser.o: ../src/comm/parser.c  .generated_files/ad426fa9d2eb07ed3ad5fc1eab191d42ad358d69.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1019403322" 
	@${RM} ${OBJECTDIR}/_ext/1019403322/parser.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019403322/parser.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1019403322/parser.o.d" -o ${OBJECTDIR}/_ext/1019403322/parser.o ../src/comm/parser.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1019403322/serial.o: ../src/comm/serial.c  .generated_files/a788c92d458b5be2c5dcd6edd1cbb0850c89a40.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1019403322" 
	@${RM} ${OBJECTDIR}/_ext/1019403322/serial.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019403322/serial.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1019403322/serial.o.d" -o ${OBJECTDIR}/_ext/1019403322/serial.o ../src/comm/serial.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1019403322/wifi.o: ../src/comm/wifi.c  .generated_files/638d339645ff4b5b5d2bbd4503c04318294af716.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1019403322" 
	@${RM} ${OBJECTDIR}/_ext/1019403322/wifi.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019403322/wifi.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1019403322/wifi.o.d" -o ${OBJECTDIR}/_ext/1019403322/wifi.o ../src/comm/wifi.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1019403322/gcode.o: ../src/comm/gcode.c  .generated_files/59591723b059cf2f2844fe25c3f1d21b711236a.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1019403322" 
	@${RM} ${OBJECTDIR}/_ext/1019403322/gcode.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019403322/gcode.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1019403322/gcode.o.d" -o ${OBJECTDIR}/_ext/1019403322/gcode.o ../src/comm/gcode.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1019403322/shell.o: ../src/comm/shell.c  .generated_files/5276f00d443016a658c5dba87b75a6372ef30633.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1019403322" 
	@${RM} ${OBJECTDIR}/_ext/1019403322/shell.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019403322/shell.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1019403322/shell.o.d" -o ${OBJECTDIR}/_ext/1019403322/shell.o ../src/comm/shell.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1019403322/comm.o: ../src/comm/comm.c  .generated_files/6c83cb02d60bf1ea82c46dc666a5477768dbab9c.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1019403322" 
	@${RM} ${OBJECTDIR}/_ext/1019403322/comm.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019403322/comm.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1019403322/comm.o.d" -o ${OBJECTDIR}/_ext/1019403322/comm.o ../src/comm/comm.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1019403322/canbus.o: ../src/comm/canbus.c  .generated_files/2fea43f8960c6897bb476997fa5a3a087d802de7.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1019403322" 
	@${RM} ${OBJECTDIR}/_ext/1019403322/canbus.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019403322/canbus.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1019403322/canbus.o.d" -o ${OBJECTDIR}/_ext/1019403322/canbus.o ../src/comm/canbus.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1982400153/plib_adchs.o: ../src/config/default/peripheral/adchs/plib_adchs.c  .generated_files/9ab3c94f7a3a65e1edc09b9ca6860d83ffff2b66.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1982400153" 
	@${RM} ${OBJECTDIR}/_ext/1982400153/plib_adchs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1982400153/plib_adchs.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1982400153/plib_adchs.o.d" -o ${OBJECTDIR}/_ext/1982400153/plib_adchs.o ../src/config/default/peripheral/adchs/plib_adchs.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1984157808/plib_cache.o: ../src/config/default/peripheral/cache/plib_cache.c  .generated_files/1aeeff8ec86d831fabeaae847ece3d742eb17700.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1984157808" 
	@${RM} ${OBJECTDIR}/_ext/1984157808/plib_cache.o.d 
	@${RM} ${OBJECTDIR}/_ext/1984157808/plib_cache.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1984157808/plib_cache.o.d" -o ${OBJECTDIR}/_ext/1984157808/plib_cache.o ../src/config/default/peripheral/cache/plib_cache.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/60165182/plib_can1.o: ../src/config/default/peripheral/can/plib_can1.c  .generated_files/f6e394b8b00cd82e3928dc84d97c484a58a75b8b.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/60165182" 
	@${RM} ${OBJECTDIR}/_ext/60165182/plib_can1.o.d 
	@${RM} ${OBJECTDIR}/_ext/60165182/plib_can1.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/60165182/plib_can1.o.d" -o ${OBJECTDIR}/_ext/60165182/plib_can1.o ../src/config/default/peripheral/can/plib_can1.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/60165520/plib_clk.o: ../src/config/default/peripheral/clk/plib_clk.c  .generated_files/17d2c3a91185bad8c06d64236c6906fc27965dc.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/60165520" 
	@${RM} ${OBJECTDIR}/_ext/60165520/plib_clk.o.d 
	@${RM} ${OBJECTDIR}/_ext/60165520/plib_clk.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/60165520/plib_clk.o.d" -o ${OBJECTDIR}/_ext/60165520/plib_clk.o ../src/config/default/peripheral/clk/plib_clk.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1249264884/plib_coretimer.o: ../src/config/default/peripheral/coretimer/plib_coretimer.c  .generated_files/35000cb2d4d4fe4676fe3fcab9c7814c64df671a.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1249264884" 
	@${RM} ${OBJECTDIR}/_ext/1249264884/plib_coretimer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1249264884/plib_coretimer.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1249264884/plib_coretimer.o.d" -o ${OBJECTDIR}/_ext/1249264884/plib_coretimer.o ../src/config/default/peripheral/coretimer/plib_coretimer.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1865200349/plib_evic.o: ../src/config/default/peripheral/evic/plib_evic.c  .generated_files/4765047187fee4e0126b24cd0ea90c5213d442cd.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1865200349" 
	@${RM} ${OBJECTDIR}/_ext/1865200349/plib_evic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865200349/plib_evic.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1865200349/plib_evic.o.d" -o ${OBJECTDIR}/_ext/1865200349/plib_evic.o ../src/config/default/peripheral/evic/plib_evic.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1865254177/plib_gpio.o: ../src/config/default/peripheral/gpio/plib_gpio.c  .generated_files/c2ec1c6e7c11c772259a0ac5cad2d6203da86d17.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1865254177" 
	@${RM} ${OBJECTDIR}/_ext/1865254177/plib_gpio.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865254177/plib_gpio.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1865254177/plib_gpio.o.d" -o ${OBJECTDIR}/_ext/1865254177/plib_gpio.o ../src/config/default/peripheral/gpio/plib_gpio.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1865480137/plib_ocmp3.o: ../src/config/default/peripheral/ocmp/plib_ocmp3.c  .generated_files/b337d0350168cf9f1486ca8638ba5b81f72f5b0.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1865480137" 
	@${RM} ${OBJECTDIR}/_ext/1865480137/plib_ocmp3.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865480137/plib_ocmp3.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1865480137/plib_ocmp3.o.d" -o ${OBJECTDIR}/_ext/1865480137/plib_ocmp3.o ../src/config/default/peripheral/ocmp/plib_ocmp3.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/60181895/plib_tmr2.o: ../src/config/default/peripheral/tmr/plib_tmr2.c  .generated_files/e0e439ffac3272816a9ba0a9e20dd8ec3e54e934.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/60181895" 
	@${RM} ${OBJECTDIR}/_ext/60181895/plib_tmr2.o.d 
	@${RM} ${OBJECTDIR}/_ext/60181895/plib_tmr2.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/60181895/plib_tmr2.o.d" -o ${OBJECTDIR}/_ext/60181895/plib_tmr2.o ../src/config/default/peripheral/tmr/plib_tmr2.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/60181895/plib_tmr4.o: ../src/config/default/peripheral/tmr/plib_tmr4.c  .generated_files/732ae3bbe1b9d886b5b649a1efaf7b922dd1cfce.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/60181895" 
	@${RM} ${OBJECTDIR}/_ext/60181895/plib_tmr4.o.d 
	@${RM} ${OBJECTDIR}/_ext/60181895/plib_tmr4.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/60181895/plib_tmr4.o.d" -o ${OBJECTDIR}/_ext/60181895/plib_tmr4.o ../src/config/default/peripheral/tmr/plib_tmr4.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/60181895/plib_tmr3.o: ../src/config/default/peripheral/tmr/plib_tmr3.c  .generated_files/3ba1290ebe6052daedf614a955f516e76b2df99b.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/60181895" 
	@${RM} ${OBJECTDIR}/_ext/60181895/plib_tmr3.o.d 
	@${RM} ${OBJECTDIR}/_ext/60181895/plib_tmr3.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/60181895/plib_tmr3.o.d" -o ${OBJECTDIR}/_ext/60181895/plib_tmr3.o ../src/config/default/peripheral/tmr/plib_tmr3.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1865657120/plib_uart5.o: ../src/config/default/peripheral/uart/plib_uart5.c  .generated_files/d36459815016296e5de3055cef78f6ed858d84fb.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1865657120" 
	@${RM} ${OBJECTDIR}/_ext/1865657120/plib_uart5.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865657120/plib_uart5.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1865657120/plib_uart5.o.d" -o ${OBJECTDIR}/_ext/1865657120/plib_uart5.o ../src/config/default/peripheral/uart/plib_uart5.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1865657120/plib_uart3.o: ../src/config/default/peripheral/uart/plib_uart3.c  .generated_files/7a79a1cf309302ce19016a7bc565d113c003cdbe.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1865657120" 
	@${RM} ${OBJECTDIR}/_ext/1865657120/plib_uart3.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865657120/plib_uart3.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1865657120/plib_uart3.o.d" -o ${OBJECTDIR}/_ext/1865657120/plib_uart3.o ../src/config/default/peripheral/uart/plib_uart3.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1865657120/plib_uart4.o: ../src/config/default/peripheral/uart/plib_uart4.c  .generated_files/4ee1e30a584b62c2e19d7b037a9ab2e44018af47.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1865657120" 
	@${RM} ${OBJECTDIR}/_ext/1865657120/plib_uart4.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865657120/plib_uart4.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1865657120/plib_uart4.o.d" -o ${OBJECTDIR}/_ext/1865657120/plib_uart4.o ../src/config/default/peripheral/uart/plib_uart4.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1865657120/plib_uart2.o: ../src/config/default/peripheral/uart/plib_uart2.c  .generated_files/c0536d49fa63725c1846af3c5668c95a74e0864d.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1865657120" 
	@${RM} ${OBJECTDIR}/_ext/1865657120/plib_uart2.o.d 
	@${RM} ${OBJECTDIR}/_ext/1865657120/plib_uart2.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1865657120/plib_uart2.o.d" -o ${OBJECTDIR}/_ext/1865657120/plib_uart2.o ../src/config/default/peripheral/uart/plib_uart2.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/631034509/plib_uart6.o: ..\src\config\default\peripheral\uart\plib_uart6.c  .generated_files/c4b3944e129f583bc22b1ab38065a9a12dff6b05.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/631034509" 
	@${RM} ${OBJECTDIR}/_ext/631034509/plib_uart6.o.d 
	@${RM} ${OBJECTDIR}/_ext/631034509/plib_uart6.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/631034509/plib_uart6.o.d" -o ${OBJECTDIR}/_ext/631034509/plib_uart6.o ..\src\config\default\peripheral\uart\plib_uart6.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/163028504/xc32_monitor.o: ../src/config/default/stdio/xc32_monitor.c  .generated_files/1c17655a9553bbfe0c69c645d25551f246d1df38.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/163028504" 
	@${RM} ${OBJECTDIR}/_ext/163028504/xc32_monitor.o.d 
	@${RM} ${OBJECTDIR}/_ext/163028504/xc32_monitor.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/163028504/xc32_monitor.o.d" -o ${OBJECTDIR}/_ext/163028504/xc32_monitor.o ../src/config/default/stdio/xc32_monitor.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1171490990/initialization.o: ../src/config/default/initialization.c  .generated_files/5d5e20240887a79182109ecd0c286ac42c9cc9aa.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1171490990" 
	@${RM} ${OBJECTDIR}/_ext/1171490990/initialization.o.d 
	@${RM} ${OBJECTDIR}/_ext/1171490990/initialization.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1171490990/initialization.o.d" -o ${OBJECTDIR}/_ext/1171490990/initialization.o ../src/config/default/initialization.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1171490990/interrupts.o: ../src/config/default/interrupts.c  .generated_files/ca0f4b5fd1014998c01288b036f2d2c64a93c656.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1171490990" 
	@${RM} ${OBJECTDIR}/_ext/1171490990/interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/1171490990/interrupts.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1171490990/interrupts.o.d" -o ${OBJECTDIR}/_ext/1171490990/interrupts.o ../src/config/default/interrupts.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1171490990/exceptions.o: ../src/config/default/exceptions.c  .generated_files/2fc8f4a077ca123a3d50c2251dc69791a2a8ebf4.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1171490990" 
	@${RM} ${OBJECTDIR}/_ext/1171490990/exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1171490990/exceptions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1171490990/exceptions.o.d" -o ${OBJECTDIR}/_ext/1171490990/exceptions.o ../src/config/default/exceptions.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1256919026/feeders.o: ../src/feeders/feeders.c  .generated_files/87d7459baba33b93e7f95f453b253b75652cb2fd.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1256919026" 
	@${RM} ${OBJECTDIR}/_ext/1256919026/feeders.o.d 
	@${RM} ${OBJECTDIR}/_ext/1256919026/feeders.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1256919026/feeders.o.d" -o ${OBJECTDIR}/_ext/1256919026/feeders.o ../src/feeders/feeders.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1256919026/slots.o: ../src/feeders/slots.c  .generated_files/694790a12c47703a1c84f3662d9f332a5db77e3d.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1256919026" 
	@${RM} ${OBJECTDIR}/_ext/1256919026/slots.o.d 
	@${RM} ${OBJECTDIR}/_ext/1256919026/slots.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1256919026/slots.o.d" -o ${OBJECTDIR}/_ext/1256919026/slots.o ../src/feeders/slots.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/659861322/log.o: ../src/log/log.c  .generated_files/408911339262047d7b79609a908c089c387e782b.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/659861322" 
	@${RM} ${OBJECTDIR}/_ext/659861322/log.o.d 
	@${RM} ${OBJECTDIR}/_ext/659861322/log.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/659861322/log.o.d" -o ${OBJECTDIR}/_ext/659861322/log.o ../src/log/log.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/107549200/engine.o: ../src/motion/engine.c  .generated_files/5103ff1fbd3aa31ef7b7a505f06ff2dac3ff3112.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/107549200" 
	@${RM} ${OBJECTDIR}/_ext/107549200/engine.o.d 
	@${RM} ${OBJECTDIR}/_ext/107549200/engine.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/107549200/engine.o.d" -o ${OBJECTDIR}/_ext/107549200/engine.o ../src/motion/engine.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/107549200/driver.o: ../src/motion/driver.c  .generated_files/625cb09b46303b495e4c32e94c723bba15cfc0b1.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/107549200" 
	@${RM} ${OBJECTDIR}/_ext/107549200/driver.o.d 
	@${RM} ${OBJECTDIR}/_ext/107549200/driver.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/107549200/driver.o.d" -o ${OBJECTDIR}/_ext/107549200/driver.o ../src/motion/driver.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/107549200/movement.o: ../src/motion/movement.c  .generated_files/eb7f6608ef700111d58e985263aaf107b7402cae.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/107549200" 
	@${RM} ${OBJECTDIR}/_ext/107549200/movement.o.d 
	@${RM} ${OBJECTDIR}/_ext/107549200/movement.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/107549200/movement.o.d" -o ${OBJECTDIR}/_ext/107549200/movement.o ../src/motion/movement.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/107549200/profiler.o: ../src/motion/profiler.c  .generated_files/f2379f8b6c7293e6bda22628d65ca57c9b125585.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/107549200" 
	@${RM} ${OBJECTDIR}/_ext/107549200/profiler.o.d 
	@${RM} ${OBJECTDIR}/_ext/107549200/profiler.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/107549200/profiler.o.d" -o ${OBJECTDIR}/_ext/107549200/profiler.o ../src/motion/profiler.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/107549200/blocks.o: ../src/motion/blocks.c  .generated_files/5b3a4996cd4383d5e866581baadf97dea561c65b.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/107549200" 
	@${RM} ${OBJECTDIR}/_ext/107549200/blocks.o.d 
	@${RM} ${OBJECTDIR}/_ext/107549200/blocks.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/107549200/blocks.o.d" -o ${OBJECTDIR}/_ext/107549200/blocks.o ../src/motion/blocks.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/107549200/axes_struct.o: ../src/motion/axes_struct.c  .generated_files/352cd193d13a75fdaf24f928587c6c4ed88ce754.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/107549200" 
	@${RM} ${OBJECTDIR}/_ext/107549200/axes_struct.o.d 
	@${RM} ${OBJECTDIR}/_ext/107549200/axes_struct.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/107549200/axes_struct.o.d" -o ${OBJECTDIR}/_ext/107549200/axes_struct.o ../src/motion/axes_struct.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/13954849/solenoids.o: ../src/pneumatics/solenoids.c  .generated_files/aac214defcc01e20182b3aef99bc5068c8387d8b.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/13954849" 
	@${RM} ${OBJECTDIR}/_ext/13954849/solenoids.o.d 
	@${RM} ${OBJECTDIR}/_ext/13954849/solenoids.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/13954849/solenoids.o.d" -o ${OBJECTDIR}/_ext/13954849/solenoids.o ../src/pneumatics/solenoids.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/13954849/pump.o: ../src/pneumatics/pump.c  .generated_files/c391fc1601838a82b6b77ffac1e6ddda90fcf1de.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/13954849" 
	@${RM} ${OBJECTDIR}/_ext/13954849/pump.o.d 
	@${RM} ${OBJECTDIR}/_ext/13954849/pump.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/13954849/pump.o.d" -o ${OBJECTDIR}/_ext/13954849/pump.o ../src/pneumatics/pump.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/13954849/sensors.o: ../src/pneumatics/sensors.c  .generated_files/f8abec6186fb59af82db849b7d0bb320dd978d6.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/13954849" 
	@${RM} ${OBJECTDIR}/_ext/13954849/sensors.o.d 
	@${RM} ${OBJECTDIR}/_ext/13954849/sensors.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/13954849/sensors.o.d" -o ${OBJECTDIR}/_ext/13954849/sensors.o ../src/pneumatics/sensors.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/659865144/signalers.o: ../src/pnp/signalers.c  .generated_files/98a747aaec5ae3317e2d2561add664436d3c73c3.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/659865144" 
	@${RM} ${OBJECTDIR}/_ext/659865144/signalers.o.d 
	@${RM} ${OBJECTDIR}/_ext/659865144/signalers.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/659865144/signalers.o.d" -o ${OBJECTDIR}/_ext/659865144/signalers.o ../src/pnp/signalers.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/659865144/cam_led.o: ../src/pnp/cam_led.c  .generated_files/abc6c42f8ec99dac7aaad4e1fedb2e45c5589b16.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/659865144" 
	@${RM} ${OBJECTDIR}/_ext/659865144/cam_led.o.d 
	@${RM} ${OBJECTDIR}/_ext/659865144/cam_led.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/659865144/cam_led.o.d" -o ${OBJECTDIR}/_ext/659865144/cam_led.o ../src/pnp/cam_led.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/659865144/tests.o: ../src/pnp/tests.c  .generated_files/89e64246c2b118dcc0d0aa4bf6e01cd36ea53e81.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/659865144" 
	@${RM} ${OBJECTDIR}/_ext/659865144/tests.o.d 
	@${RM} ${OBJECTDIR}/_ext/659865144/tests.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/659865144/tests.o.d" -o ${OBJECTDIR}/_ext/659865144/tests.o ../src/pnp/tests.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/659865144/psu.o: ../src/pnp/psu.c  .generated_files/634f293af9a3cdd0cd87b43120bce230e42aa05a.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/659865144" 
	@${RM} ${OBJECTDIR}/_ext/659865144/psu.o.d 
	@${RM} ${OBJECTDIR}/_ext/659865144/psu.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/659865144/psu.o.d" -o ${OBJECTDIR}/_ext/659865144/psu.o ../src/pnp/psu.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/73441385/system.o: ../src/system/system.c  .generated_files/3643a5207d9ef8ec20111aad84d857c358766633.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/73441385" 
	@${RM} ${OBJECTDIR}/_ext/73441385/system.o.d 
	@${RM} ${OBJECTDIR}/_ext/73441385/system.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/73441385/system.o.d" -o ${OBJECTDIR}/_ext/73441385/system.o ../src/system/system.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1014815340/printf.o: ../../../tools/printf.c  .generated_files/a2640f0ab8ddbc8687baf91004512eadaaab9029.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1014815340" 
	@${RM} ${OBJECTDIR}/_ext/1014815340/printf.o.d 
	@${RM} ${OBJECTDIR}/_ext/1014815340/printf.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1014815340/printf.o.d" -o ${OBJECTDIR}/_ext/1014815340/printf.o ../../../tools/printf.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1014815340/ms_timer.o: ../../../tools/ms_timer.c  .generated_files/de77e3d84c4bbc005b2638cf529c065de3d667af.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1014815340" 
	@${RM} ${OBJECTDIR}/_ext/1014815340/ms_timer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1014815340/ms_timer.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1014815340/ms_timer.o.d" -o ${OBJECTDIR}/_ext/1014815340/ms_timer.o ../../../tools/ms_timer.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  .generated_files/e5b63633563c384a41b96ec0222c439a2396a587.flag .generated_files/5c4d50afbefed163d8b382b3a977fd5a48121127.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../tools" -I"../src" -ffunction-sections -O2 -funroll-loops -fomit-frame-pointer -I"../../../tools" -I"../src" -I"../src/config/default" -I"../src/mips" -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -std=c99 -Wall -Wdouble-promotion -std=c99 -Wall -Wdouble-promotion -mdfp="${DFP_DIR}"  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/PNP_MB_V2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../src/config/default/p32MZ2048EFH144.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -g -mdebugger -D__MPLAB_DEBUGGER_PK3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/PNP_MB_V2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -std=c99 -Wall -Wdouble-promotion $(COMPARISON_BUILD)   -mreserve=data@0x0:0x37F   -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=__MPLAB_DEBUGGER_PK3=1,--defsym=_min_heap_size=4096,--defsym=_min_stack_size=32767,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -mdfp="${DFP_DIR}"
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/PNP_MB_V2.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../src/config/default/p32MZ2048EFH144.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/PNP_MB_V2.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -std=c99 -Wall -Wdouble-promotion $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=4096,--defsym=_min_stack_size=32767,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -mdfp="${DFP_DIR}"
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/PNP_MB_V2.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
	@echo Normalizing hex file
	@"C:/Program Files/Microchip/MPLABX/v5.45/mplab_platform/platform/../mplab_ide/modules/../../bin/hexmate" --edf="C:/Program Files/Microchip/MPLABX/v5.45/mplab_platform/platform/../mplab_ide/modules/../../dat/en_msgs.txt" dist/${CND_CONF}/${IMAGE_TYPE}/PNP_MB_V2.X.${IMAGE_TYPE}.hex -odist/${CND_CONF}/${IMAGE_TYPE}/PNP_MB_V2.X.${IMAGE_TYPE}.hex

endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
